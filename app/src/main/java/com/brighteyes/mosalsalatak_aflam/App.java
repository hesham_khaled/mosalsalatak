package com.brighteyes.mosalsalatak_aflam;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.brighteyes.mosalsalatak_aflam.dagger.ApplicationModule;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;
import io.fabric.sdk.android.Fabric;

/**
 * Created by user on 1/3/17.
 */

public class App extends Application {

    public static ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Object[] modules = getModules().toArray();
        objectGraph = ObjectGraph.create(modules);
        objectGraph.inject(this);
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-5084160762510449~3669154616");

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    protected List<Object> getModules() {
        ApplicationModule applicationModule = new ApplicationModule(this);
        return Arrays.<Object>asList(
                applicationModule

        );
    }
}
