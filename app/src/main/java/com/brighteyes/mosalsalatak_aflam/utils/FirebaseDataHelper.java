package com.brighteyes.mosalsalatak_aflam.utils;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Category;
import com.brighteyes.mosalsalatak_aflam.entities.CategorySeries;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.entities.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Collections;

/**
 * Created by user on 12/25/16.
 */

public class FirebaseDataHelper {

    Singleton singleton;
    Context context;

    public FirebaseDataHelper(Context context) {
        singleton = Singleton.getInstance();
        this.context = context;
    }

    public void openAuthenticationListener() {
        FirebaseAuth.AuthStateListener mAuthListener;

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    singleton.currentUser = new User();
                } else {
                    // User is signed out
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "onAuthStateChanged:signed_out");
                    singleton.currentUser = null;
                }
                Intent intent = new Intent();
                intent.setAction(context.getResources().getString(R.string.authChanged));
                context.sendBroadcast(intent);
            }
        };

        Singleton.sAuth.addAuthStateListener(mAuthListener);
    }

    public void openUserListener(final String email, final String password) {
        final DatabaseReference userDatabaseReferece = Singleton.sDatabase.child("user");
        userDatabaseReferece.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()) {
                    User user = User.parse(userDataSnapshot);
                    if (user != null && user.email.equals(email) && user.password.equals(password)) {
                        Singleton.getInstance().currentUser = user;
                        Intent intent = new Intent();
                        intent.setAction(context.getResources().getString(R.string.userChanged));
                        context.sendBroadcast(intent);
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, databaseError.toString());
            }
        });
    }

    public void openCategoriesListener() {

        final DatabaseReference categoriesDatabaseReference = Singleton.sDatabase.child("category");
        categoriesDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                singleton.categories.clear();

                for (DataSnapshot categoryDataSnapshot : dataSnapshot.getChildren()) {
                    Category category = Category.parse(categoryDataSnapshot);
                    singleton.categories.add(category);
                }

                Collections.reverse(singleton.categories);

                Intent intent = new Intent();
                intent.setAction(context.getResources().getString(R.string.categoryChanged));
                context.sendBroadcast(intent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void openEpisodesListener() {
        final DatabaseReference episodesDatabaseReference = Singleton.sDatabase.child("episode");
        episodesDatabaseReference.orderByChild("creationDate").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                singleton.episodes.clear();

                for (DataSnapshot episodeDataSnaspshot : dataSnapshot.getChildren()) {
                    Episode episode = Episode.parse(episodeDataSnaspshot);
                    singleton.episodes.add(episode);
                }

                Intent intent = new Intent();
                intent.setAction(context.getResources().getString(R.string.episodesChanged));
                context.sendBroadcast(intent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void openCategorySeriesListener(final String categoryId) {

        final CategorySeries categorySeries = new CategorySeries();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                categorySeries.categoryId = categoryId;

                for (DataSnapshot seriesDataSnapshot : dataSnapshot.getChildren()) {
                    Series series = Series.parse(seriesDataSnapshot);
                    categorySeries.series.add(series);
                }

                Intent intent = new Intent();
                intent.setAction(context.getResources().getString(R.string.categorySeriesChanged));
                Bundle bundle = new Bundle();
                bundle.putSerializable("category_series", categorySeries);
                intent.putExtras(bundle);
                context.sendBroadcast(intent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Intent intent = new Intent();
                intent.setAction(context.getResources().getString(R.string.categorySeriesChanged));
                Bundle bundle = new Bundle();
                bundle.putString("error", databaseError.getMessage());
                intent.putExtras(bundle);
                context.sendBroadcast(intent);
            }
        };
        final DatabaseReference categorySeriesDatabaseReference = Singleton.sDatabase.
                child("category_series").child(categoryId);
        categorySeriesDatabaseReference.addValueEventListener(listener);

//        if (!isClose) {
//            categorySeriesDatabaseReference.addValueEventListener(listener);
//        } else {
//            categorySeriesDatabaseReference.removeEventListener(listener);
//        }


    }

}
