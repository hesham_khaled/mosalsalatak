package com.brighteyes.mosalsalatak_aflam.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Surface;

/**
 * Created by hesham on 04/04/16.
 */
public class Sensor {

    public static void lockSensor(Context ctx) {
        if (((Activity) ctx).getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_0)
            ((Activity) ctx).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (((Activity) ctx).getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_90)
            ((Activity) ctx).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        if (((Activity) ctx).getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_270)
            ((Activity) ctx).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
    }

    /**
     * @param ctx application mContext
     */
    public static void unlockSensor(Context ctx) {
        ((Activity) ctx).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

    }

}
