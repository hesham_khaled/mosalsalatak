package com.brighteyes.mosalsalatak_aflam.utils;

/**
 * Created by heshamkhaled on 9/20/16.
 */
public class CommonConstants {

    public static final String APPLICATION_LOG_TAG = "mosalsalatApp";
    public static final String SHARED_PREFERENCES_NAME = "mosalsalat";
    public static final String IS_LOGGED_IN = "isLoggedIn";
    public static final String USER_ID = "userId";
    public static final String USER_IS_ADMIN = "userIsAdmin";
    public static final String USER_NAME = "userName";
    public static final String USER_EMAIL = "userEmail";
    public static final String USER_PASSWORD = "userPassword";
    public static final String USER_NATIONALITY = "userNationality";
    public static final String USER_GENDER = "userGender";
    public static final String USER_BIRTH_DATE = "userBirthDate";
    public static final String USER_FCM_TOKEN = "userFcmToken";
    public static final String USER_PHOTO_URL = "userPhotoUrl";
    public static final String USER_FAVOURITES = "userFavourites";
    public static final String USER_SETTINGS = "userSettings";
    public static final String CATEGORIES_AD = "CATEGORIES_AD";
    public static final String SERIES_AD = "SERIES_AD";
    public static final String EPISODE_AD = "EPISODE_AD";
    public static final String SEARCH_RESULTS_AD = "SEARCH_RESULTS_AD";
    public static final String FAVOURITES_AD = "FAVOURITES_AD";
    public static final String FULL_SCREEN_AD = "FULL_SCREEN_AD";
    public static final String VIDEO_AD = "VIDEO_AD";
    public static final String IS_NOTIFICATIONS_ENABLED = "IS_NOTIFICATIONS_ENABLED";



    int lastAction = 0;

    /*
    alias = bright
    any password = 123456

     */


}
