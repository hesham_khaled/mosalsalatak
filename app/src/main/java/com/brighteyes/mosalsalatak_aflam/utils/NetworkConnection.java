package com.brighteyes.mosalsalatak_aflam.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkConnection {

    /**
     * @param context the application mContext
     * @return true if the device connected to a network, false if not
     */
    public static boolean networkConnectivity(Context context) {
        boolean networkConnectivity = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            networkConnectivity = true;
        }

        return networkConnectivity;
    }

}
