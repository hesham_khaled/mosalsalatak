package com.brighteyes.mosalsalatak_aflam.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.brighteyes.mosalsalatak_aflam.NotifyFirebaseObserverEnum;
import com.brighteyes.mosalsalatak_aflam.entities.CategorySeries;
import com.brighteyes.mosalsalatak_aflam.interfaces.AuthenticationObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.CategoryObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.CategorySeriesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.EpisodeCommentsObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.EpisodesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.FavouritesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.FirebaseSubjectListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.SeriesEpisodesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.SeriesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.SettingsObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.UserObserverListener;


public class FirebaseListenersUpdateReceiver extends BroadcastReceiver implements FirebaseSubjectListener {

    Singleton singleton;

    public FirebaseListenersUpdateReceiver() {
        singleton = Singleton.getInstance();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        switch (action) {
            case "authChanged":
                notifyObservers(NotifyFirebaseObserverEnum.AUTHENTICATION_CHANGED, intent);
                break;

            case "categoryChanged":
                notifyObservers(NotifyFirebaseObserverEnum.CATEGORY_CHANGED, intent);
                break;

            case "userChanged":
                notifyObservers(NotifyFirebaseObserverEnum.USER_CHANGED, intent);
                break;

            case "categorySeriesChanged":
                notifyObservers(NotifyFirebaseObserverEnum.CATEGORY_SERIES_CHANGED, intent);
                break;

            case "episodeCommentsChanged":
                notifyObservers(NotifyFirebaseObserverEnum.EPISODE_COMMENTS_CHANGED, intent);
                break;

            case "favouritesChanged":
                notifyObservers(NotifyFirebaseObserverEnum.FAVOURITE_CHANGED, intent);
                break;

            case "seriesEpisodesChanged":
                notifyObservers(NotifyFirebaseObserverEnum.SERIES_EPISODES_CHANGED, intent);
                break;

            case "seriesChanged":
                notifyObservers(NotifyFirebaseObserverEnum.SERIES_CHANGED, intent);
                break;

            case "settingsChanged":
                notifyObservers(NotifyFirebaseObserverEnum.SETTINGS_CHANGED, intent);
                break;

            case "episodesChanged":
                notifyObservers(NotifyFirebaseObserverEnum.EPISODE_CHANGED, intent);
                break;

            default:
                break;
        }
    }

    @Override
    public void registerAuthenticationObserver(AuthenticationObserverListener observer) {
        singleton.authenticationObserverListeners.add(observer);
    }

    @Override
    public void unregisterAuthenticationObserver(AuthenticationObserverListener observer) {
        int index = singleton.authenticationObserverListeners.indexOf(observer);
        singleton.authenticationObserverListeners.remove(index);
    }

    @Override
    public void registerUserObserver(UserObserverListener observer) {
        singleton.userObserverListeners.add(observer);
    }

    @Override
    public void unregisterUserObserver(UserObserverListener observer) {
        int index = singleton.userObserverListeners.indexOf(observer);
        singleton.userObserverListeners.remove(index);
    }

    @Override
    public void registerCategoryObserver(CategoryObserverListener observer) {
        singleton.categoryObserverListeners.add(observer);
    }

    @Override
    public void unregisterCategoryObserver(CategoryObserverListener observer) {
        int index = singleton.categoryObserverListeners.indexOf(observer);
        singleton.categoryObserverListeners.remove(index);
    }

    @Override
    public void registerCategorySeriesObserver(CategorySeriesObserver observer) {
        singleton.categorySeriesObservers.add(observer);
    }

    @Override
    public void unregisterCategorySeriesObserver(CategorySeriesObserver observer) {
        int index = singleton.categorySeriesObservers.indexOf(observer);
        singleton.categorySeriesObservers.remove(index);
    }

    @Override
    public void registerEpisodeCommentsObserver(EpisodeCommentsObserver observer) {
        singleton.episodeCommentsObservers.add(observer);
    }

    @Override
    public void unregisterEpisodeCommentsObserver(EpisodeCommentsObserver observer) {
        int index = singleton.episodeCommentsObservers.indexOf(observer);
        singleton.episodeCommentsObservers.remove(index);
    }

    @Override
    public void registerFavouriteObserver(FavouritesObserver observer) {
        singleton.favouritesObservers.add(observer);
    }

    @Override
    public void unregisterFavouriteObserver(FavouritesObserver observer) {
        int index = singleton.favouritesObservers.indexOf(observer);
        singleton.favouritesObservers.remove(index);
    }

    @Override
    public void registerSeriesEpisodeObserver(SeriesEpisodesObserver observer) {
        singleton.seriesEpisodesObservers.add(observer);
    }

    @Override
    public void unregisterSeriesEpisodeObserver(SeriesEpisodesObserver observer) {
        int index = singleton.seriesEpisodesObservers.indexOf(observer);
        singleton.seriesEpisodesObservers.remove(index);
    }

    @Override
    public void registerSeriesObserver(SeriesObserver observer) {
        singleton.seriesObservers.add(observer);
    }

    @Override
    public void unregisterSeriesObserver(SeriesObserver observer) {
        int index = singleton.seriesObservers.indexOf(observer);
        singleton.seriesObservers.remove(index);
    }

    @Override
    public void registerSettingsObserver(SettingsObserver observer) {
        singleton.settingsObservers.add(observer);
    }

    @Override
    public void unregisterSettingsObserver(SettingsObserver observer) {
        int index = singleton.settingsObservers.indexOf(observer);
        singleton.settingsObservers.remove(index);
    }

    @Override
    public void registerEpisodesObserver(EpisodesObserver observer) {
        singleton.episodesObservers.add(observer);
    }

    @Override
    public void unregisterEpisodesObserver(EpisodesObserver observer) {
        int index = singleton.episodesObservers.indexOf(observer);
        singleton.episodesObservers.remove(index);
    }

    @Override
    public void notifyObservers(NotifyFirebaseObserverEnum type, Intent intent) {
        switch (type) {

            case CATEGORY_CHANGED:
                for (CategoryObserverListener observer : singleton.categoryObserverListeners) {
                    observer.categoryChanged();
                }
                break;

            case AUTHENTICATION_CHANGED:
                for (AuthenticationObserverListener observer : singleton.authenticationObserverListeners) {
                    observer.authenticationStatusChanged(singleton.currentUser != null);
                }
                break;

            case USER_CHANGED:
                for (UserObserverListener observer : singleton.userObserverListeners) {
                    observer.userChanged();
                }
                break;

            case CATEGORY_SERIES_CHANGED:

                if (intent.getExtras().containsKey("category_series")) {
                    CategorySeries categorySeries = (CategorySeries) intent.getExtras().getSerializable("category_series");
                    for (CategorySeriesObserver observer : singleton.categorySeriesObservers) {
                        observer.categorySeriesChanged(categorySeries);
                    }
                } else {
                    for (CategorySeriesObserver observer : singleton.categorySeriesObservers) {
                        observer.onError(intent.getExtras().getString("error"));
                    }
                }

                break;

            case EPISODE_COMMENTS_CHANGED:
                for (EpisodeCommentsObserver observer : singleton.episodeCommentsObservers) {
                    observer.episodeCommentsObserver();
                }
                break;

            case FAVOURITE_CHANGED:
                for (FavouritesObserver observer : singleton.favouritesObservers) {
                    observer.favouriteChanged();
                }
                break;

            case SERIES_EPISODES_CHANGED:
                for (SeriesEpisodesObserver observer : singleton.seriesEpisodesObservers) {
                    observer.seriesEpisodesChanged();
                }
                break;

            case SERIES_CHANGED:
                for (SeriesObserver observer : singleton.seriesObservers) {
                    observer.seriesChanged();
                }
                break;

            case SETTINGS_CHANGED:
                for (SettingsObserver observer : singleton.settingsObservers) {
                    observer.settingsChanged();
                }
                break;

            case EPISODE_CHANGED:
                for (EpisodesObserver observer : singleton.episodesObservers) {
                    observer.episodeChanged();
                }
                break;

            default:
                break;

        }
    }
}
