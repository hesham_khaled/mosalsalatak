package com.brighteyes.mosalsalatak_aflam.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.brighteyes.mosalsalatak_aflam.interfaces.ObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.SubjectListener;

public class NetworkStateReceiver extends BroadcastReceiver implements SubjectListener {
    Singleton singleton;

    public NetworkStateReceiver() {
        singleton = Singleton.getInstance();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("profilePictureUpdated")) {
            notifyProfilePictureUpdated();
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                notifyObservers(true);
            } else {
                notifyObservers(false);
            }
        }
    }

    @Override
    public void register(ObserverListener observer) {
        singleton.observers.add(observer);
    }

    @Override
    public void unregister(ObserverListener observer) {
        int index = singleton.observers.indexOf(observer);
        singleton.observers.remove(index);
    }

    @Override
    public void notifyObservers(boolean internetConnectionExist) {
        for (ObserverListener observer : singleton.observers) {
            observer.update(internetConnectionExist);
        }
    }

    @Override
    public void notifyProfilePictureUpdated() {
        for (ObserverListener observer : singleton.observers) {
            observer.updateProfilePicture();
        }
    }

    @Override
    public void notifyDataChanged() {

    }
}
