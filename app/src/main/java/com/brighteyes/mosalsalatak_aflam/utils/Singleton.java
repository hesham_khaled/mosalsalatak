package com.brighteyes.mosalsalatak_aflam.utils;

import android.app.Activity;

import com.brighteyes.mosalsalatak_aflam.entities.AdsId;
import com.brighteyes.mosalsalatak_aflam.entities.Category;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.entities.Settings;
import com.brighteyes.mosalsalatak_aflam.entities.User;
import com.brighteyes.mosalsalatak_aflam.interfaces.AuthenticationObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.CategoryObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.CategorySeriesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.EpisodeCommentsObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.EpisodesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.FavouritesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.ObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.SeriesEpisodesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.SeriesObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.SettingsObserver;
import com.brighteyes.mosalsalatak_aflam.interfaces.UserObserverListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;

/**
 * Created by heshamkhaled on 10/10/16.
 */

public class Singleton {

    public static Singleton singleton = null;
    public static Activity currentActivity = null;
    public static DatabaseReference sDatabase = FirebaseDatabase.getInstance().getReference();
    public static FirebaseStorage sStorage = FirebaseStorage.getInstance();
    public static FirebaseAuth sAuth = FirebaseAuth.getInstance();
    public static ArrayList<Category> sCategories = new ArrayList<>();
    public ArrayList<ObserverListener> observers = new ArrayList<>();
    public ArrayList<AuthenticationObserverListener> authenticationObserverListeners = new ArrayList<>();
    public ArrayList<CategoryObserverListener> categoryObserverListeners = new ArrayList<>();
    public ArrayList<UserObserverListener> userObserverListeners = new ArrayList<>();
    public ArrayList<CategorySeriesObserver> categorySeriesObservers = new ArrayList<>();
    public ArrayList<EpisodeCommentsObserver> episodeCommentsObservers = new ArrayList<>();
    public ArrayList<FavouritesObserver> favouritesObservers = new ArrayList<>();
    public ArrayList<SeriesEpisodesObserver> seriesEpisodesObservers = new ArrayList<>();
    public ArrayList<SeriesObserver> seriesObservers = new ArrayList<>();
    public ArrayList<EpisodesObserver> episodesObservers = new ArrayList<>();
    public ArrayList<SettingsObserver> settingsObservers = new ArrayList<>();
    public ArrayList<Settings> settings = new ArrayList<>();
    public ArrayList<Series> seriesArrayList = new ArrayList<>();
    public ArrayList<Episode> episodeArrayList = new ArrayList<>();
    public int notificationsNumber = 0;
    public ArrayList<AdsId> adsIds = new ArrayList<>();
    public InterstitialAd mInterstitialAd;
    public ArrayList<Category> categories = new ArrayList<>();
    public ArrayList<Episode> episodes = new ArrayList<>();

    public User currentUser = null;

    public static Singleton getInstance() {

        if (singleton == null) {
            singleton = new Singleton();
        }

        return singleton;
    }
}
