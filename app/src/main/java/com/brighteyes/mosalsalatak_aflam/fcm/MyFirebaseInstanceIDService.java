package com.brighteyes.mosalsalatak_aflam.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.entities.FCMToken;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by heshamkhaled on 9/19/16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    Context context;

    public MyFirebaseInstanceIDService() {
    }

    public MyFirebaseInstanceIDService(Context context) {
        this.context = context;
    }

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedToken);
        saveInSharedPreferences(refreshedToken);
        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);


    }

    private void sendRegistrationToServer(final String token) {

        final String key = Singleton.sDatabase.child("fcm").push().getKey();
        final FCMToken fcmToken = new FCMToken();
        fcmToken.token = token;


        Map<String, Object> tokenValues = FCMToken.toMap(fcmToken);

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/fcm/" + key, tokenValues);

//        if (Singleton.getInstance().currentUser != null && Singleton.getInstance().currentUser.id != null) {
//            Singleton.getInstance().currentUser.fcmToken = token;
//        Singleton.sDatabase.child("user").child(Singleton.getInstance().currentUser.id)
//                .updateChildren(User.toMap(Singleton.getInstance().currentUser), new DatabaseReference.CompletionListener() {
//                    @Override
//                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//
//                    }
//                });
//        }
        Singleton.sDatabase.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "fcm token saved");
                }

            }
        });
    }

    private void saveInSharedPreferences(String token) {
        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CommonConstants.USER_FCM_TOKEN, token);
        editor.commit();
    }

}
