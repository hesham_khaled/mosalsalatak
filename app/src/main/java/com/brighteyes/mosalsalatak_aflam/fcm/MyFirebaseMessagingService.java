package com.brighteyes.mosalsalatak_aflam.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.AdsId;
import com.brighteyes.mosalsalatak_aflam.entities.Category;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.entities.User;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.activities.EpisodeActivity;
import com.brighteyes.mosalsalatak_aflam.views.activities.MainActivity;
import com.brighteyes.mosalsalatak_aflam.views.activities.SeriesActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Map;

/**
 * Created by heshamkhaled on 9/19/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Calling method to generate notification
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "onMessageReceived");
        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);

        boolean isLogged = sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, remoteMessage.getData().toString());
//        if (isLogged && sharedPreferences.getBoolean(CommonConstants.IS_NOTIFICATIONS_ENABLED, false)) {
            sendNotification(remoteMessage.getData());
//        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    public void sendNotification(Map<String, String> data) {

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "sendNotifications");

        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);

        Singleton.getInstance().adsIds.add(new AdsId(sharedPreferences.getString(CommonConstants.CATEGORIES_AD, null)));
        Singleton.getInstance().adsIds.add(new AdsId(sharedPreferences.getString(CommonConstants.SERIES_AD, null)));
        Singleton.getInstance().adsIds.add(new AdsId(sharedPreferences.getString(CommonConstants.EPISODE_AD, null)));
        Singleton.getInstance().adsIds.add(new AdsId(sharedPreferences.getString(CommonConstants.SEARCH_RESULTS_AD, null)));
        Singleton.getInstance().adsIds.add(new AdsId(sharedPreferences.getString(CommonConstants.FAVOURITES_AD, null)));
        Singleton.getInstance().adsIds.add(new AdsId(sharedPreferences.getString(CommonConstants.FULL_SCREEN_AD, null)));

        if (Singleton.getInstance().currentUser == null) {
            Singleton.getInstance().currentUser = new User();
            Singleton.getInstance().currentUser.name = sharedPreferences.getString(CommonConstants.USER_NAME, null);
            Singleton.getInstance().currentUser.email = sharedPreferences.getString(CommonConstants.USER_EMAIL, null);
            Singleton.getInstance().currentUser.id = sharedPreferences.getString(CommonConstants.USER_ID, null);
            Singleton.getInstance().currentUser.password = sharedPreferences.getString(CommonConstants.USER_PASSWORD, null);
            Singleton.getInstance().currentUser.nationality = sharedPreferences.getString(CommonConstants.USER_NATIONALITY, null);
            Singleton.getInstance().currentUser.photoUrl = sharedPreferences.getString(CommonConstants.USER_PHOTO_URL, "");
            Singleton.getInstance().currentUser.birthDate = sharedPreferences.getString(CommonConstants.USER_BIRTH_DATE, null);
            Singleton.getInstance().currentUser.gender = sharedPreferences.getString(CommonConstants.USER_GENDER, null);
            Singleton.getInstance().currentUser.isAdmin = sharedPreferences.getBoolean(CommonConstants.USER_IS_ADMIN, false);
            Singleton.getInstance().currentUser.fcmToken = sharedPreferences.getString(CommonConstants.USER_FCM_TOKEN, null);
        }

        Intent intent;
        Bundle bundle;

        JSONObject jsonObject = new JSONObject(data);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "jsonObject: " + jsonObject.toString());
        try {
            int type1 = Integer.valueOf(jsonObject.getString("[type]"));
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "type1: " + type1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "data.get(\"[type]\"): " + data.get("[type]"));
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "Integer.valueOf(data.get(\"[type]\")): " + Integer.valueOf(data.get("[type]")));


        int type = Integer.parseInt(data.get("[type]"));

        try {
            switch (type) {
                case 1: // category


                    Category category = new Category(jsonObject.getString("[object][id]"), jsonObject.getString("[object][name]"));

                    intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    intent.setAction(Long.toString(System.currentTimeMillis()));
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
                    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.logo)
                            .setContentTitle(getResources().getString(R.string.newSeriesNotificationTitle)
                                    + " " + category.name)
                            .setContentText("")
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(Singleton.getInstance().notificationsNumber++,
                            notificationBuilder.build());
                    break;

                case 2: // series

                    final Series series = new Series();

                    series.id = jsonObject.getString("[object][id]");
                    series.categoryId = jsonObject.getString("[object][category_id]");
                    series.name = jsonObject.getString("[object][name]");
                    series.photoUrl = jsonObject.getString("[object][photo_url]");
                    series.numberOfVideos = Long.valueOf(jsonObject.getString("[object][number_of_videos]"));
                    series.numberOfViews = Long.valueOf(jsonObject.getString("[object][number_of_viewes]"));

                    intent = new Intent(this, SeriesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    bundle = new Bundle();
                    bundle.putSerializable("series", series);
                    bundle.putString("categoryName", "");

                    intent.putExtras(bundle);

                    getBitmapFromUrlAndGenerateNotification(series.photoUrl,
                            getResources().getString(R.string.newSeriesNotificationTitle) + " " + series.name, intent);

                    break;

                case 3: // episode

                    final Episode episode = new Episode();

                    episode.id = jsonObject.getString("[object][id]");
                    episode.seriesId = jsonObject.getString("[object][series_id]");
                    episode.seriesName = jsonObject.getString("[object][seriesName]");
                    episode.photoUrl = jsonObject.getString("[object][photo_url]");
                    episode.videoUrl = jsonObject.getString("[object][video_url]");
                    episode.name = jsonObject.getString("[object][name]");
                    episode.numberOfViews = Long.valueOf(jsonObject.getString("[object][number_of_viewes]"));
                    episode.creationDate = Long.valueOf(jsonObject.getString("[object][creationDate]"));
                    episode.numberOfFavourites = Long.valueOf(jsonObject.getString("[object][number_of_favourites]"));
                    episode.numberOfLikes = Long.valueOf(jsonObject.getString("[object][number_of_likes]"));
                    episode.numberOfUnlikes = Long.valueOf(jsonObject.getString("[object][number_of_unlikes]"));

                    bundle = new Bundle();
                    bundle.putSerializable("episode", episode);

                    intent = new Intent(this, EpisodeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    intent.putExtras(bundle);

                    final String contentTitle = getResources().getString(R.string.newEpisodeNotificationTitle)
                            + " " + episode.name;

                    getBitmapFromUrlAndGenerateNotification(episode.photoUrl, contentTitle, intent);
                    break;

                case 4:
                    String notificationText = jsonObject.getString("[object]");
                    Uri defaultSoundUri3 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder3 = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.logo)
                            .setContentTitle(notificationText)
                            .setContentText("")
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri3)
                            .setContentIntent(null);

                    NotificationManager notificationManager3 =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager3.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), notificationBuilder3.build());
                    break;

                default:
                    break;
            }
        } catch (
                JSONException e
                )

        {
            e.printStackTrace();
        }

    }


    public void getBitmapFromUrlAndGenerateNotification(String imageUrl, String contentTitle, Intent intent) {
        try {


            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);

            NotificationCompat.BigPictureStyle s1 = new NotificationCompat.BigPictureStyle().bigPicture(bitmap);

            intent.setAction(Long.toString(System.currentTimeMillis()));
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle(contentTitle)
                    .setContentText("")
                    .setAutoCancel(true)
                    .setLargeIcon(bitmap)
                    .setStyle(s1)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(Singleton.getInstance().notificationsNumber++, notificationBuilder.build());

//            URL url = new URL(imageUrl);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            Bitmap bitmap = BitmapFactory.decodeStream(input);
//
//            NotificationCompat.BigPictureStyle s1 = new NotificationCompat.BigPictureStyle().bigPicture(bitmap);
//
//            // Do whatever you want with the Bitmap
//            PendingIntent pendingIntent2 = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0,
//                    intent, PendingIntent.FLAG_ONE_SHOT);
//            Uri defaultSoundUri2 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notificationBuilder2 = new NotificationCompat.Builder(MyFirebaseMessagingService.this)
//                    .setSmallIcon(R.drawable.logo)
//                    .setContentTitle(contentTitle)
//                    .setContentText("")
//                    .setLargeIcon(bitmap)
//                    .setAutoCancel(true)
//                    .setSound(defaultSoundUri2)
//                    .setContentIntent(pendingIntent2)
//                    .setStyle(s1);
//
//            NotificationManager notificationManager2 =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//            notificationManager2.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), notificationBuilder2.build());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}


