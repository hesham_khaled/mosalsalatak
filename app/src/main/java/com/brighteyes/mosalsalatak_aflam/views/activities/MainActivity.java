package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Category;
import com.brighteyes.mosalsalatak_aflam.fcm.MyFirebaseInstanceIDService;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.fragments.MyCategoryFragment;
import com.brighteyes.mosalsalatak_aflam.views.fragments.RecentlyAddedFragment;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.Collections;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AbstractActivity {

    @Bind(R.id.title)
    public TextView titleTextView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.connectionErrorContainer)
    RelativeLayout noConnectionBar;
    @Bind(R.id.profileIconImageView)
    ImageView profileIconImageView;
//
//    @Bind(R.id.favouritesIconImageView)
//    ImageView favouritesIconImageView;

    @Bind(R.id.adView)
    RelativeLayout adContainer;

    @Bind(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @Bind(R.id.removeNoConnectionBarImageView)
    ImageView removeNoConnectionBarImageView;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.viewpager)
    ViewPager viewPager;

    @Bind(R.id.tabs)
    TabLayout tabLayout;

    int selectedTabIndex = -1;
    int size = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        profileIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RequestsActivity.class);
                startActivity(intent);
            }
        });

        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);

        if (sharedPreferences.getString(CommonConstants.USER_FCM_TOKEN, null) == null) {
            final MyFirebaseInstanceIDService idService = new MyFirebaseInstanceIDService(this);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    idService.onTokenRefresh();
                }
            });
        }
    }

    @Override
    void showNoConnectionContainer() {
        noConnectionBar.setVisibility(View.VISIBLE);
    }

    @Override
    void hideNoConnectionContainer() {
        noConnectionBar.setVisibility(View.GONE);
    }

    private void loadCategoriesBannerAd() {
        AdRequest adRequest = new AdRequest.Builder().build();

        if (Singleton.getInstance().adsIds != null && !Singleton.getInstance().adsIds.isEmpty()) {
            AdView mAdView = new AdView(this);
            mAdView.setAdSize(AdSize.SMART_BANNER);
            mAdView.setAdUnitId(Singleton.getInstance().adsIds.get(0).id);
            adContainer.addView(mAdView);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "ad loaded");
                }
            });
            mAdView.loadAd(adRequest);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        openCategoriesSingleValueEventListener();

        if (Singleton.getInstance().adsIds.size() > 5) {
            Singleton.getInstance().mInterstitialAd = new InterstitialAd(this);
            Singleton.getInstance().mInterstitialAd.setAdUnitId(Singleton.getInstance().adsIds.get(5).id);

            Singleton.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    requestNewInterstitial();
                }
            });

            requestNewInterstitial();
        }

        Typeface typeface = App.objectGraph.get(Typeface.class);
        titleTextView.setTypeface(typeface);

        setSupportActionBar(toolbar);

        removeNoConnectionBarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNoConnectionContainer();
            }
        });

//        profileIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
//                    startActivity(intent);
//
//                } else {
//                    Intent intent = new Intent(MainActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(MainActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });
//
//        favouritesIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(MainActivity.this, FavouritesActivity.class);
//                    startActivity(intent);
//                } else {
//                    Intent intent = new Intent(MainActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(MainActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });

        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                            Singleton.getInstance().mInterstitialAd.show();
                        }
                    }
                }, 2000);

            }
        });

        loadCategoriesBannerAd();
    }

    @Override
    protected void onPause() {
        super.onPause();
        selectedTabIndex = tabLayout.getSelectedTabPosition();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        Singleton.getInstance().mInterstitialAd.loadAd(adRequest);
    }

    private void openCategoriesSingleValueEventListener() {

        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();

        Singleton.sDatabase.child("category").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Singleton.sCategories.clear();

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Singleton.sCategories.add(Category.parse(child));
                }

                Collections.reverse(Singleton.sCategories);

                Singleton.sCategories.add(new Category("recentlyAdded", getResources().getString(R.string.recentAdded)));

                if (selectedTabIndex == -1) {
                    selectedTabIndex = Singleton.sCategories.size() - 1;
                }

                PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), Singleton.sCategories.size());
                viewPager.setAdapter(adapter);

                viewPager.setCurrentItem(selectedTabIndex);

                tabLayout.setupWithViewPager(viewPager);
                changeTabsFont();

                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();

                openCategoriesChildValueEventListener();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openCategoriesChildValueEventListener() {
        Singleton.sDatabase.child("category").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                size++;
                if (size + 1 > Singleton.sCategories.size() && Singleton.currentActivity == MainActivity.this) {
                    recreate();
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                for (Category cat : Singleton.sCategories) {
                    if (cat.id.equals(dataSnapshot.getKey())) {
                        cat.name = (String) dataSnapshot.child("name").getValue();
                        if (Singleton.currentActivity == MainActivity.this)
                            recreate();
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                size--;
                if ((size + 1) < Singleton.sCategories.size() && Singleton.currentActivity == MainActivity.this) {
                    recreate();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void updateProfilePicture() {

    }

    private void changeTabsFont() {
        Typeface typeface = App.objectGraph.get(Typeface.class);
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeface);
                }
            }
        }
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Singleton.sCategories.get(position).name;
        }

        @Override
        public Fragment getItem(int position) {
            if (position != Singleton.sCategories.size() - 1) {
                MyCategoryFragment fragment = new MyCategoryFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("category", Singleton.sCategories.get(position));
                fragment.setArguments(bundle);
                return fragment;
            } else {
                RecentlyAddedFragment fragment = new RecentlyAddedFragment();
                return fragment;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}
