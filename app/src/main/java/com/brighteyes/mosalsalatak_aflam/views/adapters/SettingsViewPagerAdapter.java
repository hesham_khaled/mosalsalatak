package com.brighteyes.mosalsalatak_aflam.views.adapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.brighteyes.mosalsalatak_aflam.entities.TabObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2/12/17.
 */

public class SettingsViewPagerAdapter extends FragmentPagerAdapter {

    private List<TabObject> tabObjects = new ArrayList<>();
    Activity context;
    FragmentManager fragmentManager;

    public SettingsViewPagerAdapter(Activity context, FragmentManager fm, List<TabObject> tabObjects) {
        super(fm);
        this.fragmentManager = fm;
        this.tabObjects = tabObjects;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return tabObjects.get(position).fragment;
    }

    @Override
    public int getCount() {
        return tabObjects.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabObjects.get(position).fragmentTitle;
    }
}
