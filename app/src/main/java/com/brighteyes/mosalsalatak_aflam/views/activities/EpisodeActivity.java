package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Comment;
import com.brighteyes.mosalsalatak_aflam.entities.CommentObject;
import com.brighteyes.mosalsalatak_aflam.entities.CommentUser;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.EpisodeComment;
import com.brighteyes.mosalsalatak_aflam.entities.UserEpisodes;
import com.brighteyes.mosalsalatak_aflam.entities.UserLikes;
import com.brighteyes.mosalsalatak_aflam.entities.UserUnlikes;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.NetworkConnection;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.adapters.CommentsRecyclerViewAdapter;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EpisodeActivity extends AbstractActivity {

    @Bind(R.id.title)
    public TextView titleTextView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.connectionErrorContainer)
    RelativeLayout noConnectionBar;

    //    @Bind(R.id.profileIconImageView)
//    ImageView profileIconImageView;
//
//    @Bind(R.id.favouritesIconImageView)
//    ImageView favouritesIconImageView;
    @Bind(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @Bind(R.id.removeNoConnectionBarImageView)
    ImageView removeNoConnectionBarImageView;

    @Bind(R.id.backImageView)
    ImageView backImageView;

    @Bind(R.id.playImageView)
    ImageView playImageView;

    @Bind(R.id.episodeCoverImageView)
    ImageView episodeCoverImageView;

    @Bind(R.id.episodeNameTextView)
    TextView episodeNameTextView;

    @Bind(R.id.viewsTextView)
    TextView viewsTextView;

    @Bind(R.id.commentsTextView)
    TextView commentsNumberTextView;

    @Bind(R.id.favouriteNumberTextView)
    TextView favouriteNumberTextView;

    @Bind(R.id.addFavouriteImageView)
    ImageView addFavouriteImageView;

    @Bind(R.id.unlikeNumberTextView)
    TextView unlikeNumberTextView;

    @Bind(R.id.unlikeImageView)
    ImageView unlikeImageView;

    @Bind(R.id.likeNumberTextView)
    TextView likeNumberTextView;

    @Bind(R.id.likeImageView)
    ImageView likeImageView;

    @Bind(R.id.commentsTitleTextView)
    TextView commentsTitleTextView;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.commentsRecyclerView)
    RecyclerView commentsRecyclerView;

    @Bind(R.id.commentEditText)
    EditText commentEditText;

    @Bind(R.id.addCommentButton)
    Button addCommentButton;

    @Bind(R.id.adView)
    RelativeLayout adContainer;

    Episode episode;
//    ArrayList<Comment> comments = new ArrayList<>();

    EpisodeComment episodeComment;
    ArrayList<CommentUser> commentUsers = new ArrayList<>();
    UserEpisodes userEpisodes;
    UserLikes userLikes;
    UserUnlikes userUnlikes;

    LinearLayoutManager linearLayoutManager;

    Typeface typeface;
    int episodeCommentsSize;
    int commentUsersSize = 0;
    int userLikesSize;
    int userUnlikesSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episode);

        try {
            ButterKnife.bind(this);
        } catch (Exception e) {
            e.printStackTrace();
        }


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        linearLayoutManager = new LinearLayoutManager(this);

        episode = (Episode) getIntent().getExtras().getSerializable("episode");
//        if (getIntent().getExtras().containsKey("isNotification")) {
//            increaseEpisodeViews(episode);
//        }

        typeface = App.objectGraph.get(Typeface.class);
        titleTextView.setTypeface(typeface);


        setSupportActionBar(toolbar);

        if (episode != null) {

            titleTextView.setText(episode.seriesName);

            openEpisodeValueListener(episode.id);
            openEpisodeCommentsSingleValueListener(episode.id);
            SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
            if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
                openUserEpisodesSingleListener();
                openUserLikesSingleListener();
                openUserUnlikesSingleListener();
            }
        }


        /*

        get the value of episode TAMAM
        open the listener of episode (value) TAMAM
        open the listener of episode comments (single + child) TAMAM
        open listener of comments user (value) TAMAM
        open listener of user episodes(Single) TAMAM
        open listener of user likes (single + child) TAMAM
        open listener of user unlikes (single + child) TAMAM

        if episode is in the array of likes ==> put selected image of like, set unselected for unlikes TAMAM

        if episode is in the array of unlikes ==> put selected image of unlike, set unselected for like TAMAM

        if episode in the array of user episodes ==> put selected image of favs, else ==> put unselected TAMAM

        put the array of comments in the recycler view TAMAM

        when add new comment, add to firebase new comment in (comments, episode comments, comment user) TAMAM

        when click like TAMAM

        when click unlike TAMAM

        when click favourite TAMAM

        when click play image view TAMAM
         */

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loadAd();

        openCommentUserSingleValueListener();

        removeNoConnectionBarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNoConnectionContainer();
            }
        });

//        profileIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(EpisodeActivity.this, SettingsActivity.class);
//                    startActivity(intent);
//
//                } else {
//                    Intent intent = new Intent(EpisodeActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(EpisodeActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });
//
//        favouritesIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(EpisodeActivity.this, FavouritesActivity.class);
//                    startActivity(intent);
//                } else {
//                    Intent intent = new Intent(EpisodeActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(EpisodeActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });

        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EpisodeActivity.this, SearchActivity.class);
                startActivity(intent);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                            Singleton.getInstance().mInterstitialAd.show();
                        }
                    }
                }, 2000);
            }
        });

        playImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // go to video screen
                if (NetworkConnection.networkConnectivity(EpisodeActivity.this)) {
                    Intent intent = new Intent(EpisodeActivity.this, VideoPlayerActivity.class);
                    intent.putExtra("episode", episode);
                    startActivity(intent);
                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                            Singleton.getInstance().mInterstitialAd.show();
                        }
                    }
                }, 2000);
            }
        });

        updateEpisodeData();

        addFavouriteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);

                if (!sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
                    Intent intent = new Intent(EpisodeActivity.this, LandingActivity.class);
                    startActivity(intent);

                    Toast.makeText(EpisodeActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
                } else {

                    boolean isExist = false;

                    for (Episode episode : userEpisodes.episodes) {
                        if (EpisodeActivity.this.episode.id.equals(episode.id)) {
                            isExist = true;
                            break;
                        }
                    }

                    if (!isExist) {

                        addFavouriteImageView.setImageResource(R.drawable.favourite_selected);

                        Singleton.sDatabase.child("user_episodes").child(Singleton.getInstance().currentUser.id)
                                .child(episode.id).setValue(Episode.toMap(episode), new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            }
                        });

                        episode.numberOfFavourites++;
                        Singleton.sDatabase.child("episode").child(episode.id).updateChildren(Episode.toMap(episode));
                        userEpisodes.episodes.add(episode);
                    } else {
                        addFavouriteImageView.setImageResource(R.drawable.favourite_white);

                        Singleton.sDatabase.child("user_episodes").child(Singleton.getInstance().currentUser.id)
                                .child(episode.id).removeValue();

                        episode.numberOfFavourites--;
                        Singleton.sDatabase.child("episode").child(episode.id).updateChildren(Episode.toMap(episode));
                        for (int i = 0; i < userEpisodes.episodes.size(); i++) {
                            if (userEpisodes.episodes.get(i).id.equals(EpisodeActivity.this.episode.id)) {
                                userEpisodes.episodes.remove(i);
                                break;
                            }
                        }
                    }

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                                Singleton.getInstance().mInterstitialAd.show();
                            }
                        }
                    }, 2000);

                }
            }
        });

        unlikeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);

                if (!sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
                    Intent intent = new Intent(EpisodeActivity.this, LandingActivity.class);
                    startActivity(intent);

                    Toast.makeText(EpisodeActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
                } else {

                    boolean isExist = false;

                    for (Episode episode : userUnlikes.episodes) {
                        if (EpisodeActivity.this.episode.id.equals(episode.id)) {
                            isExist = true;
                            break;
                        }
                    }

                    if (!isExist) {

                        unlikeImageView.setImageResource(R.drawable.unlike_selected);

                        Singleton.sDatabase.child("user_unlikes").child(Singleton.getInstance().currentUser.id)
                                .child(episode.id).setValue(Episode.toMap(episode), new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            }
                        });

                        episode.numberOfUnlikes++;
                        Singleton.sDatabase.child("episode").child(episode.id).updateChildren(Episode.toMap(episode));

                        boolean isExistInLikes = false;
                        for (Episode episode : userLikes.episodes) {
                            if (EpisodeActivity.this.episode.id.equals(episode.id)) {
                                isExistInLikes = true;
                                break;
                            }
                        }

                        if (isExistInLikes) {
                            likeImageView.setImageResource(R.drawable.like_not_selected);

                            Singleton.sDatabase.child("user_likes").child(Singleton.getInstance().currentUser.id)
                                    .child(episode.id).removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                }
                            });

                            episode.numberOfLikes--;
                            Singleton.sDatabase.child("episode").child(episode.id).updateChildren(Episode.toMap(episode));
                        }

                    } else {

                        unlikeImageView.setImageResource(R.drawable.unlike_not_selected);

                        Singleton.sDatabase.child("user_unlikes").child(Singleton.getInstance().currentUser.id)
                                .child(episode.id).removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            }
                        });

                        episode.numberOfUnlikes--;
                        Singleton.sDatabase.child("episode").child(episode.id).updateChildren(Episode.toMap(episode));

                    }

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                                Singleton.getInstance().mInterstitialAd.show();
                            }
                        }
                    }, 2000);

                }
            }
        });


        likeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);

                if (!sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
                    Intent intent = new Intent(EpisodeActivity.this, LandingActivity.class);
                    startActivity(intent);

                    Toast.makeText(EpisodeActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
                } else {

                    boolean isExist = false;

                    for (Episode episode : userLikes.episodes) {
                        if (EpisodeActivity.this.episode.id.equals(episode.id)) {
                            isExist = true;
                            break;
                        }
                    }

                    if (!isExist) {

                        likeImageView.setImageResource(R.drawable.like_selected);

                        Singleton.sDatabase.child("user_likes").child(Singleton.getInstance().currentUser.id)
                                .child(episode.id).setValue(Episode.toMap(episode), new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            }
                        });

                        episode.numberOfLikes++;
                        Singleton.sDatabase.child("episode").child(episode.id).updateChildren(Episode.toMap(episode));

                        boolean isExistInUnlikes = false;
                        for (Episode episode : userUnlikes.episodes) {
                            if (EpisodeActivity.this.episode.id.equals(episode.id)) {
                                isExistInUnlikes = true;
                                break;
                            }
                        }

                        if (isExistInUnlikes) {
                            unlikeImageView.setImageResource(R.drawable.unlike_not_selected);

                            Singleton.sDatabase.child("user_unlikes").child(Singleton.getInstance().currentUser.id)
                                    .child(episode.id).removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                }
                            });

                            episode.numberOfUnlikes--;
                            Singleton.sDatabase.child("episode").child(episode.id).updateChildren(Episode.toMap(episode));

                        }

                    } else {
                        likeImageView.setImageResource(R.drawable.like_not_selected);
                        Singleton.sDatabase.child("user_likes").child(Singleton.getInstance().currentUser.id)
                                .child(episode.id).removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            }
                        });

                        episode.numberOfLikes--;
                        Singleton.sDatabase.child("episode").child(episode.id).updateChildren(Episode.toMap(episode));

                    }

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                                Singleton.getInstance().mInterstitialAd.show();
                            }
                        }
                    }, 2000);
                }
            }
        });

        commentsTitleTextView.setTypeface(typeface);

        commentEditText.setTypeface(typeface);

        addCommentButton.setTypeface(typeface);

        addCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);

                if (!sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
                    Intent intent = new Intent(EpisodeActivity.this, LandingActivity.class);
                    startActivity(intent);

                    Toast.makeText(EpisodeActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
                } else {

                    //TODO:  when add new comment, add to firebase new comment in (comments, episode comments, comment user)
                    if (!commentEditText.getText().toString().isEmpty()) {

                        Comment comment = new Comment();
                        String commentId = Singleton.sDatabase.child("comment").push().getKey();

                        comment.id = commentId;
                        comment.body = commentEditText.getText().toString();
                        Singleton.sDatabase.child("comment").child(commentId).updateChildren(Comment.toMap(comment));

                        Singleton.sDatabase.child("episode_comments").child(episode.id)
                                .child(comment.id).setValue(Comment.toMap(comment), new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            }
                        });

                        HashMap<String, String> map = new HashMap<>();
                        map.put("photo_url", Singleton.getInstance().currentUser.photoUrl);

                        Singleton.sDatabase.child("comment_user").child(comment.id)
                                .child(Singleton.getInstance().currentUser.id).setValue(map, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            }
                        });

                        commentEditText.setText("");
                    }

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                                Singleton.getInstance().mInterstitialAd.show();
                            }
                        }
                    }, 2000);
                }
            }
        });
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        if (Singleton.getInstance().adsIds != null && !Singleton.getInstance().adsIds.isEmpty()
                && Singleton.getInstance().adsIds.size() >= 3) {
            AdView mAdView = new AdView(this);
            mAdView.setAdSize(AdSize.SMART_BANNER);
            mAdView.setAdUnitId(Singleton.getInstance().adsIds.get(2).id);
            adContainer.addView(mAdView);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "ad loaded");
                }
            });
            mAdView.loadAd(adRequest);
        }
    }

//    ArrayList<CommentUser> commentUsers = new ArrayList<>();

    private void openEpisodeValueListener(String episodeId) {

        final DatabaseReference episodeDatabaseReference = Singleton.sDatabase.
                child("episode").child(episodeId);

        episodeDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                episode = Episode.parse(dataSnapshot);
                updateEpisodeData();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openEpisodeCommentsSingleValueListener(String episodeId) {
        final DatabaseReference episodeCommentsDatabaseReference = Singleton.sDatabase.
                child("episode_comments").child(episodeId);

        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();

        episodeComment = new EpisodeComment();
        episodeComment.episodeId = episodeId;

        episodeCommentsDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot commentDataSnapShot : dataSnapshot.getChildren()) {
                    Comment comment = Comment.parse(commentDataSnapShot);
                    episodeComment.comments.add(comment);
//                    openCommentUserValueListener(comment.id);
                }

                updateEpisodeCommentsData(true);

                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openEpisodeCommentsChildValueListener(String episodeId) {
        final DatabaseReference episodeCommentsDatabaseReference = Singleton.sDatabase.
                child("episode_comments").child(episodeId);

        episodeCommentsDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                episodeCommentsSize++;
                if (episodeComment.comments.size() < episodeCommentsSize) {
                    Comment comment = Comment.parse(dataSnapshot);
                    episodeComment.comments.add(comment);
                    updateEpisodeCommentsData(false);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Comment comment = Comment.parse(dataSnapshot);

                for (int i = 0; i < episodeComment.comments.size(); i++) {
                    if (episodeComment.comments.get(i).id.equals(comment.id)) {
                        episodeComment.comments.remove(i);
                        episodeComment.comments.add(i, comment);
                        break;
                    }
                }

                updateEpisodeCommentsData(false);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                episodeCommentsSize--;
                Comment comment = Comment.parse(dataSnapshot);

                for (int i = 0; i < episodeComment.comments.size(); i++) {
                    if (episodeComment.comments.get(i).id.equals(comment.id)) {
                        episodeComment.comments.remove(i);
                        break;
                    }
                }

                updateEpisodeCommentsData(false);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

//    private void openCommentUserValueListener(final String commentId) {
//        Singleton.sDatabase.child("comment_user").child(commentId).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                CommentUser commentUser = CommentUser.parse(dataSnapshot);
//                commentUser.commentId = commentId;
//                boolean isExist = false;
//                int index;
//                for (index = 0; index < commentUsers.size(); index++) {
//                    if (commentUsers.get(index).commentId.equals(commentUser.commentId)) {
//                        isExist = true;
//                        break;
//                    }
//                }
//
//                if (!isExist) {
//                    commentUsers.add(commentUser);
//                } else {
//                    commentUsers.remove(index);
//                    commentUsers.add(index, commentUser);
//                }
//
//                updateEpisodeCommentsData(false);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }

    private void openCommentUserSingleValueListener() {
        Singleton.sDatabase.child("comment_user").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    commentUsers.add(CommentUser.parse(child));
                }
                updateEpisodeCommentsData(false);
                openCommentUserChildValueListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openCommentUserChildValueListener() {
        Singleton.sDatabase.child("comment_user").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                commentUsersSize++;
                if (commentUsersSize > commentUsers.size()) {
                    CommentUser commentUser = CommentUser.parse(dataSnapshot);
                    commentUsers.add(commentUser);
                    updateEpisodeCommentsData(false);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                ArrayList<CommentUser> temp = commentUsers;
                for (CommentUser commentUser : temp) {
                    if (commentUser.commentId.equals(dataSnapshot.getKey())) {
                        commentUser.userPhotoUrl = (String) dataSnapshot.child(commentUser.commentId)
                                .child(commentUser.userId).child("photo_url").getValue();

                        updateEpisodeCommentsData(false);
                        break;
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                commentUsersSize--;

                ArrayList<CommentUser> temp = commentUsers;
                for (int i = 0; i < temp.size(); i++) {
                    if (commentUsers.get(i).commentId.equals(dataSnapshot.getKey())) {
                        commentUsers.remove(i);

                        updateEpisodeCommentsData(false);
                        break;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openUserEpisodesSingleListener() {

        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();

        final DatabaseReference userEpisodesDatabaseReference = Singleton.sDatabase.
                child("user_episodes").child(Singleton.getInstance().currentUser.id);

        userEpisodesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userEpisodes = new UserEpisodes();
                userEpisodes.userId = Singleton.getInstance().currentUser.id;
                for (DataSnapshot episodeDataSnapshot : dataSnapshot.getChildren()) {
                    Episode episode = Episode.parse(episodeDataSnapshot);
                    userEpisodes.episodes.add(episode);
                }

                updateUserEpisodesData();

                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openUserLikesSingleListener() {

        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();

        final DatabaseReference userLikesDatabaseReference = Singleton.sDatabase.
                child("user_likes").child(Singleton.getInstance().currentUser.id);

        userLikesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userLikes = new UserLikes();
                userLikes.userId = Singleton.getInstance().currentUser.id;
                for (DataSnapshot episodeDataSnapshot : dataSnapshot.getChildren()) {
                    Episode episode = Episode.parse(episodeDataSnapshot);
                    userLikes.episodes.add(episode);
                }

                updateUserLikesData(true);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openUserLikesChildListener() {
        final DatabaseReference userLikesDatabaseReference = Singleton.sDatabase.
                child("user_likes").child(Singleton.getInstance().currentUser.id);

        userLikesDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                userLikesSize++;
                if (userLikes.episodes.size() < userLikesSize) {
                    Episode episode = Episode.parse(dataSnapshot);
                    userLikes.episodes.add(episode);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Episode episode = Episode.parse(dataSnapshot);
                for (int i = 0; i < userLikes.episodes.size(); i++) {
                    if (userLikes.episodes.get(i).id.equals(episode.id)) {
                        userLikes.episodes.remove(i);
                        userLikes.episodes.add(i, episode);
                        break;
                    }
                }

                updateUserLikesData(false);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                userLikesSize--;
                Episode episode = Episode.parse(dataSnapshot);
                for (int i = 0; i < userLikes.episodes.size(); i++) {
                    if (userLikes.episodes.get(i).id.equals(episode.id)) {
                        userLikes.episodes.remove(i);
                        break;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openUserUnlikesSingleListener() {
        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();

        final DatabaseReference userUnlikesDatabaseReference = Singleton.sDatabase.
                child("user_unlikes").child(Singleton.getInstance().currentUser.id);

        userUnlikesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userUnlikes = new UserUnlikes();
                userUnlikes.userId = Singleton.getInstance().currentUser.id;
                for (DataSnapshot episodeDataSnapshot : dataSnapshot.getChildren()) {
                    Episode episode = Episode.parse(episodeDataSnapshot);
                    userUnlikes.episodes.add(episode);
                }

                updateUserUnlikesData(true);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openUserUnlikesChildListener() {
        final DatabaseReference userUnlikesDatabaseReference = Singleton.sDatabase.
                child("user_unlikes").child(Singleton.getInstance().currentUser.id);

        userUnlikesDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                userUnlikesSize++;
                if (userUnlikes.episodes.size() < userUnlikesSize) {
                    Episode episode = Episode.parse(dataSnapshot);
                    userUnlikes.episodes.add(episode);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Episode episode = Episode.parse(dataSnapshot);
                for (int i = 0; i < userUnlikes.episodes.size(); i++) {
                    if (userUnlikes.episodes.get(i).id.equals(episode.id)) {
                        userUnlikes.episodes.remove(i);
                        userUnlikes.episodes.add(i, episode);
                        break;
                    }
                }

                updateUserUnlikesData(false);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                userUnlikesSize--;
                Episode episode = Episode.parse(dataSnapshot);
                for (int i = 0; i < userUnlikes.episodes.size(); i++) {
                    if (userUnlikes.episodes.get(i).id.equals(episode.id)) {
                        userUnlikes.episodes.remove(i);
                        break;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateEpisodeData() {

        Picasso
                .with(this)
                .load(episode.photoUrl)
                .placeholder(R.drawable.logo_bg)
                .into(episodeCoverImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        episodeCoverImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }

                    @Override
                    public void onError() {

                    }
                });

        String episodeName = episode.name;
        episodeNameTextView.setText(episodeName);
        episodeNameTextView.setTypeface(typeface);

        viewsTextView.setTypeface(typeface);
        viewsTextView.setText(String.valueOf(episode.numberOfViews));

        favouriteNumberTextView.setText(String.valueOf(episode.numberOfFavourites));
        favouriteNumberTextView.setTypeface(typeface);

        unlikeNumberTextView.setText(String.valueOf(episode.numberOfUnlikes));
        unlikeNumberTextView.setTypeface(typeface);

        likeNumberTextView.setText(String.valueOf(episode.numberOfLikes));
        likeNumberTextView.setTypeface(typeface);
    }

    private void updateEpisodeCommentsData(boolean isFirst) {
        commentsNumberTextView.setText(String.valueOf(episodeComment.comments.size()));
        commentsNumberTextView.setTypeface(typeface);

        //TODO: update recycler view here
        ArrayList<CommentObject> commentObjects = new ArrayList<>();

        CommentObject commentObject;

        for (Comment comment : episodeComment.comments) {
            for (CommentUser commentUser : commentUsers) {
                if (comment.id.equals(commentUser.commentId)) {
                    commentObject = new CommentObject();
                    commentObject.comment = comment;
                    commentObject.userProfilePicture = commentUser.userPhotoUrl;
                    commentObjects.add(commentObject);
                }
            }
        }

        commentsRecyclerView.setLayoutManager(linearLayoutManager);
        CommentsRecyclerViewAdapter adapter = new CommentsRecyclerViewAdapter(this, commentObjects);
        commentsRecyclerView.setAdapter(adapter);

        linearLayoutManager.scrollToPositionWithOffset(commentObjects.size() - 1, 0);

        commentsRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        if (isFirst) {
            openEpisodeCommentsChildValueListener(episode.id);
        }

    }

    private void updateUserEpisodesData() {
        boolean isExist = false;

        for (Episode episode : userEpisodes.episodes) {
            if (this.episode.id.equals(episode.id)) {
                addFavouriteImageView.setImageResource(R.drawable.favourite_selected);
                isExist = true;
                break;
            }
        }

        if (!isExist) {
            addFavouriteImageView.setImageResource(R.drawable.favourite_white);
        }
    }

    private void updateUserLikesData(boolean isFirst) {
        for (Episode episode : userLikes.episodes) {
            if (this.episode.id.equals(episode.id)) {
                likeImageView.setImageResource(R.drawable.like_selected);
                break;
            }
        }

        if (isFirst) {
            openUserLikesChildListener();
        }
    }

    private void updateUserUnlikesData(boolean isFirst) {
        for (Episode episode : userUnlikes.episodes) {
            if (this.episode.id.equals(episode.id)) {
                unlikeImageView.setImageResource(R.drawable.unlike_selected);
                break;
            }
        }

        if (isFirst) {
            openUserUnlikesChildListener();
        }
    }


    @Override
    void showNoConnectionContainer() {
        noConnectionBar.setVisibility(View.VISIBLE);
    }

    @Override
    void hideNoConnectionContainer() {
        noConnectionBar.setVisibility(View.GONE);
    }

    @Override
    public void updateProfilePicture() {

    }

//    private void increaseEpisodeViews(final Episode episode) {
//        episode.numberOfViews++;
//        Singleton.sDatabase.child("episode").child(episode.id)
//                .updateChildren(Episode.toMap(episode));
//
//        Singleton.sDatabase.child("series_episodes").child(String.valueOf(episode.seriesId))
//                .child(String.valueOf(episode.id))
//                .updateChildren(Episode.toMap(episode));
//
//        Singleton.sDatabase.child("user_episodes").child(Singleton.getInstance().currentUser.id)
//                .child(episode.id).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    Singleton.sDatabase.child("user_episodes").child(Singleton.getInstance().currentUser.id)
//                            .child(episode.id)
//                            .updateChildren(Episode.toMap(episode));
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//        Singleton.sDatabase.child("user_likes").child(Singleton.getInstance().currentUser.id)
//                .child(episode.id).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    Singleton.sDatabase.child("user_likes").child(Singleton.getInstance().currentUser.id)
//                            .child(episode.id)
//                            .updateChildren(Episode.toMap(episode));
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//        Singleton.sDatabase.child("user_unlikes").child(Singleton.getInstance().currentUser.id)
//                .child(episode.id).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    Singleton.sDatabase.child("user_unlikes").child(Singleton.getInstance().currentUser.id)
//                            .child(episode.id)
//                            .updateChildren(Episode.toMap(episode));
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//    }
}
