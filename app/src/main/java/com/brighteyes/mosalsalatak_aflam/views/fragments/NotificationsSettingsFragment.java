package com.brighteyes.mosalsalatak_aflam.views.fragments;


import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsSettingsFragment extends Fragment {


    @Bind(R.id.notificationsSettingsTitle)
    TextView notificationsSettingsTitle;

    @Bind(R.id.notificationsSettingsSubTitle)
    TextView notificationsSettingsSubTitle;

//    @Bind(R.id.settingsRecyclerView)
//    RecyclerView settingsRecyclerView;

    @Bind(R.id.switch1TextView)
    TextView switch1TextView;

    @Bind(R.id.switch1)
    Switch aSwitch;

    private Activity mActivity;

    SharedPreferences sharedPreferences;

    public NotificationsSettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications_settings, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Typeface typeface = App.objectGraph.get(Typeface.class);

        sharedPreferences = App.objectGraph.get(SharedPreferences.class);

        final SharedPreferences.Editor editor = sharedPreferences.edit();

//        openSettingsSingleValueListener();

        notificationsSettingsTitle.setTypeface(typeface);
        notificationsSettingsSubTitle.setTypeface(typeface);
        switch1TextView.setTypeface(typeface);

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editor.putBoolean(CommonConstants.IS_NOTIFICATIONS_ENABLED, true);
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "Notification enabled = " +
                            sharedPreferences.getBoolean(CommonConstants.IS_NOTIFICATIONS_ENABLED, true));
//                       update shared preferences to be get notifications
                } else {
                    editor.putBoolean(CommonConstants.IS_NOTIFICATIONS_ENABLED, false);
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "Notification enabled = " +
                            sharedPreferences.getBoolean(CommonConstants.IS_NOTIFICATIONS_ENABLED, true));
                }

                editor.commit();
            }
        });

        if (sharedPreferences.getBoolean(CommonConstants.IS_NOTIFICATIONS_ENABLED, false)) {
            aSwitch.setChecked(true);
        } else {
            aSwitch.setChecked(false);
        }

//    void openSettingsSingleValueListener() {
//        final DatabaseReference userSettingsDatabaseReference = Singleton.sDatabase.
//                child("user_settings").child(Singleton.getInstance().currentUser.id);
//
//        userSettingsDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                Singleton.getInstance().settings.clear();
//
//                for (DataSnapshot settingDataSnapshot : dataSnapshot.getChildren()) {
//                    Settings settings = Settings.parse(settingDataSnapshot);
//                    Singleton.getInstance().settings.add(settings);
//                }
//
//                updateRecyclerView(true);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }

//        int size = 0;

//    void openSettingsChildValueListener() {
//        final DatabaseReference userSettingsDatabaseReference = Singleton.sDatabase.
//                child("user_settings").child(Singleton.getInstance().currentUser.id);
//
//        userSettingsDatabaseReference.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//
//                size++;
//
//                Settings settings = Settings.parse(dataSnapshot);
//
//                if (Singleton.getInstance().settings.size() < size) {
//                    Singleton.getInstance().settings.add(settings);
//
//                    updateRecyclerView(false);
//                }
//
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Settings settings = Settings.parse(dataSnapshot);
//
//                for (int i = 0; i < Singleton.getInstance().settings.size(); i++) {
//                    if (settings.categoryId.equals(Singleton.getInstance().settings.get(i).categoryId)) {
//                        Singleton.getInstance().settings.remove(i);
//                        Singleton.getInstance().settings.add(i, settings);
//                        break;
//                    }
//                }
//
//                updateRecyclerView(false);
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//                Settings settings = Settings.parse(dataSnapshot);
//
//                for (int i = 0; i < Singleton.getInstance().settings.size(); i++) {
//                    if (settings.categoryId.equals(Singleton.getInstance().settings.get(i).categoryId)) {
//                        Singleton.getInstance().settings.remove(i);
//                        break;
//                    }
//                }
//
//                updateRecyclerView(false);
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }

        //    void updateRecyclerView(boolean isFirst) {
//
//        NotificationSettingsRecyclerViewAdapter adapter = new NotificationSettingsRecyclerViewAdapter(mActivity, Singleton.getInstance().settings);
//
//        settingsRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
//        settingsRecyclerView.setAdapter(adapter);
//
//        if (isFirst) {
//            openSettingsChildValueListener();
//        }
//
//    }
    }
}
