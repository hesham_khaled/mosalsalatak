package com.brighteyes.mosalsalatak_aflam.views.fragments;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Request;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RequestFragment extends Fragment {


    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.requestEditText)
    EditText requestEditText;

    @Bind(R.id.requestTitleTextView)
    TextView requestTitleTextView;

    @Bind(R.id.sendButton)
    Button sendButton;
    private Activity mActivity;

    public RequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_request, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Typeface typeface = App.objectGraph.get(Typeface.class);
        requestEditText.setTypeface(typeface);
        requestTitleTextView.setTypeface(typeface);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (requestEditText.getText().toString().isEmpty()) {
                    Toast.makeText(mActivity, getResources().getString(R.string.requestEmptyError), Toast.LENGTH_LONG).show();
                } else {

                    progressView.setVisibility(View.VISIBLE);
                    progressView.startAnimation();

                    String requestId = Singleton.sDatabase.child("requests").push().getKey();

                    Request request = new Request();
                    request.body = requestEditText.getText().toString();
                    request.isActive = true;

                    Singleton.sDatabase.child("requests").child(requestId)
                            .updateChildren(Request.parse(request), new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    progressView.setVisibility(View.GONE);
                                    progressView.stopAnimation();

                                    Toast.makeText(mActivity, getResources().getString(R.string.requestSentSuccess),
                                            Toast.LENGTH_LONG).show();

                                    requestEditText.setText("");
                                }
                            });

                }
            }
        });
    }
}
