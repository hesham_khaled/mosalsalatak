package com.brighteyes.mosalsalatak_aflam.views.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.views.adapters.CategorySeriesRecyclerViewAdapter;
import com.brighteyes.mosalsalatak_aflam.views.adapters.EpisodesRecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeriesAndEpisodesSearchResultsFragment extends Fragment {


    @Bind(R.id.seriesRecyclerView)
    RecyclerView seriesRecyclerView;

    @Bind(R.id.episodesRecyclerView)
    RecyclerView episodesRecyclerView;
    private Activity mActivity;

    public SeriesAndEpisodesSearchResultsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_series_and_episodes_search_results, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void updateView(ArrayList<Series> seriesArrayList, ArrayList<Episode> episodeArrayList) {


        seriesRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        CategorySeriesRecyclerViewAdapter adapter1 = new CategorySeriesRecyclerViewAdapter(mActivity, seriesArrayList, getResources().getString(R.string.app_name));
        seriesRecyclerView.setAdapter(adapter1);

        episodesRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        EpisodesRecyclerViewAdapter adapter = new EpisodesRecyclerViewAdapter(mActivity, episodeArrayList);
        episodesRecyclerView.setAdapter(adapter);
    }
}
