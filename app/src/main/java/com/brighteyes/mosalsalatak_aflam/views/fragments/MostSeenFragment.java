package com.brighteyes.mosalsalatak_aflam.views.fragments;


import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.views.adapters.EpisodesRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MostSeenFragment extends Fragment {

    @Bind(R.id.episodesRecyclerView)
    RecyclerView episodesRecyclerView;

    @Bind(R.id.loadMoreCardView)
    CardView loadMoreCardView;

    @Bind(R.id.loadMoreTextView)
    TextView loadMoreTextView;

    int page = 1;
    ArrayList<Episode> episodes = new ArrayList<>();
    private ArrayList<Episode> paginatedEpisodeArrayList = new ArrayList<>();

    private Activity mActivity;

    public MostSeenFragment() {
        // Required empty public constructor
    }

    public void setEpisodes(ArrayList<Episode> episodes) {
        this.episodes = episodes;
    }

    public ArrayList<Episode> getEpisodes() {
        return episodes;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_most_seen, container, false);
        ButterKnife.bind(this, view);

        loadMoreCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page++;

                updateView(episodes);
            }
        });

        loadMoreCardView.setVisibility(View.GONE);
        Typeface typeface = App.objectGraph.get(Typeface.class);
        loadMoreTextView.setTypeface(typeface);

        updateView(episodes);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public void updateView(ArrayList<Episode> episodes) {
        
        paginatedEpisodeArrayList.clear();

        if (episodes.size() <= 10 * page) {
            for (int i = 0; i < episodes.size(); i++) {
                paginatedEpisodeArrayList.add(episodes.get(i));
            }
            loadMoreCardView.setVisibility(View.GONE);
        } else {
            for (int i = 0; i < 10 * page; i++) {
                paginatedEpisodeArrayList.add(episodes.get(i));
            }
            loadMoreCardView.setVisibility(View.VISIBLE);
        }

        ArrayList<Episode> temp = paginatedEpisodeArrayList;


        Collections.sort(temp, new Comparator<Episode>() {
            @Override
            public int compare(Episode episode2, Episode episode1) {

                return (int) episode1.numberOfViews - (int) episode2.numberOfViews;
            }
        });

        EpisodesRecyclerViewAdapter addedVideosAdapter = new EpisodesRecyclerViewAdapter(mActivity, temp);
        episodesRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        episodesRecyclerView.setAdapter(addedVideosAdapter);

        episodesRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    public ArrayList<Episode> sortEpisodesByViews(ArrayList<Episode> episodes) {
        Episode temp;
        for (int i = 0; i < episodes.size(); i++) {
            for (int j = i + 1; j < (episodes.size() - 1); j++) {
                if (episodes.get(j - 1).numberOfViews > episodes.get(j).numberOfViews) {
                    temp = episodes.get(j - 1);
                    episodes.set(j - 1, episodes.get(j));
                    episodes.set(j, temp);
                }
            }
        }

        return episodes;
    }

}
