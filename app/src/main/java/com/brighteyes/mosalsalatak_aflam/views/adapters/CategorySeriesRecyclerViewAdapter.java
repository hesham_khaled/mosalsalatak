package com.brighteyes.mosalsalatak_aflam.views.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.activities.SeriesActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by user on 1/9/17.
 */

public class CategorySeriesRecyclerViewAdapter extends RecyclerView.Adapter<CategorySeriesRecyclerViewAdapter.ViewHolder> {

//    private static final int AD_TYPE = 1;
//    private static final int CONTENT_TYPE = 2;
//    private int count = 0;

    String categoryName;
    private LayoutInflater inflater;
    private Typeface typeface;
    private ViewHolder viewHolder;
    private Activity mContext;
    private ArrayList<Series> seriesArrayList;

    public CategorySeriesRecyclerViewAdapter(Activity context, ArrayList<Series> seriesArrayList, String categoryName) {
        mContext = context;
        this.seriesArrayList = seriesArrayList;
        inflater = LayoutInflater.from(mContext);
        typeface = App.objectGraph.get(Typeface.class);
        this.categoryName = categoryName;

    }

    @Override
    public CategorySeriesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.category_series_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

//    @Override
//    public int getItemViewType(int position) {
//        if (position % 5 == 0 && position != 0)
//            return AD_TYPE;
//        return CONTENT_TYPE;
//    }

    @Override
    public void onBindViewHolder(CategorySeriesRecyclerViewAdapter.ViewHolder holder, int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        ordinaryHolder.seriesNameTextView.setTypeface(typeface);
        ordinaryHolder.seriesNameTextView.setText(seriesArrayList.get(position).name);

        ordinaryHolder.viewsTextView.setText(seriesArrayList.get(position).numberOfViews + " مشاهدة");
        ordinaryHolder.viewsTextView.setTypeface(typeface);

        if (seriesArrayList.get(position).numberOfVideos < 1000) {
            ordinaryHolder.episodesNumberTextView.setText(seriesArrayList.get(position).numberOfVideos + " فيديو");
        } else {
            ordinaryHolder.episodesNumberTextView.setText((seriesArrayList.get(position).numberOfVideos / 1000) + "k فيديو");
        }

        ordinaryHolder.episodesNumberTextView.setTypeface(typeface);

        Picasso
                .with(mContext)
                .load(seriesArrayList.get(position).photoUrl)
                .placeholder(R.drawable.logo)
                .into(ordinaryHolder.seriesImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        ordinaryHolder.seriesImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }

                    @Override
                    public void onError() {

                    }
                });

    }

    @Override
    public int getItemCount() {
        return seriesArrayList.size();
    }

    public void updateView(ArrayList<Series> series) {
        seriesArrayList = series;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.seriesNameTextView)
        TextView seriesNameTextView;

        @Bind(R.id.viewsTextView)
        TextView viewsTextView;

        @Bind(R.id.episodesNumberTextView)
        TextView episodesNumberTextView;

        @Bind(R.id.seriesImageView)
        ImageView seriesImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Singleton.getInstance().mInterstitialAd != null &&
                            Singleton.getInstance().mInterstitialAd.isLoaded()) {
                        Singleton.getInstance().mInterstitialAd.show();
                    }

                    Intent intent = new Intent(mContext, SeriesActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("series", seriesArrayList.get(getLayoutPosition()));
                    bundle.putString("categoryName", categoryName);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);

                    seriesArrayList.get(getLayoutPosition()).numberOfViews++;
                    Singleton.sDatabase.child("series").child(seriesArrayList.get(getLayoutPosition()).id)
                            .updateChildren(Series.toMap(seriesArrayList.get(getLayoutPosition()), true));

                    Singleton.sDatabase.child("category_series")
                            .child(String.valueOf(seriesArrayList.get(getLayoutPosition()).categoryId))
                            .child(String.valueOf(seriesArrayList.get(getLayoutPosition()).id))
                            .updateChildren(Series.toMap(seriesArrayList.get(getLayoutPosition()), false));
                }
            });

        }

        void emptyViewHolder() {
            seriesNameTextView.setText("");
            seriesNameTextView.setTypeface(typeface);

            viewsTextView.setText("");
            viewsTextView.setTypeface(typeface);

            episodesNumberTextView.setText("");
            episodesNumberTextView.setTypeface(typeface);


        }
    }
}
