package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.entities.SeriesEpisodes;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.fragments.NewestEpisodesFragment;
import com.brighteyes.mosalsalatak_aflam.views.fragments.OldestEpisodesFragment;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SeriesActivity extends AbstractActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.connectionErrorContainer)
    RelativeLayout noConnectionBar;

    @Bind(R.id.title)
    public TextView titleTextView;

//    @Bind(R.id.profileIconImageView)
//    ImageView profileIconImageView;
//
//    @Bind(R.id.favouritesIconImageView)
//    ImageView favouritesIconImageView;

    @Bind(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @Bind(R.id.removeNoConnectionBarImageView)
    ImageView removeNoConnectionBarImageView;

    @Bind(R.id.tabs)
    TabLayout tabLayout;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.viewpager)
    ViewPager viewPager;

    @Bind(R.id.backImageView)
    ImageView backImageView;

    @Bind(R.id.seriesCoverImageView)
    ImageView seriesCoverImageView;

    @Bind(R.id.playsTextView)
    TextView playsTextView;

    @Bind(R.id.viewsTextView)
    TextView viewsTextView;

    @Bind(R.id.seriesNameTextView)
    TextView seriesNameTextView;

    @Bind(R.id.adView)
    RelativeLayout adContainer;


    Series series;
    ArrayList<Episode> episodes = new ArrayList<>();
    String categoryName;
    SeriesEpisodes seriesEpisodes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_series);
        ButterKnife.bind(this);

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey("series")) {
            series = (Series) getIntent().getExtras().getSerializable("series");
            if (getIntent().getExtras().containsKey("categoryName")) {
                categoryName = getIntent().getExtras().getString("categoryName");
            }
        } else {
            finish();
        }

        Typeface typeface = App.objectGraph.get(Typeface.class);
        titleTextView.setText(categoryName);
        titleTextView.setTypeface(typeface);

        setSupportActionBar(toolbar);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        removeNoConnectionBarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNoConnectionContainer();
            }
        });

//        profileIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(SeriesActivity.this, SettingsActivity.class);
//                    startActivity(intent);
//
//                } else {
//                    Intent intent = new Intent(SeriesActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(SeriesActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });
//
//        favouritesIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(SeriesActivity.this, FavouritesActivity.class);
//                    startActivity(intent);
//                } else {
//                    Intent intent = new Intent(SeriesActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(SeriesActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });

        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SeriesActivity.this, SeriesActivity.class);
                startActivity(intent);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                            Singleton.getInstance().mInterstitialAd.show();
                        }
                    }
                }, 2000);
            }
        });

        loadAd();

        openSeriesEpisodesListener(series.id);
        openSeriesListener(series.id);

        Picasso
                .with(this)
                .load(series.photoUrl)
                .placeholder(R.drawable.logo_bg)
                .into(seriesCoverImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        seriesCoverImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }

                    @Override
                    public void onError() {

                    }
                });

        viewsTextView.setTypeface(typeface);
        viewsTextView.setText(String.valueOf(series.numberOfViews));

        playsTextView.setText(String.valueOf(series.numberOfVideos));
        playsTextView.setTypeface(typeface);

        seriesNameTextView.setText(series.name);
        seriesNameTextView.setTypeface(typeface);
    }

    int currentTab = -1;

    @Override
    protected void onPause() {
        super.onPause();
        currentTab = viewPager.getCurrentItem();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (currentTab != -1) {
            viewPager.setCurrentItem(currentTab);
        }
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        if (Singleton.getInstance().adsIds != null && !Singleton.getInstance().adsIds.isEmpty()
                && Singleton.getInstance().adsIds.size() >= 2) {
            AdView mAdView = new AdView(this);
            mAdView.setAdSize(AdSize.SMART_BANNER);
            mAdView.setAdUnitId(Singleton.getInstance().adsIds.get(1).id);
            adContainer.addView(mAdView);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "ad loaded");
                }
            });
            mAdView.loadAd(adRequest);
        }
    }

    private void changeTabsFont() {
        Typeface typeface = App.objectGraph.get(Typeface.class);
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeface);
                }
            }
        }
    }

    private void setupViewPager(boolean isFirst) {

        try {

            if (viewPager != null) {
                episodes.clear();

                for (Episode episode : seriesEpisodes.episodes) {
                    episodes.add(episode);
                }

                viewPager.setOffscreenPageLimit(3);

                PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), 2);
                viewPager.setAdapter(pagerAdapter);

                viewPager.setCurrentItem(1);

                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();

                if (isFirst) {
                    openSeriesEpisodesChildListener(series.id);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateMovieData() {
        Typeface typeface = App.objectGraph.get(Typeface.class);
        viewsTextView.setTypeface(typeface);
        viewsTextView.setText(String.valueOf(series.numberOfViews));

        playsTextView.setText(String.valueOf(series.numberOfVideos));
        playsTextView.setTypeface(typeface);

        seriesNameTextView.setText(series.name);
        seriesNameTextView.setTypeface(typeface);
    }

    public void openSeriesEpisodesListener(final String seriesId) {

        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();

        seriesEpisodes = new SeriesEpisodes();

        final DatabaseReference seriesEpisodesDatabaseReference = Singleton.sDatabase.
                child("series_episodes").child(seriesId);

        seriesEpisodesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                seriesEpisodes.episodes.clear();
                seriesEpisodes.seriesId = seriesId;

                for (DataSnapshot episodeDataSnapshot : dataSnapshot.getChildren()) {
                    Episode episode = Episode.parse(episodeDataSnapshot);
                    seriesEpisodes.episodes.add(episode);
                }

                setupViewPager(true);
                tabLayout.setupWithViewPager(viewPager);
                changeTabsFont();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    int size = 0;

    public void openSeriesEpisodesChildListener(String seriesId) {
        final DatabaseReference seriesEpisodesDatabaseReference = Singleton.sDatabase.
                child("series_episodes").child(seriesId);

        seriesEpisodesDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                size++;
                Episode episode = Episode.parse(dataSnapshot);

                if (seriesEpisodes.episodes.size() < size) {
                    seriesEpisodes.episodes.add(episode);
                    setupViewPager(false);
                    tabLayout.setupWithViewPager(viewPager);
                    changeTabsFont();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Episode episode = Episode.parse(dataSnapshot);

                ArrayList<Episode> temp = seriesEpisodes.episodes;

                for (Episode episode : temp) {
                    if (episode.id.equals(dataSnapshot.getKey())) {
                        episode.name = (String) dataSnapshot.child("name").getValue();
                        episode.photoUrl = (String) dataSnapshot.child("photo_url").getValue();
                        episode.photoUrl = (String) dataSnapshot.child("video_url").getValue();
                        episode.seriesName = (String) dataSnapshot.child("seriesName").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("creationDate").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("number_of_viewes").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("number_of_favourites").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("number_of_likes").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("number_of_unlikes").getValue();

                        setupViewPager(false);
                        tabLayout.setupWithViewPager(viewPager);
                        changeTabsFont();

                        break;

                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                size--;
                ArrayList<Episode> temp = seriesEpisodes.episodes;

                for (int i = 0; i < temp.size(); i++) {
                    if (seriesEpisodes.episodes.get(i).id.equals(dataSnapshot.getKey())) {
                        seriesEpisodes.episodes.remove(i);

                        setupViewPager(false);
                        tabLayout.setupWithViewPager(viewPager);
                        changeTabsFont();
                        break;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void openSeriesListener(String seriesId) {
        final DatabaseReference seriesDatabaseReference = Singleton.sDatabase.
                child("series").child(seriesId);

        seriesDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Series series = Series.parse(dataSnapshot);
                SeriesActivity.this.series = series;
                updateMovieData();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    void showNoConnectionContainer() {
        noConnectionBar.setVisibility(View.VISIBLE);
    }

    @Override
    void hideNoConnectionContainer() {
        noConnectionBar.setVisibility(View.GONE);
    }

    @Override
    public void updateProfilePicture() {

    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {

                case 0:
//                    return getResources().getString(R.string.mostSeen);
                    return getResources().getString(R.string.oldest);

                case 1:
//                    return getResources().getString(R.string.oldest);
                    return getResources().getString(R.string.newest);

//                case 2:
//                    return getResources().getString(R.string.newest);

                default:
                    return null;

            }
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {

//                case 0:
//                    MostSeenFragment mostSeenFragment = new MostSeenFragment();
//                    mostSeenFragment.setEpisodes(episodes);
//                    return mostSeenFragment;

                case 0:
                    OldestEpisodesFragment oldestEpisodesFragment = new OldestEpisodesFragment();
                    oldestEpisodesFragment.setEpisodes(episodes);
                    return oldestEpisodesFragment;

                case 1:
                    NewestEpisodesFragment newestEpisodesFragment = new NewestEpisodesFragment();
                    newestEpisodesFragment.setEpisodes(episodes);
                    return newestEpisodesFragment;

                default:
                    return null;

            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}
