package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Request;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RequestActivity extends AbstractActivity {

    @Bind(R.id.title)
    public TextView titleTextView;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.connectionErrorContainer)
    RelativeLayout noConnectionBar;

//    @Bind(R.id.profileIconImageView)
//    ImageView profileIconImageView;
//
//    @Bind(R.id.favouritesIconImageView)
//    ImageView favouritesIconImageView;

    @Bind(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @Bind(R.id.removeNoConnectionBarImageView)
    ImageView removeNoConnectionBarImageView;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.tabs)
    TabLayout tabLayout;

    @Bind(R.id.requestEditText)
    EditText requestEditText;

    @Bind(R.id.requestTitleTextView)
    TextView requestTitleTextView;

    @Bind(R.id.sendButton)
    Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        ButterKnife.bind(this);

        Typeface typeface = App.objectGraph.get(Typeface.class);
        titleTextView.setTypeface(typeface);
        titleTextView.setText("طلباتك");

        requestEditText.setTypeface(typeface);
        requestTitleTextView.setTypeface(typeface);

        setSupportActionBar(toolbar);

        tabLayout.setVisibility(View.GONE);


        removeNoConnectionBarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNoConnectionContainer();
            }
        });

//        profileIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(RequestActivity.this, SettingsActivity.class);
//                    startActivity(intent);
//
//                } else {
//                    Intent intent = new Intent(RequestActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(RequestActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });
//
//        favouritesIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(RequestActivity.this, FavouritesActivity.class);
//                    startActivity(intent);
//                } else {
//                    Intent intent = new Intent(RequestActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(RequestActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });

        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                            Singleton.getInstance().mInterstitialAd.show();
                        }
                    }
                }, 2000);
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (requestEditText.getText().toString().isEmpty()) {
                    Toast.makeText(RequestActivity.this, getResources().getString(R.string.requestEmptyError), Toast.LENGTH_LONG).show();
                } else {

                    progressView.setVisibility(View.VISIBLE);
                    progressView.startAnimation();

                    String requestId = Singleton.sDatabase.child("requests").push().getKey();

                    Request request = new Request();
                    request.body = requestEditText.getText().toString();
                    request.isActive = true;

                    Singleton.sDatabase.child("requests").child(requestId)
                            .updateChildren(Request.parse(request), new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    progressView.setVisibility(View.GONE);
                                    progressView.stopAnimation();

                                    Toast.makeText(RequestActivity.this, getResources().getString(R.string.requestSentSuccess),
                                            Toast.LENGTH_LONG).show();

                                    requestEditText.setText("");
                                }
                            });

                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                            Singleton.getInstance().mInterstitialAd.show();
                        }
                    }
                }, 2000);
            }
        });
    }

    @Override
    void showNoConnectionContainer() {
        noConnectionBar.setVisibility(View.VISIBLE);
    }

    @Override
    void hideNoConnectionContainer() {
        noConnectionBar.setVisibility(View.GONE);
    }

    @Override
    public void updateProfilePicture() {

    }
}
