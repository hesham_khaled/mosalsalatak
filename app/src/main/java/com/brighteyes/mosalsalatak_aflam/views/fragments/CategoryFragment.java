package com.brighteyes.mosalsalatak_aflam.views.fragments;


import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.CategorySeries;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.adapters.CategorySeriesRecyclerViewAdapter;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment {

    @Bind(R.id.categorySeriesRecyclerView)
    RecyclerView categorySeriesRecyclerView;

    @Bind(R.id.loadMoreCardView)
    CardView loadMoreCardView;

    @Bind(R.id.loadMoreTextView)
    TextView loadMoreTextView;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    public String categoryId;
    public String categoryName;
    ArrayList<Series> seriesArrayList = new ArrayList<>();
    ArrayList<Series> paginatedSeriesArrayList = new ArrayList<>();

    int page = 1;
    CategorySeriesRecyclerViewAdapter adapter;

    CategorySeries categorySeries;
    private Activity mActivity;

    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        openCategorySeriesListener(categoryId);

        loadMoreCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page++;

                updateView();

                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();
            }
        });

        loadMoreCardView.setVisibility(View.GONE);
        Typeface typeface = App.objectGraph.get(Typeface.class);
        loadMoreTextView.setTypeface(typeface);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    void updateView() {
        paginatedSeriesArrayList.clear();

        if (seriesArrayList.size() <= (4 * page)) {
            for (int i = 0; i < seriesArrayList.size(); i++) {
                paginatedSeriesArrayList.add(seriesArrayList.get(i));
            }
            loadMoreCardView.setVisibility(View.GONE);
        } else {
            for (int i = 0; i < 4 * page; i++) {
                paginatedSeriesArrayList.add(seriesArrayList.get(i));
            }
            loadMoreCardView.setVisibility(View.VISIBLE);
        }

        categorySeriesRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        adapter = new CategorySeriesRecyclerViewAdapter(mActivity,
                paginatedSeriesArrayList, categoryName);
        categorySeriesRecyclerView.setAdapter(adapter);
    }

    public void categorySeriesChanged(CategorySeries categorySeries) {

        if (categorySeries.categoryId.equals(categoryId)) {

            seriesArrayList = categorySeries.series;

            updateView();

            progressView.setVisibility(View.GONE);
            progressView.stopAnimation();

            openCategorySeriesChildListener();
        }
    }

    public void openCategorySeriesListener(final String categoryId) {

        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();

        categorySeries = new CategorySeries();

        Singleton.sDatabase.child("category_series").child(categoryId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                categorySeries.series.clear();

                categorySeries.categoryId = categoryId;

                for (DataSnapshot seriesDataSnapshot : dataSnapshot.getChildren()) {
                    Series series = Series.parse(seriesDataSnapshot);
                    series.categoryId = categoryId;
                    categorySeries.series.add(series);
                }

                categorySeriesChanged(categorySeries);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.d(CommonConstants.APPLICATION_LOG_TAG, databaseError.getMessage());
                Toast.makeText(mActivity, databaseError.getMessage(), Toast.LENGTH_LONG).show();

                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();
            }
        });
//        final CategorySeries categorySeries = new CategorySeries();
//        ValueEventListener listener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                categorySeries.categoryId = categoryId;
//
//                for (DataSnapshot seriesDataSnapshot : dataSnapshot.getChildren()) {
//                    Series series = Series.parse(seriesDataSnapshot);
//                    categorySeries.series.add(series);
//                }
//
////                Intent intent = new Intent();
////                intent.setAction(mActivity.getResources().getString(R.string.categorySeriesChanged));
////                Bundle bundle = new Bundle();
////                bundle.putSerializable("category_series", categorySeries);
////                intent.putExtras(bundle);
////                mActivity.sendBroadcast(intent);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
////                Intent intent = new Intent();
////                intent.setAction(mActivity.getResources().getString(R.string.categorySeriesChanged));
////                Bundle bundle = new Bundle();
////                bundle.putString("error", databaseError.getMessage());
////                intent.putExtras(bundle);
////                mActivity.sendBroadcast(intent);
//            }
//        };


//        if (!isClose) {
//            categorySeriesDatabaseReference.addValueEventListener(listener);
//        } else {
//            categorySeriesDatabaseReference.removeEventListener(listener);
//        }
    }

    int size = 0;

    private void openCategorySeriesChildListener() {
        Singleton.sDatabase.
                child("category_series").child(categoryId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                size++;
                Series series = Series.parse(dataSnapshot);
                series.categoryId = categorySeries.categoryId;
                if (seriesArrayList.size() < size) {

                    if (seriesArrayList.size() <= 3) {
                        seriesArrayList.add(series);
                        updateView();
                    } else {
                        seriesArrayList.add(series);
                        loadMoreCardView.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Series series = Series.parse(dataSnapshot);
                series.categoryId = categorySeries.categoryId;
                for (int i = 0; i < seriesArrayList.size(); i++) {
                    if (seriesArrayList.get(i).id.equals(series.id)) {
                        seriesArrayList.remove(i);
                        seriesArrayList.add(i, series);
                        break;
                    }
                }

                updateView();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                size--;
                Series series = Series.parse(dataSnapshot);
                series.categoryId = categorySeries.categoryId;
                for (int i = 0; i < seriesArrayList.size(); i++) {
                    if (seriesArrayList.get(i).id.equals(series.id)) {
                        seriesArrayList.remove(i);
                        break;
                    }
                }

                updateView();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        FragmentManager manager = getFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove(this);
        trans.commit();
    }
}
