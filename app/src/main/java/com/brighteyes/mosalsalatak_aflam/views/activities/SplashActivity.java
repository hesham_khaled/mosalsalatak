package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.AdsId;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.entities.Settings;
import com.brighteyes.mosalsalatak_aflam.entities.User;
import com.brighteyes.mosalsalatak_aflam.interfaces.CategoryObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.UserObserverListener;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.FirebaseDataHelper;
import com.brighteyes.mosalsalatak_aflam.utils.NetworkConnection;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SplashActivity extends AbstractActivity implements UserObserverListener, CategoryObserverListener {

    final long SPLASH_SCREEN_DELAY_TIME = 4000;
    boolean isInBackground = false;

    Handler handler = new Handler();
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    FirebaseDataHelper firebaseDataHelper1;

    @Bind(R.id.connectionErrorContainer)
    RelativeLayout noConnectionBar;

    @Bind(R.id.removeNoConnectionBarImageView)
    ImageView removeNoConnectionBarImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        firebaseReceiver.registerUserObserver(this);
        firebaseDataHelper1 = new FirebaseDataHelper(this);
        sharedPreferences = App.objectGraph.get(SharedPreferences.class);
        editor = sharedPreferences.edit();


        if (NetworkConnection.networkConnectivity(this)) {
            openAdIdsSingleValueListener();
        } else {
            noConnectionBar.setVisibility(View.VISIBLE);
            removeNoConnectionBarImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

        }

        setEnglishLocale();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isInBackground = false;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isInBackground) {
                    goToSpecifiedScreen();
                }
            }
        }, SPLASH_SCREEN_DELAY_TIME / 2);
    }

    private void goToSpecifiedScreen() {
        //Save ads ids in shared preferences.
        if (Singleton.getInstance().adsIds != null && !Singleton.getInstance().adsIds.isEmpty() &&
                Singleton.getInstance().adsIds.size() >= 7) {
            editor.putString(CommonConstants.CATEGORIES_AD, Singleton.getInstance().adsIds.get(0).id);
            editor.putString(CommonConstants.SERIES_AD, Singleton.getInstance().adsIds.get(1).id);
            editor.putString(CommonConstants.EPISODE_AD, Singleton.getInstance().adsIds.get(2).id);
            editor.putString(CommonConstants.SEARCH_RESULTS_AD, Singleton.getInstance().adsIds.get(3).id);
            editor.putString(CommonConstants.FAVOURITES_AD, Singleton.getInstance().adsIds.get(4).id);
            editor.putString(CommonConstants.FULL_SCREEN_AD, Singleton.getInstance().adsIds.get(5).id);
            editor.putString(CommonConstants.VIDEO_AD, Singleton.getInstance().adsIds.get(6).id);
            editor.commit();
        }

        if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
            Singleton.getInstance().currentUser = new User();
            Singleton.getInstance().currentUser.name = sharedPreferences.getString(CommonConstants.USER_NAME, null);
            Singleton.getInstance().currentUser.email = sharedPreferences.getString(CommonConstants.USER_EMAIL, null);
            Singleton.getInstance().currentUser.id = sharedPreferences.getString(CommonConstants.USER_ID, null);
            Singleton.getInstance().currentUser.password = sharedPreferences.getString(CommonConstants.USER_PASSWORD, null);
            Singleton.getInstance().currentUser.nationality = sharedPreferences.getString(CommonConstants.USER_NATIONALITY, null);
            Singleton.getInstance().currentUser.photoUrl = sharedPreferences.getString(CommonConstants.USER_PHOTO_URL, "");
            Singleton.getInstance().currentUser.birthDate = sharedPreferences.getString(CommonConstants.USER_BIRTH_DATE, null);
            Singleton.getInstance().currentUser.gender = sharedPreferences.getString(CommonConstants.USER_GENDER, null);
            Singleton.getInstance().currentUser.isAdmin = sharedPreferences.getBoolean(CommonConstants.USER_IS_ADMIN, false);
            Singleton.getInstance().currentUser.fcmToken = sharedPreferences.getString(CommonConstants.USER_FCM_TOKEN, null);
        }

        if (NetworkConnection.networkConnectivity(this) && Singleton.getInstance().currentUser != null) {

            firebaseDataHelper1.openUserListener(Singleton.getInstance().currentUser.email,
                    Singleton.getInstance().currentUser.password);

            openSettingsSingleValueListener();
            openSeriesSingleValueListener();
            openEpisodeSingleValueListener();
        } else {

            if (Singleton.getInstance().currentUser == null) {
                openSeriesSingleValueListener();
                openEpisodeSingleValueListener();
            } else {
                //show dialog of no internet connection
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isInBackground = true;
    }

    @Override
    void showNoConnectionContainer() {

    }

    @Override
    void hideNoConnectionContainer() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isInBackground = true;
    }

    private void setEnglishLocale() {
        Locale locale = new Locale("en");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    public void userChanged() {


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        firebaseReceiver.unregisterUserObserver(this);
    }

    @Override
    public void updateProfilePicture() {

    }

    void openSettingsSingleValueListener() {
        final DatabaseReference userSettingsDatabaseReference = Singleton.sDatabase.
                child("user_settings").child(Singleton.getInstance().currentUser.id);

        userSettingsDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Singleton.getInstance().settings.clear();

                for (DataSnapshot settingDataSnapshot : dataSnapshot.getChildren()) {
                    Settings settings = Settings.parse(settingDataSnapshot);
                    Singleton.getInstance().settings.add(settings);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void openSeriesSingleValueListener() {
        final DatabaseReference seriesDatabaseReference = Singleton.sDatabase.
                child("series");

        seriesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Singleton.getInstance().seriesArrayList.add(Series.parse(child));
                }

                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void openEpisodeSingleValueListener() {
        Singleton.sDatabase.
                child("episode").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Singleton.getInstance().episodeArrayList.add(Episode.parse(child));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void categoryChanged() {

    }

    private void openAdIdsSingleValueListener() {

        Singleton.getInstance().adsIds.clear();

        Singleton.sDatabase.child("ads").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren())
                    Singleton.getInstance().adsIds.add(AdsId.parse(child));

                openAdIdsChildValueListener();
                goToSpecifiedScreen();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "ads calling cancelled.");
                Log.d(CommonConstants.APPLICATION_LOG_TAG, databaseError.getMessage());

                goToSpecifiedScreen();
            }
        });
    }

    int size = 0;

    private void openAdIdsChildValueListener() {
        Singleton.sDatabase.child("ads").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                size++;

                AdsId adsId = AdsId.parse(dataSnapshot);

                if (Singleton.getInstance().adsIds.size() < size) {
                    Singleton.getInstance().adsIds.add(adsId);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                openAdIdsSingleValueListener();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                openAdIdsSingleValueListener();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
