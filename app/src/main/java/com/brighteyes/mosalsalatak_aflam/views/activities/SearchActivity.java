package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.entities.TabObject;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.adapters.SettingsViewPagerAdapter;
import com.brighteyes.mosalsalatak_aflam.views.fragments.EpisodesSearchResultsFragment;
import com.brighteyes.mosalsalatak_aflam.views.fragments.SeriesAndEpisodesSearchResultsFragment;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SearchActivity extends AbstractActivity {

    @Bind(R.id.title)
    public TextView titleTextView;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.connectionErrorContainer)
    RelativeLayout noConnectionBar;

//    @Bind(R.id.profileIconImageView)
//    ImageView profileIconImageView;
//
//    @Bind(R.id.favouritesIconImageView)
//    ImageView favouritesIconImageView;

    @Bind(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @Bind(R.id.removeNoConnectionBarImageView)
    ImageView removeNoConnectionBarImageView;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.tabs)
    TabLayout tabLayout;

    @Bind(R.id.searchEditText)
    EditText searchEditText;

    @Bind(R.id.searchImageView)
    ImageView searchImageView;

    @Bind(R.id.searchTypesTabs)
    TabLayout searchTypesTabs;

    @Bind(R.id.viewpager)
    ViewPager viewPager;

    @Bind(R.id.backImageView)
    ImageView backImageView;

    @Bind(R.id.adView)
    RelativeLayout adContainer;

    ArrayList<Episode> episodesSearchResults = new ArrayList<>();
    ArrayList<Series> seriesSearchResults = new ArrayList<>();

    EpisodesSearchResultsFragment episodesSearchResultsFragment;

    SeriesAndEpisodesSearchResultsFragment seriesAndEpisodesSearchResultsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);

        Typeface typeface = App.objectGraph.get(Typeface.class);
        titleTextView.setTypeface(typeface);
        titleTextView.setText("صفحة البحث");

        tabLayout.setVisibility(View.GONE);

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "series size: " + Singleton.getInstance().seriesArrayList.size() + "");
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "episodes size: " + Singleton.getInstance().episodeArrayList.size() + "");

        if (Singleton.getInstance().seriesArrayList.isEmpty()) {
            openSeriesSingleValueListener();
        } else {
            openSeriesChildValueEventListener();
        }

        if (Singleton.getInstance().episodeArrayList.isEmpty()) {
            openEpisodeSingleValueListener();
        } else {
            openEpisodeChildValueListener();
        }


        backImageView.setVisibility(View.VISIBLE);
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        removeNoConnectionBarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNoConnectionContainer();
            }
        });

//        profileIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(SearchActivity.this, SettingsActivity.class);
//                    startActivity(intent);
//
//                } else {
//                    Intent intent = new Intent(SearchActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(SearchActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });
//
//        favouritesIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//                if (sharedPreferences.getBoolean(CommonConstants.IS_LOGGED_IN, false)) {
//                    Intent intent = new Intent(SearchActivity.this, FavouritesActivity.class);
//                    startActivity(intent);
//                } else {
//                    Intent intent = new Intent(SearchActivity.this, LandingActivity.class);
//                    startActivity(intent);
//
//                    Toast.makeText(SearchActivity.this, "من فضلك قم بالتسجيل اولا !", Toast.LENGTH_LONG).show();
//                }
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });

        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                            Singleton.getInstance().mInterstitialAd.show();
                        }
                    }
                }, 2000);
            }
        });

        searchImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!searchEditText.getText().toString().isEmpty()) {
                    progressView.setVisibility(View.VISIBLE);
                    progressView.startAnimation();
                    search();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                                Singleton.getInstance().mInterstitialAd.show();
                            }
                        }
                    }, 2000);
                }
            }
        });

        loadAd();
        setupViewPager();
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        if (Singleton.getInstance().adsIds != null && !Singleton.getInstance().adsIds.isEmpty()
                && Singleton.getInstance().adsIds.size() >= 4) {
            AdView mAdView = new AdView(this);
            mAdView.setAdSize(AdSize.SMART_BANNER);
            mAdView.setAdUnitId(Singleton.getInstance().adsIds.get(3).id);
            adContainer.addView(mAdView);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "ad loaded");
                }
            });
            mAdView.loadAd(adRequest);
        }
    }

    private void search() {

        episodesSearchResults.clear();
        seriesSearchResults.clear();

        for (Episode episode : Singleton.getInstance().episodeArrayList) {
            boolean isExist = false;
            if (episode.name.contains(searchEditText.getText().toString()) || episode.seriesName.contains(searchEditText.getText().toString())) {
                for (Episode episode1 : episodesSearchResults) {
                    if (episode.id.equals(episode1.id)) {
                        isExist = true;
                    }
                }
                if (!isExist) {
                    episodesSearchResults.add(episode);
                }
            }
        }

        for (Series series : Singleton.getInstance().seriesArrayList) {
            boolean isExist = false;
            if (series.name.contains(searchEditText.getText().toString())) {
                for (Series series1 : seriesSearchResults) {
                    if (series.id.equals(series1.id) || series.name.equals(series1.name)) {
                        isExist = true;
                    }
                }
                if (!isExist) {
                    seriesSearchResults.add(series);
                }
            }
        }

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "series size inside search: " + Singleton.getInstance().seriesArrayList.size() + "");
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "episodes size inside search: " + Singleton.getInstance().episodeArrayList.size() + "");

        episodesSearchResultsFragment.updateView(episodesSearchResults);
        seriesAndEpisodesSearchResultsFragment.updateView(seriesSearchResults, episodesSearchResults);

        progressView.setVisibility(View.GONE);
        progressView.stopAnimation();

    }

    private void setupViewPager() {
        if (viewPager != null) {
            viewPager.setOffscreenPageLimit(2);

            ArrayList<TabObject> tabObjects = new ArrayList<>();

            episodesSearchResultsFragment = new EpisodesSearchResultsFragment();

            seriesAndEpisodesSearchResultsFragment = new SeriesAndEpisodesSearchResultsFragment();

            tabObjects.add(new TabObject(episodesSearchResultsFragment, getResources().getString(R.string.episodesOnly)));
            tabObjects.add(new TabObject(seriesAndEpisodesSearchResultsFragment, getResources().getString(R.string.searchAll)));

            SettingsViewPagerAdapter adapter = new SettingsViewPagerAdapter(this, getSupportFragmentManager(), tabObjects);

            viewPager.setAdapter(adapter);

            viewPager.setCurrentItem(1);

            searchTypesTabs.setupWithViewPager(viewPager);
            changeTabsFont();

        }
    }

    private void changeTabsFont() {
        Typeface typeface = App.objectGraph.get(Typeface.class);
        ViewGroup vg = (ViewGroup) searchTypesTabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeface);
                }
            }
        }
    }

    void openSeriesSingleValueListener() {
        final DatabaseReference seriesDatabaseReference = Singleton.sDatabase.
                child("series");

        seriesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Singleton.getInstance().seriesArrayList.add(Series.parse(child));
                }

                openSeriesChildValueEventListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void openEpisodeSingleValueListener() {
        final DatabaseReference episodeDatabaseReference = Singleton.sDatabase.
                child("episode");

        episodeDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Singleton.getInstance().episodeArrayList.add(Episode.parse(child));
                }

                openEpisodeChildValueListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    int seriesSize = 0;

    void openSeriesChildValueEventListener() {
        final DatabaseReference seriesDatabaseReference = Singleton.sDatabase.
                child("series");

        seriesDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                seriesSize++;
                Series series = Series.parse(dataSnapshot);

                if (Singleton.getInstance().seriesArrayList.size() < seriesSize) {
                    Singleton.getInstance().seriesArrayList.add(series);
//                    episodesSearchResultsFragment.updateView(Singleton.getInstance().episodeArrayList);
//                    seriesAndEpisodesSearchResultsFragment.updateView(Singleton.getInstance().seriesArrayList,
//                            Singleton.getInstance().episodeArrayList);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Series series = Series.parse(dataSnapshot);
                for (int i = 0; i < Singleton.getInstance().seriesArrayList.size(); i++) {
                    if (Singleton.getInstance().seriesArrayList.get(i).id.equals(series.id)) {
                        Singleton.getInstance().seriesArrayList.remove(i);
                        Singleton.getInstance().seriesArrayList.add(i, series);
                        break;
                    }
                }

//                episodesSearchResultsFragment.updateView(Singleton.getInstance().episodeArrayList);
//                seriesAndEpisodesSearchResultsFragment.updateView(Singleton.getInstance().seriesArrayList,
//                        Singleton.getInstance().episodeArrayList);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                seriesSize--;
                Series series = Series.parse(dataSnapshot);
                for (int i = 0; i < Singleton.getInstance().seriesArrayList.size(); i++) {
                    if (Singleton.getInstance().seriesArrayList.get(i).id.equals(series.id)) {
                        Singleton.getInstance().seriesArrayList.remove(i);
                        break;
                    }
                }

//                episodesSearchResultsFragment.updateView(Singleton.getInstance().episodeArrayList);
//                seriesAndEpisodesSearchResultsFragment.updateView(Singleton.getInstance().seriesArrayList,
//                        Singleton.getInstance().episodeArrayList);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    int episodesSize = 0;

    void openEpisodeChildValueListener() {
        final DatabaseReference episodeDatabaseReference = Singleton.sDatabase.
                child("episode");

        episodeDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                episodesSize++;
                Episode episode = Episode.parse(dataSnapshot);

                if (Singleton.getInstance().episodeArrayList.size() < episodesSize) {
                    Singleton.getInstance().episodeArrayList.add(episode);
                    if (episodesSearchResultsFragment.isAdded()) {
                        episodesSearchResultsFragment.updateView(Singleton.getInstance().episodeArrayList);
                        seriesAndEpisodesSearchResultsFragment.updateView(Singleton.getInstance().seriesArrayList,
                                Singleton.getInstance().episodeArrayList);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Episode episode = Episode.parse(dataSnapshot);

                for (int i = 0; i < Singleton.getInstance().episodeArrayList.size(); i++) {
                    if (Singleton.getInstance().episodeArrayList.get(i).id.equals(episode.id)) {
                        Singleton.getInstance().episodeArrayList.remove(i);
                        Singleton.getInstance().episodeArrayList.add(i, episode);
                        break;
                    }
                }

                if (episodesSearchResultsFragment.isAdded()) {
                    episodesSearchResultsFragment.updateView(Singleton.getInstance().episodeArrayList);
                    seriesAndEpisodesSearchResultsFragment.updateView(Singleton.getInstance().seriesArrayList,
                            Singleton.getInstance().episodeArrayList);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Episode episode = Episode.parse(dataSnapshot);
                for (int i = 0; i < Singleton.getInstance().episodeArrayList.size(); i++) {
                    if (Singleton.getInstance().episodeArrayList.get(i).id.equals(episode.id)) {
                        Singleton.getInstance().episodeArrayList.remove(i);
                        break;
                    }
                }

                if (episodesSearchResultsFragment.isAdded()) {
                    episodesSearchResultsFragment.updateView(Singleton.getInstance().episodeArrayList);
                    seriesAndEpisodesSearchResultsFragment.updateView(Singleton.getInstance().seriesArrayList,
                            Singleton.getInstance().episodeArrayList);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    void showNoConnectionContainer() {
        noConnectionBar.setVisibility(View.VISIBLE);
    }

    @Override
    void hideNoConnectionContainer() {
        noConnectionBar.setVisibility(View.GONE);
    }

    @Override
    public void updateProfilePicture() {

    }
}
