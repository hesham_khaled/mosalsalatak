package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.exceptions.NoInternetConnectionException;
import com.brighteyes.mosalsalatak_aflam.interfaces.ObserverListener;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.CustomTypefaceSpan;
import com.brighteyes.mosalsalatak_aflam.utils.FirebaseDataHelper;
import com.brighteyes.mosalsalatak_aflam.utils.FirebaseListenersUpdateReceiver;
import com.brighteyes.mosalsalatak_aflam.utils.NetworkConnection;
import com.brighteyes.mosalsalatak_aflam.utils.NetworkStateReceiver;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;


/**
 * Created by heshamkhaled on 9/20/16.
 */
abstract public class AbstractActivity extends AppCompatActivity implements ObserverListener {


    protected LinearLayout mNoConnectionBar;
    ProgressDialog dialog;
    boolean isTablet;
    private NetworkStateReceiver stateReceiver;
    public FirebaseListenersUpdateReceiver firebaseReceiver;
    public FirebaseDataHelper firebaseDataHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stateReceiver = new NetworkStateReceiver();
        firebaseReceiver = new FirebaseListenersUpdateReceiver();
        firebaseDataHelper = new FirebaseDataHelper(this);

        Singleton.currentActivity = this;

        isTablet = getResources().getBoolean(R.bool.isTab);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //data to be saved
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //restore data
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        stateReceiver.register(this);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "registered number: " + Singleton.getInstance().observers.size());
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "registered class name: " + this.getClass().getName());
        if (NetworkConnection.networkConnectivity(this)) {
            hideNoConnectionContainer();
        } else {
            showNoConnectionContainer();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stateReceiver.unregister(this);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "registered number after unregister: " + Singleton.getInstance().observers.size());
    }

//    public void logout() {
//        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putBoolean(CommonConstants.IS_LOGGED_IN, false);
//        editor.remove(CommonConstants.EMAIL_KEY);
//        editor.remove(CommonConstants.BIRTHDAY_KEY);
//        editor.remove(CommonConstants.PASSWORD_KEY);
//        editor.remove(CommonConstants.FULL_NAME_KEY);
//        editor.remove(CommonConstants.USER_ID_KEY);
//        editor.remove(CommonConstants.PROFILE_PICTURE_KEY);
//        editor.remove(CommonConstants.TOKEN_KEY);
//        editor.remove(CommonConstants.REFRESH_TOKEN_KEY);
//        editor.remove(CommonConstants.FCM_TOKEN_KEY);
//        editor.commit();
//
//        Singleton.getInstance().user = null;
//        if (!isTablet) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        } else {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        }
//        finish();
//        if (Singleton.currentActivity instanceof LoginActivity) {
//            Log.d(CommonConstants.APPLICATION_LOG_TAG, "do nothing");
//        } else {
//            Intent intent = new Intent(this, LoginActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        }
//    }
//
//    public void logout(boolean isFromConverterActivity) {
//        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putBoolean(CommonConstants.IS_LOGGED_IN, false);
//        editor.remove(CommonConstants.EMAIL_KEY);
//        editor.remove(CommonConstants.BIRTHDAY_KEY);
//        editor.remove(CommonConstants.PASSWORD_KEY);
//        editor.remove(CommonConstants.FULL_NAME_KEY);
//        editor.remove(CommonConstants.USER_ID_KEY);
//        editor.remove(CommonConstants.PROFILE_PICTURE_KEY);
//        editor.remove(CommonConstants.TOKEN_KEY);
//        editor.remove(CommonConstants.REFRESH_TOKEN_KEY);
//        editor.remove(CommonConstants.FCM_TOKEN_KEY);
//        editor.commit();
//
//        Singleton.getInstance().user = null;
//
//        finish();
//        if (Singleton.currentActivity instanceof LoginActivity) {
//            Log.d(CommonConstants.APPLICATION_LOG_TAG, "do nothing");
//        } else {
//            Intent intent = new Intent(this, LoginActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            Bundle bundle = new Bundle();
//            bundle.putBoolean("isFromConverterActivity", isFromConverterActivity);
//            intent.putExtras(bundle);
//            startActivity(intent);
//        }
//    }

    @Override
    public void update(boolean internetConnectionExist) {

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "inside update: " + internetConnectionExist);

        if (internetConnectionExist) {
            hideNoConnectionContainer();
        } else {
            showNoConnectionContainer();
        }
    }

    abstract void showNoConnectionContainer();

    abstract void hideNoConnectionContainer();

//    private void showNoConnectionContainer() {
//        Log.d(CommonConstants.APPLICATION_LOG_TAG, "inside showNoConnectionContainer");
//        if (mNoConnectionBar != null) {
//            Log.d(CommonConstants.APPLICATION_LOG_TAG, "inside showNoConnectionContainer bar is not null");
//            mNoConnectionBar.setVisibility(View.VISIBLE);
//            mNoConnectionBar.bringToFront();
//        }
//    }
//
//    private void hideNoConnectionContainer() {
//        Log.d(CommonConstants.APPLICATION_LOG_TAG, "inside hideNoConnectionContainer");
//        if (mNoConnectionBar != null) {
//            Log.d(CommonConstants.APPLICATION_LOG_TAG, "inside hideNoConnectionContainer bar is not null");
//            mNoConnectionBar.setVisibility(View.GONE);
//        }
//    }

    public void share(String textToBeShared) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, textToBeShared);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void createLoadingDialog() {
        dialog = ProgressDialog.show(this, "",
                "Downloading. Please wait...", true);
    }

    public void dismissLoadingDialog() {
        dialog.dismiss();
    }

    private void applyFontToMenuItem(MenuItem mi, Typeface font) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void applyFontOnNavigationViewItems(NavigationView navigationView, Typeface font) {
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            applyFontToMenuItem(menuItem, font);
        }
    }

    public boolean validEmail(String email) {
        if (email == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public void onRequestError(Throwable error) {
        if (error instanceof java.net.UnknownHostException) {
            Toast.makeText(this, getResources().getString(R.string.unknownHostErrorMessage),
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof java.net.SocketTimeoutException) {
            Toast.makeText(this, getResources().getString(R.string.timeoutErrorMessage),
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof NoInternetConnectionException) {
            Toast.makeText(this, getResources().getString(R.string.noConnection),
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, error.getLocalizedMessage(),
                    Toast.LENGTH_LONG).show();
            Log.d(CommonConstants.APPLICATION_LOG_TAG, error.getLocalizedMessage());
        }
    }

//    private void openAdIdsSingleValueListener() {
//
//        Singleton.getInstance().adsIds.clear();
//
//        size = 0;
//
//        final DatabaseReference adsDatabaseReference = Singleton.sDatabase.child("ads");
//        adsDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot child : dataSnapshot.getChildren())
//                    Singleton.getInstance().adsIds.add(AdsId.parse(child));
//
//                openAdIdsChildValueListener();
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }
//
//    int size = 0;
//
//    private void openAdIdsChildValueListener() {
//        final DatabaseReference adsDatabaseReference = Singleton.sDatabase.child("ads");
//        adsDatabaseReference.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                size++;
//
//                AdsId adsId = AdsId.parse(dataSnapshot);
//
//                if (Singleton.getInstance().adsIds.size() < size) {
//                    Singleton.getInstance().adsIds.add(adsId);
//                }
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                openAdIdsSingleValueListener();
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//                openAdIdsSingleValueListener();
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }
}
