package com.brighteyes.mosalsalatak_aflam.views.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.CommentObject;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by user on 1/23/17.
 */

public class CommentsRecyclerViewAdapter extends RecyclerView.Adapter<CommentsRecyclerViewAdapter.ViewHolder> {


    LayoutInflater inflater;
    Typeface typeface;
    ViewHolder viewHolder;
    ArrayList<CommentObject> commentObjects;
    private Activity mContext;

    public CommentsRecyclerViewAdapter(Activity context, ArrayList<CommentObject> commentObjects) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        typeface = App.objectGraph.get(Typeface.class);
        this.commentObjects = commentObjects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.comment_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final ViewHolder ordinaryViewHolder = holder;
        ordinaryViewHolder.commentTextView.setText(commentObjects.get(position).comment.body);
        if (Singleton.getInstance().currentUser != null && Singleton.getInstance().currentUser.isAdmin) {
            ordinaryViewHolder.commentTextView.setTextColor(mContext.getResources().getColor(R.color.adminComment));
        }

        if (commentObjects.get(position).userProfilePicture.isEmpty()) {
            Picasso.with(mContext)
                    .load(R.drawable.logo)
                    .into(ordinaryViewHolder.userProfileImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            ordinaryViewHolder.userProfileImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            Picasso.with(mContext)
                    .load(commentObjects.get(position).userProfilePicture)
                    .into(ordinaryViewHolder.userProfileImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            ordinaryViewHolder.userProfileImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        }

                        @Override
                        public void onError() {
                            ordinaryViewHolder.userProfileImageView.setImageResource(R.drawable.logo);
                        }
                    });
        }
    }

    @Override
    public int getItemCount() {
        return commentObjects.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.commentTextView)
        TextView commentTextView;

        @Bind(R.id.userProfileImageView)
        RoundedImageView userProfileImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void emptyViewHolder() {
            commentTextView.setText("");
            userProfileImageView.setImageBitmap(null);
        }
    }
}
