package com.brighteyes.mosalsalatak_aflam.views.fragments;


import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Category;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.adapters.CategorySeriesRecyclerViewAdapter;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCategoryFragment extends Fragment {

    @Bind(R.id.categorySeriesRecyclerView)
    RecyclerView categorySeriesRecyclerView;

    @Bind(R.id.loadMoreCardView)
    CardView loadMoreCardView;

    @Bind(R.id.loadMoreTextView)
    TextView loadMoreTextView;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    private Activity mActivity;

    int page = 1;
    private ArrayList<Series> paginatedSeriesArrayList = new ArrayList<>();
    ArrayList<Series> seriesArrayList = new ArrayList<>();
    Category category;

    CategorySeriesRecyclerViewAdapter adapter;

    public MyCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        category = (Category) getArguments().getSerializable("category");

        View view = inflater.inflate(R.layout.fragment_my_category, container, false);
        ButterKnife.bind(this, view);

        openSingleValueListener();

        loadMoreCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page++;

                updateView();

                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();
            }
        });

        loadMoreCardView.setVisibility(View.GONE);
        Typeface typeface = App.objectGraph.get(Typeface.class);
        loadMoreTextView.setTypeface(typeface);

        return view;
    }

    private void openSingleValueListener() {
        Singleton.sDatabase.child("series").orderByChild("category_id").equalTo(category.id).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Series series = Series.parse(child);
                            seriesArrayList.add(series);
                        }

                        Collections.reverse(seriesArrayList);

                        updateView();
                        openChildValueListener();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    int size = 0;

    private void openChildValueListener() {
        Singleton.sDatabase.child("series").orderByChild("category_id").equalTo(category.id)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        size++;
                        if (size > seriesArrayList.size()) {
                            seriesArrayList.add(Series.parse(dataSnapshot));
                            Singleton.getInstance().seriesArrayList.add(Series.parse(dataSnapshot));

                            updateView();
                        }

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        ArrayList<Series> temp = seriesArrayList;

                        for (Series series : temp) {
                            if (series.id.equals(dataSnapshot.getKey())) {
                                series.name = (String) dataSnapshot.child("name").getValue();
                                series.photoUrl = (String) dataSnapshot.child("photo_url").getValue();
                                series.numberOfVideos = (long) dataSnapshot.child("number_of_videos").getValue();
                                series.numberOfViews = (long) dataSnapshot.child("number_of_viewes").getValue();

                                Singleton.getInstance().seriesArrayList.get(temp.indexOf(series)).name = (String) dataSnapshot.child("name").getValue();
                                Singleton.getInstance().seriesArrayList.get(temp.indexOf(series)).photoUrl = (String) dataSnapshot.child("photo_url").getValue();
                                Singleton.getInstance().seriesArrayList.get(temp.indexOf(series)).numberOfVideos = (long) dataSnapshot.child("number_of_videos").getValue();
                                Singleton.getInstance().seriesArrayList.get(temp.indexOf(series)).numberOfViews = (long) dataSnapshot.child("number_of_viewes").getValue();

                                updateView();
                                break;
                            }
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        size--;
                        ArrayList<Series> temp = seriesArrayList;
                        for (int i = 0; i < temp.size(); i++) {
                            if (seriesArrayList.get(i).id.equals(dataSnapshot.getKey())) {
                                seriesArrayList.remove(i);
                                Singleton.getInstance().seriesArrayList.remove(i);
                                updateView();
                                break;
                            }
                        }
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    void updateView() {
        paginatedSeriesArrayList.clear();

        if (seriesArrayList.size() <= (5 * page)) {
            for (int i = 0; i < seriesArrayList.size(); i++) {
                paginatedSeriesArrayList.add(seriesArrayList.get(i));
            }
            loadMoreCardView.setVisibility(View.GONE);
        } else {
            for (int i = 0; i < 5 * page; i++) {
                paginatedSeriesArrayList.add(seriesArrayList.get(i));
            }
            loadMoreCardView.setVisibility(View.VISIBLE);
        }

//        Collections.reverse(paginatedSeriesArrayList);

        categorySeriesRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        adapter = new CategorySeriesRecyclerViewAdapter(mActivity,
                paginatedSeriesArrayList, category.name);
        categorySeriesRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
