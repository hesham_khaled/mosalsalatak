package com.brighteyes.mosalsalatak_aflam.views.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.CommentUser;
import com.brighteyes.mosalsalatak_aflam.entities.User;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.activities.SettingsActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {

    private static final int SELECT_PICTURE = 1;

    @Bind(R.id.profilePictureImageView)
    ImageView profilePictureImageView;

    @Bind(R.id.cameraIcon)
    ImageView cameraIcon;

    @Bind(R.id.usernameTextView)
    TextView usernameTextView;

    @Bind(R.id.usernameEditText)
    EditText usernameEditText;

    @Bind(R.id.usernameErrorTextView)
    TextView usernameErrorTextView;

    @Bind(R.id.passwordTextView)
    TextView passwordTextView;

    @Bind(R.id.passwordEditText)
    EditText passwordEditText;

    @Bind(R.id.passwordErrorTextView)
    TextView passwordErrorTextView;

    @Bind(R.id.update1Button)
    Button update1Button;

    @Bind(R.id.nickNameTextView)
    TextView nickNameTextView;

    @Bind(R.id.nickNameEditText)
    EditText nickNameEditText;

    @Bind(R.id.ageTextView)
    TextView ageTextView;

    @Bind(R.id.ageEditText)
    EditText ageEditText;

    @Bind(R.id.nationalityTextView)
    TextView nationalityTextView;

    @Bind(R.id.nationalityEditText)
    EditText nationalityEditText;

    @Bind(R.id.genderTextView)
    TextView genderTextView;

    @Bind(R.id.genderEditText)
    EditText genderEditText;

    @Bind(R.id.update2Button)
    Button update2Button;

    File profilePicture;
    String name;
    private Activity mActivity;

    public ProfileFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Typeface typeface = App.objectGraph.get(Typeface.class);

        usernameTextView.setTypeface(typeface);
        usernameEditText.setTypeface(typeface);
        usernameErrorTextView.setTypeface(typeface);

        passwordTextView.setTypeface(typeface);
        passwordEditText.setTypeface(typeface);
        passwordErrorTextView.setTypeface(typeface);

        update1Button.setTypeface(typeface);

        nickNameEditText.setTypeface(typeface);
        nickNameTextView.setTypeface(typeface);

        ageEditText.setTypeface(typeface);
        ageTextView.setTypeface(typeface);

        nationalityEditText.setTypeface(typeface);
        nationalityTextView.setTypeface(typeface);

        genderEditText.setTypeface(typeface);
        genderTextView.setTypeface(typeface);

        update2Button.setTypeface(typeface);

        if (Singleton.getInstance().currentUser.photoUrl != null && !Singleton.getInstance().currentUser.photoUrl.isEmpty())
            Picasso.with(mActivity)
                    .load(Singleton.getInstance().currentUser.photoUrl)
                    .into(profilePictureImageView);

        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
        name = sharedPreferences.getString(CommonConstants.USER_NAME, null);

        if (Singleton.getInstance().currentUser.email != null && !Singleton.getInstance().currentUser.email.isEmpty()) {
            usernameEditText.setText(Singleton.getInstance().currentUser.email);
        }

        if (Singleton.getInstance().currentUser.password != null && !Singleton.getInstance().currentUser.password.isEmpty()) {
            passwordEditText.setText(Singleton.getInstance().currentUser.password);
        }

        if (name != null && !name.isEmpty()) {
            nickNameEditText.setText(name);
        }

        if (Singleton.getInstance().currentUser.birthDate != null &&
                !Singleton.getInstance().currentUser.birthDate.isEmpty()) {
            ageEditText.setText(String.valueOf(Singleton.getInstance().currentUser.birthDate));
        }

        if (Singleton.getInstance().currentUser.nationality != null && !Singleton.getInstance().currentUser.nationality.isEmpty()) {
            nationalityEditText.setText(Singleton.getInstance().currentUser.nationality);
        }

        if (Singleton.getInstance().currentUser.gender != null && !Singleton.getInstance().currentUser.gender.isEmpty()) {
            genderEditText.setText(Singleton.getInstance().currentUser.gender);
        }

        profilePictureImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mActivity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        return;
                    }
                }

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "اختر صورة"), SELECT_PICTURE);
            }
        });

        update1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (usernameEditText.getText().toString().isEmpty() || passwordEditText.getText().toString().isEmpty()) {
                    Toast.makeText(mActivity, getResources().getString(R.string.emptyFieldsError), Toast.LENGTH_LONG).show();
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (mActivity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            Log.v(CommonConstants.APPLICATION_LOG_TAG, "Permission is granted");
                        } else {
                            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        }
                    }

                    ((SettingsActivity) mActivity).showLoader();
                    Singleton.getInstance().currentUser.email = usernameEditText.getText().toString();
                    Singleton.getInstance().currentUser.password = passwordEditText.getText().toString();

                    SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString(CommonConstants.USER_PASSWORD, Singleton.getInstance().currentUser.password);
                    editor.putString(CommonConstants.USER_EMAIL, Singleton.getInstance().currentUser.email);

                    editor.commit();

                    Singleton.getInstance().currentUser.name = name;

                    Singleton.sDatabase.child("user").child(Singleton.getInstance().currentUser.id)
                            .updateChildren(User.toMap(Singleton.getInstance().currentUser), new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if (profilePicture != null) {
                                        try {

//                                            StorageReference storageRef = Singleton.sStorage.getReferenceFromUrl("gs://mosalsalatak-7d015.appspot.com/");
                                            StorageReference storageRef = Singleton.sStorage.getReferenceFromUrl("gs://mosalsalatak-c6ddc.appspot.com");

                                            StorageReference profilePicRef = storageRef.child(Singleton.getInstance().currentUser.email + ".jpg");

                                            InputStream stream = new FileInputStream(profilePicture);
                                            UploadTask uploadTask = profilePicRef.putStream(stream);

                                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "Upload error");
                                                    e.printStackTrace();
                                                    ((SettingsActivity) mActivity).hideLoader();
                                                }
                                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                @Override
                                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                                    Singleton.getInstance().currentUser.photoUrl = downloadUrl.toString();

                                                    SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
                                                    SharedPreferences.Editor editor = sharedPreferences.edit();

                                                    editor.putString(CommonConstants.USER_PHOTO_URL, Singleton.getInstance().currentUser.photoUrl);

                                                    editor.commit();

                                                    Singleton.sDatabase.child("user").child(Singleton.getInstance().currentUser.id)
                                                            .updateChildren(User.toMap(Singleton.getInstance().currentUser));

                                                    //update comment user photo url
                                                    Singleton.sDatabase.child("comment_user").addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                                                CommentUser commentUser = CommentUser.parse(child);
                                                                if (commentUser.userId.equals(Singleton.getInstance().currentUser.id)) {
                                                                    HashMap<String, String> map = new HashMap<>();
                                                                    map.put("photo_url", Singleton.getInstance().currentUser.photoUrl);
                                                                    Singleton.sDatabase.child("comment_user").child(commentUser.commentId)
                                                                            .child(commentUser.userId).setValue(map, new DatabaseReference.CompletionListener() {
                                                                        @Override
                                                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                                                        }
                                                                    });
                                                                }
                                                            }

                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {

                                                        }
                                                    });

                                                    ((SettingsActivity) mActivity).hideLoader();

                                                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.updateSuccess), Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                            ((SettingsActivity) mActivity).hideLoader();
                                        }
                                    } else {
                                        ((SettingsActivity) mActivity).hideLoader();
                                    }
                                }
                            });
                }
            }
        });

        update2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isUpdated = false;

                if (!nickNameEditText.getText().toString().isEmpty()) {
                    isUpdated = true;
                    Singleton.getInstance().currentUser.name = nickNameEditText.getText().toString();

                    SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString(CommonConstants.USER_NAME, Singleton.getInstance().currentUser.name);

                    editor.commit();
                }

                if (!ageEditText.getText().toString().isEmpty()) {
                    isUpdated = true;
                    Singleton.getInstance().currentUser.birthDate = String.valueOf(ageEditText.getText().toString());

                    SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString(CommonConstants.USER_BIRTH_DATE, Singleton.getInstance().currentUser.birthDate);

                    editor.commit();
                }

                if (!nationalityEditText.getText().toString().isEmpty()) {
                    isUpdated = true;
                    Singleton.getInstance().currentUser.nationality = nationalityEditText.getText().toString();

                    SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString(CommonConstants.USER_NATIONALITY, Singleton.getInstance().currentUser.nationality);

                    editor.commit();
                }

                if (!genderEditText.getText().toString().isEmpty()) {

                    if (genderEditText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.male_en)) ||
                            genderEditText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.female_en)) ||
                            genderEditText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.female_ar)) ||
                            genderEditText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.male_ar))) {
                        isUpdated = true;
                        Singleton.getInstance().currentUser.gender = genderEditText.getText().toString();

                        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        editor.putString(CommonConstants.USER_GENDER, Singleton.getInstance().currentUser.gender);

                        editor.commit();
                    } else {
                        Toast.makeText(mActivity, "", Toast.LENGTH_LONG).show();
                    }
                }

                if (isUpdated) {
                    ((SettingsActivity) mActivity).showLoader();
                    Singleton.sDatabase.child("user").child(Singleton.getInstance().currentUser.id)
                            .updateChildren(User.toMap(Singleton.getInstance().currentUser), new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    ((SettingsActivity) mActivity).hideLoader();
                                }
                            });
                }


            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                profilePictureImageView.setImageURI(selectedImageUri);
                profilePicture = new File(getPath(mActivity, selectedImageUri));
            }
        }
    }

    public String getPath(final Context context, final Uri uri) {


        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }

        //7amada
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

//    public String getPath(Uri uri) {
//        // just some safety built in
//        if (uri == null) {
//            Log.d(CommonConstants.APPLICATION_LOG_TAG, "uri is null");
//            return null;
//        }
//        // try to retrieve the image from the media store first
//        // this will only work for images selected from gallery
//        String[] projection = {MediaStore.Images.Media.DATA};
//        Cursor cursor = mActivity.managedQuery(uri, projection, null, null, null);
//        if (cursor != null) {
//            int column_index = cursor
//                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        }
//        // this is our fallback here
//        return uri.getPath();
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //resume tasks needing this permission
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,
                    "اختر صورة"), SELECT_PICTURE);
        }
    }
}
