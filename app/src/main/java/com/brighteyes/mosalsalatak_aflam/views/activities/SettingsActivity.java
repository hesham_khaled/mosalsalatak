package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.TabObject;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.adapters.SettingsViewPagerAdapter;
import com.brighteyes.mosalsalatak_aflam.views.fragments.NotificationsSettingsFragment;
import com.brighteyes.mosalsalatak_aflam.views.fragments.ProfileFragment;
import com.brighteyes.mosalsalatak_aflam.views.fragments.RequestFragment;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SettingsActivity extends AbstractActivity {


    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.connectionErrorContainer)
    RelativeLayout noConnectionBar;

    @Bind(R.id.title)
    public TextView titleTextView;

//    @Bind(R.id.profileIconImageView)
//    ImageView profileIconImageView;
//
//    @Bind(R.id.favouritesIconImageView)
//    ImageView favouritesIconImageView;

    @Bind(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @Bind(R.id.removeNoConnectionBarImageView)
    ImageView removeNoConnectionBarImageView;

    @Bind(R.id.tabs)
    TabLayout tabLayout;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.viewpager)
    ViewPager viewPager;

    @Bind(R.id.backImageView)
    ImageView backImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        Typeface typeface = App.objectGraph.get(Typeface.class);

        titleTextView.setTypeface(typeface);
        titleTextView.setText(getResources().getString(R.string.profileTitle));

        setSupportActionBar(toolbar);

        backImageView.setVisibility(View.VISIBLE);
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        removeNoConnectionBarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNoConnectionContainer();
            }
        });

//        profileIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                viewPager.setCurrentItem(1);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });
//
//        favouritesIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(SettingsActivity.this, FavouritesActivity.class);
//                startActivity(intent);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });

        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, SearchActivity.class);
                startActivity(intent);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                            Singleton.getInstance().mInterstitialAd.show();
                        }
                    }
                }, 2000);
            }
        });

        setupViewPager();
    }

    private void setupViewPager() {
        if (viewPager != null) {
            viewPager.setOffscreenPageLimit(3);

            ArrayList<TabObject> tabObjects = new ArrayList<>();

            tabObjects.add(new TabObject(new RequestFragment(), getResources().getString(R.string.request)));
            tabObjects.add(new TabObject(new NotificationsSettingsFragment(), getResources().getString(R.string.notificationsSettings)));
            tabObjects.add(new TabObject(new ProfileFragment(), getResources().getString(R.string.profile)));

            SettingsViewPagerAdapter adapter = new SettingsViewPagerAdapter(this, getSupportFragmentManager(), tabObjects);

            viewPager.setAdapter(adapter);
//            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                @Override
//                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//                }
//
//                @Override
//                public void onPageSelected(int position) {
//                    switch (position) {
//                        case 1:
//                            setTitle(getResources().getString(R.string.profileTitle));
//                            break;
//
//                        case 0:
//                            setTitle(getResources().getString(R.string.notificationsSettingsTitle));
//                            break;
//
//                        default:
//                            break;
//                    }
//                }
//
//                @Override
//                public void onPageScrollStateChanged(int state) {
//
//                }
//            });
            viewPager.setCurrentItem(2);

            tabLayout.setupWithViewPager(viewPager);
            changeTabsFont();

        }
    }

    private void changeTabsFont() {
        Typeface typeface = App.objectGraph.get(Typeface.class);
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeface);
                }
            }
        }
    }

    @Override
    void showNoConnectionContainer() {
        noConnectionBar.setVisibility(View.VISIBLE);
    }

    @Override
    void hideNoConnectionContainer() {
        noConnectionBar.setVisibility(View.GONE);
    }

    @Override
    public void updateProfilePicture() {

    }

    public void setTitle(String title) {
        titleTextView.setText(title);
    }

    public void showLoader() {
        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();
    }

    public void hideLoader() {
        progressView.setVisibility(View.GONE);
        progressView.stopAnimation();
    }
}
