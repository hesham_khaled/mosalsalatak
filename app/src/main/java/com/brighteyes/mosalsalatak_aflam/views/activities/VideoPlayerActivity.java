package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VideoPlayerActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener,
        RewardedVideoAdListener {
    private final int AD_TIP_DISPLAY_LENGTH = 3000;
    private final int VIDEO_WAIT_TIP_DISPLAY_LENGTH = 60000;
    @Bind(R.id.videoPlayer)
    VideoView videoPlayer;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.adView)
    RelativeLayout adContainer;

    @Bind(R.id.ad_tip)
    TextView adTip;

    @Bind(R.id.video_tip)
    TextView videoTip;

    Episode episode;
    private RewardedVideoAd mAd;
    private boolean isVideoAdPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        ButterKnife.bind(this);
        progressView.setVisibility(View.VISIBLE);
        progressView.startAnimation();

        mAd = MobileAds.getRewardedVideoAdInstance(this);
        mAd.setRewardedVideoAdListener(this);

        loadRewardedVideoAd();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        episode = (Episode) getIntent().getSerializableExtra("episode");

        videoPlayer.setOnPreparedListener(this);

        loadAd();
        adContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adContainer.setVisibility(View.GONE);
            }
        });
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        if (Singleton.getInstance().adsIds != null && !Singleton.getInstance().adsIds.isEmpty()
                && Singleton.getInstance().adsIds.size() >= 7) {
            AdView mAdView = new AdView(this);
            mAdView.setAdSize(AdSize.SMART_BANNER);
//        mAdView.setAdUnitId("ca-app-pub-5084160762510449/3352927015");
            mAdView.setAdUnitId(Singleton.getInstance().adsIds.get(6).id);
            adContainer.addView(mAdView);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "ad loaded");
                }
            });
            mAdView.loadAd(adRequest);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        videoPlayer.pause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!videoPlayer.isPlaying() && videoPlayer.getCurrentPosition() > 1) {
            videoPlayer.resume();
        }
    }

    private void setupVideoView() {
        if (videoPlayer != null) {
            MediaController mediaController = new
                    MediaController(this);
            mediaController.setAnchorView(videoPlayer);
            videoPlayer.setMediaController(mediaController);
            videoPlayer.setVideoURI(Uri.parse(episode.videoUrl));
        }
    }

    @Override
    public void onBackPressed() {
        if (!isVideoAdPlaying) {
            videoPlayer.stopPlayback();
            super.onBackPressed();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {

                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    progressView.setVisibility(View.GONE);
                    progressView.stopAnimation();
                    videoTip.setVisibility(View.GONE);
                }

                if (what == MediaPlayer.MEDIA_INFO_BUFFERING_START) {
                    progressView.setVisibility(View.VISIBLE);
                    progressView.startAnimation();
                }
                if (what == MediaPlayer.MEDIA_INFO_BUFFERING_END) {
                    progressView.setVisibility(View.GONE);
                    progressView.stopAnimation();
                    videoTip.setVisibility(View.GONE);
                }
                return true;
            }
        });

        videoPlayer.start();
    }

    private void loadRewardedVideoAd() {
        mAd.loadAd(getResources().getString(R.string.rewarded_video_ad_id), new AdRequest.Builder().build());
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.d("Hesham", "loaded");
        if (mAd.isLoaded()) {
            isVideoAdPlaying = true;
            mAd.show();
        }
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.d("Hesham", "opened");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.d("Hesham", "started");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        isVideoAdPlaying = false;
        setupVideoView();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                adTip.setVisibility(View.GONE);
            }
        }, AD_TIP_DISPLAY_LENGTH);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                videoTip.setVisibility(View.GONE);
            }
        }, VIDEO_WAIT_TIP_DISPLAY_LENGTH);
        Log.d("Hesham", "closed");
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Log.d("Hesham", "rewarded");
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.d("Hesham", "left app");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        setupVideoView();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                adTip.setVisibility(View.GONE);
            }
        }, AD_TIP_DISPLAY_LENGTH);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                videoTip.setVisibility(View.GONE);
            }
        }, VIDEO_WAIT_TIP_DISPLAY_LENGTH);
        Log.d("Hesham", "failed");
    }

}
