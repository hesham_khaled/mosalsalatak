package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.UserEpisodes;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.adapters.FavouritesRecyclerViewAdapter;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FavouritesActivity extends AbstractActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.connectionErrorContainer)
    RelativeLayout noConnectionBar;

    @Bind(R.id.title)
    public TextView titleTextView;

//    @Bind(R.id.profileIconImageView)
//    ImageView profileIconImageView;

    @Bind(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @Bind(R.id.removeNoConnectionBarImageView)
    ImageView removeNoConnectionBarImageView;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.favouritesRecyclerView)
    RecyclerView favouritesRecyclerView;

    @Bind(R.id.tabs)
    TabLayout tabLayout;

    @Bind(R.id.favouritesTitleTextView)
    TextView favouritesTitleTextView;

    @Bind(R.id.backImageView)
    ImageView backImageView;

    @Bind(R.id.adView)
    RelativeLayout adContainer;


    UserEpisodes userEpisodes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);

        ButterKnife.bind(this);
        tabLayout.setVisibility(View.GONE);

        backImageView.setVisibility(View.VISIBLE);
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Typeface typeface = App.objectGraph.get(Typeface.class);
        titleTextView.setText(getResources().getString(R.string.favouritesTitle));
        titleTextView.setTypeface(typeface);
        favouritesTitleTextView.setTypeface(typeface);

        setSupportActionBar(toolbar);

        removeNoConnectionBarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNoConnectionContainer();
            }
        });

//        profileIconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(FavouritesActivity.this, SettingsActivity.class);
//                startActivity(intent);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
//                            Singleton.getInstance().mInterstitialAd.show();
//                        }
//                    }
//                }, 2000);
//            }
//        });

        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FavouritesActivity.this, SearchActivity.class);
                startActivity(intent);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                            Singleton.getInstance().mInterstitialAd.show();
                        }
                    }
                }, 2000);
            }
        });

        loadAd();

        openFavouritesSingleValueListener();

    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();

        AdView mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(Singleton.getInstance().adsIds.get(4).id);
        adContainer.addView(mAdView);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "ad loaded");
            }
        });
        mAdView.loadAd(adRequest);
    }

    private void openFavouritesSingleValueListener() {
        final DatabaseReference favouritesDatabaseReference = Singleton.sDatabase.child("user_episodes")
                .child(Singleton.getInstance().currentUser.id);

        favouritesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userEpisodes = new UserEpisodes();
                userEpisodes.userId = Singleton.getInstance().currentUser.id;

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    userEpisodes.episodes.add(Episode.parse(child));

                }

                updateRecyclerView(true);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateRecyclerView(boolean isFirst) {

        favouritesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        FavouritesRecyclerViewAdapter adapter = new FavouritesRecyclerViewAdapter(this, userEpisodes.episodes);
        favouritesRecyclerView.setAdapter(adapter);

        if (isFirst) {
            openFavouritesChildValueListener();
        }
    }

    int size = 0;

    private void openFavouritesChildValueListener() {
        final DatabaseReference favouritesDatabaseReference = Singleton.sDatabase.child("user_episodes")
                .child(Singleton.getInstance().currentUser.id);

        favouritesDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                size++;
                Episode episode = Episode.parse(dataSnapshot);

                if (userEpisodes.episodes.size() < size) {
                    userEpisodes.episodes.add(episode);
                }

                updateRecyclerView(false);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Episode episode = Episode.parse(dataSnapshot);
                for (int i = 0; i < userEpisodes.episodes.size(); i++) {
                    if (userEpisodes.episodes.get(i).id.equals(episode.id)) {
                        userEpisodes.episodes.remove(i);
                        userEpisodes.episodes.add(i, episode);
                        break;
                    }
                }

                updateRecyclerView(false);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Episode episode = Episode.parse(dataSnapshot);
                for (int i = 0; i < userEpisodes.episodes.size(); i++) {
                    if (userEpisodes.episodes.get(i).id.equals(episode.id)) {
                        userEpisodes.episodes.remove(i);
                        break;
                    }
                }

                updateRecyclerView(false);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    void showNoConnectionContainer() {
        noConnectionBar.setVisibility(View.VISIBLE);
    }

    @Override
    void hideNoConnectionContainer() {
        noConnectionBar.setVisibility(View.GONE);
    }

    @Override
    public void updateProfilePicture() {

    }
}
