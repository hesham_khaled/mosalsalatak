package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Request;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RequestsActivity extends AppCompatActivity {

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.nameEditText)
    EditText nameEditText;

    @Bind(R.id.requestEditText)
    EditText requestEditText;

    @Bind(R.id.requestTitleTextView)
    TextView requestTitleTextView;

    @Bind(R.id.sendButton)
    Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);

        ButterKnife.bind(this);

        Typeface typeface = App.objectGraph.get(Typeface.class);
        nameEditText.setTypeface(typeface);
        requestEditText.setTypeface(typeface);
        requestTitleTextView.setTypeface(typeface);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (requestEditText.getText().toString().isEmpty() || nameEditText.getText().toString().isEmpty()) {
                    Toast.makeText(RequestsActivity.this, getResources().getString(R.string.emptyFieldsError), Toast.LENGTH_LONG).show();
                } else {
                    progressView.setVisibility(View.VISIBLE);
                    progressView.startAnimation();

                    String requestId = Singleton.sDatabase.child("requests").push().getKey();

                    Request request = new Request();
                    request.name = nameEditText.getText().toString();
                    request.body = requestEditText.getText().toString();
                    request.isActive = true;

                    Singleton.sDatabase.child("requests").child(requestId)
                            .updateChildren(Request.parse(request), new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    progressView.setVisibility(View.GONE);
                                    progressView.stopAnimation();

                                    Toast.makeText(RequestsActivity.this, getResources().getString(R.string.requestSentSuccess),
                                            Toast.LENGTH_LONG).show();

                                    requestEditText.setText("");
                                    nameEditText.setText("");
                                }
                            });

                }
            }
        });


    }
}
