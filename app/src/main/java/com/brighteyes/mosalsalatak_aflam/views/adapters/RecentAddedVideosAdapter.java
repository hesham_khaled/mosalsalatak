package com.brighteyes.mosalsalatak_aflam.views.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.activities.EpisodeActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by user on 1/9/17.
 */

public class RecentAddedVideosAdapter extends RecyclerView.Adapter<RecentAddedVideosAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private Typeface typeface;
    private ViewHolder viewHolder;

    private ArrayList<Episode> mEpisodes, mLeftEpisodes = new ArrayList<>(), mRightEpisodes = new ArrayList<>();
    private Activity mContext;

    public RecentAddedVideosAdapter(Activity context, ArrayList<Episode> episodes) {
        mContext = context;
        mEpisodes = episodes;
        for (int i = 0; i < mEpisodes.size(); i++) {
            if (i % 2 == 0) {
                mLeftEpisodes.add(mEpisodes.get(i));
            } else {
                mRightEpisodes.add(mEpisodes.get(i));
            }
        }

        mContext = context;
        inflater = LayoutInflater.from(mContext);
        typeface = App.objectGraph.get(Typeface.class);
    }

    @Override
    public RecentAddedVideosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recent_added_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecentAddedVideosAdapter.ViewHolder holder, final int position) {

        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        if (mLeftEpisodes.size() > 0 && position < mLeftEpisodes.size()) {

            ordinaryHolder.leftGradientRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(mContext, EpisodeActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("episode", mLeftEpisodes.get(position));
                    bundle.putString("seriesName", mLeftEpisodes.get(position).seriesName);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                                Singleton.getInstance().mInterstitialAd.show();
                            }
                        }
                    }, 2000);
                }
            });

            ordinaryHolder.leftPlayImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(mContext, EpisodeActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("episode", mLeftEpisodes.get(position));
                    bundle.putString("seriesName", mLeftEpisodes.get(position).seriesName);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                                Singleton.getInstance().mInterstitialAd.show();
                            }
                        }
                    }, 2000);
                }
            });

            Picasso
                    .with(mContext)
                    .load(mLeftEpisodes.get(position).photoUrl)
                    .placeholder(R.drawable.logo_bg)
                    .into(ordinaryHolder.leftVideoImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            ordinaryHolder.leftVideoImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                            ordinaryHolder.leftGradientRelativeLayout.bringToFront();
                            ordinaryHolder.leftPlayImageView.bringToFront();
                            ordinaryHolder.leftViewsLinearLayout.bringToFront();
                        }

                        @Override
                        public void onError() {

                        }
                    });

            ordinaryHolder.leftVideoTitleTextView.setText(mLeftEpisodes.get(position).name);
            if (mLeftEpisodes.get(position).numberOfViews < 1000) {
                ordinaryHolder.leftViewsTextView.setText(String.valueOf(mLeftEpisodes.get(position).numberOfViews));
            } else {
                ordinaryHolder.leftViewsTextView.setText(String.valueOf((mLeftEpisodes.get(position)
                        .numberOfViews / 1000)) + "k");
            }


        } else {
            ordinaryHolder.leftVideoCardView.setVisibility(View.INVISIBLE);
        }

        if (mRightEpisodes.size() > 0 && position < mRightEpisodes.size()) {

            ordinaryHolder.rightGradientRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(mContext, EpisodeActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("episode", mRightEpisodes.get(position));
                    bundle.putString("seriesName", mRightEpisodes.get(position).seriesName);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                                Singleton.getInstance().mInterstitialAd.show();
                            }
                        }
                    }, 2000);
                }
            });

            ordinaryHolder.rightPlayImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(mContext, EpisodeActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("episode", mRightEpisodes.get(position));
                    bundle.putString("seriesName", mRightEpisodes.get(position).seriesName);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (Singleton.getInstance().mInterstitialAd != null && Singleton.getInstance().mInterstitialAd.isLoaded()) {
                                Singleton.getInstance().mInterstitialAd.show();
                            }
                        }
                    }, 2000);
                }
            });

            Picasso
                    .with(mContext)
                    .load(mRightEpisodes.get(position).photoUrl)
                    .placeholder(R.drawable.logo_bg)
                    .into(ordinaryHolder.rightVideoImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            ordinaryHolder.rightVideoImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                            ordinaryHolder.rightGradientRelativeLayout.bringToFront();
                            ordinaryHolder.rightPlayImageView.bringToFront();
                            ordinaryHolder.rightViewsLinearLayout.bringToFront();
                        }

                        @Override
                        public void onError() {

                        }
                    });

            ordinaryHolder.rightVideoTitleTextView.setText(mRightEpisodes.get(position).name);
            ordinaryHolder.rightViewsTextView.setText(String.valueOf(mRightEpisodes.get(position).numberOfViews));

        } else {
            ordinaryHolder.rightVideoCardView.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        if (mRightEpisodes.size() >= mLeftEpisodes.size()) {
            return mRightEpisodes.size();
        } else {
            return mLeftEpisodes.size();
        }
    }

//    @Override
//    public int getItemViewType(int position) {
////        if (position % 3 == 0 && position != 0)
////            return AD_TYPE;
//        return CONTENT_TYPE;
//    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.leftVideoCardView)
        CardView leftVideoCardView;

        @Bind(R.id.leftViewsLinearLayout)
        LinearLayout leftViewsLinearLayout;

        @Bind(R.id.leftViewsTextView)
        TextView leftViewsTextView;

        @Bind(R.id.leftPlayImageView)
        ImageView leftPlayImageView;

        @Bind(R.id.leftGradientRelativeLayout)
        RelativeLayout leftGradientRelativeLayout;

        @Bind(R.id.leftVideoImageView)
        ImageView leftVideoImageView;

        @Bind(R.id.leftVideoTitleTextView)
        TextView leftVideoTitleTextView;

        @Bind(R.id.rightVideoCardView)
        CardView rightVideoCardView;

        @Bind(R.id.rightViewsLinearLayout)
        LinearLayout rightViewsLinearLayout;

        @Bind(R.id.rightViewsTextView)
        TextView rightViewsTextView;

        @Bind(R.id.rightPlayImageView)
        ImageView rightPlayImageView;

        @Bind(R.id.rightGradientRelativeLayout)
        RelativeLayout rightGradientRelativeLayout;

        @Bind(R.id.rightVideoImageView)
        ImageView rightVideoImageView;

        @Bind(R.id.rightVideoTitleTextView)
        TextView rightVideoTitleTextView;

        public ViewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void emptyViewHolder() {
            leftViewsTextView.setTypeface(typeface);
            leftViewsTextView.setText("");

            leftVideoTitleTextView.setTypeface(typeface);
            leftVideoTitleTextView.setText("");

            rightViewsTextView.setTypeface(typeface);
            rightViewsTextView.setText("");

            rightVideoTitleTextView.setTypeface(typeface);
            rightVideoTitleTextView.setText("");

            Picasso
                    .with(mContext)
                    .load(R.drawable.logo_bg)
                    .into(leftVideoImageView);

            Picasso
                    .with(mContext)
                    .load(R.drawable.logo_bg)
                    .into(rightVideoImageView);

            leftVideoCardView.setVisibility(View.VISIBLE);

            rightVideoCardView.setVisibility(View.VISIBLE);
        }
    }
}
