package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Category;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.entities.Settings;
import com.brighteyes.mosalsalatak_aflam.entities.User;
import com.brighteyes.mosalsalatak_aflam.fcm.MyFirebaseInstanceIDService;
import com.brighteyes.mosalsalatak_aflam.interfaces.AuthenticationObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.CategoryObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.UserObserverListener;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.NetworkConnection;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AbstractActivity implements AuthenticationObserverListener, UserObserverListener,
        CategoryObserverListener {

    @Bind(R.id.loginTitleTextView)
    TextView loginTitleTextView;

    @Bind(R.id.usernameTextView)
    TextView usernameTextView;

    @Bind(R.id.usernameEditText)
    EditText usernameEditText;

    @Bind(R.id.usernameErrorTextView)
    TextView usernameErrorTextView;

    @Bind(R.id.passwordTextView)
    TextView passwordTextView;

    @Bind(R.id.passwordEditText)
    EditText passwordEditText;

    @Bind(R.id.passwordErrorTextView)
    TextView passwordErrorTextView;

    @Bind(R.id.loginButton)
    Button loginButton;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    @Bind(R.id.facebookLoginButton)
    LoginButton facebookLoginButton;

    CallbackManager mCallbackManager;
    FirebaseUser user;
    private boolean isFacebookLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mCallbackManager = CallbackManager.Factory.create();

        openCategoriesSingleValueListener();

//        firebaseReceiver.registerCategoryObserver(this);
        firebaseReceiver.registerAuthenticationObserver(this);
        firebaseReceiver.registerUserObserver(this);

        Typeface typeface = App.objectGraph.get(Typeface.class);
        loginTitleTextView.setTypeface(typeface);
        usernameTextView.setTypeface(typeface);
        usernameEditText.setTypeface(typeface);
        usernameErrorTextView.setTypeface(typeface);
        passwordTextView.setTypeface(typeface);
        passwordEditText.setTypeface(typeface);
        passwordErrorTextView.setTypeface(typeface);

        facebookLoginButton.setText(getResources().getString(R.string.loginWithFacebook));
        facebookLoginButton.setTypeface(typeface);

        facebookLoginButton.setReadPermissions("email", "public_profile");
        facebookLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "facebook:onError", error);
            }
        });


        loginButton.setTypeface(typeface);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (usernameEditText.getText().toString().isEmpty()) {
                    usernameErrorTextView.setVisibility(View.VISIBLE);
                    usernameErrorTextView.setText(getResources().getString(R.string.usernameNotExist));
                    return;
                } else if (!validEmail(usernameEditText.getText().toString())) {
                    usernameErrorTextView.setVisibility(View.VISIBLE);
                    usernameErrorTextView.setText(getResources().getString(R.string.emailError));
                    return;
                }

                if (passwordEditText.getText().toString().isEmpty()) {
                    passwordErrorTextView.setVisibility(View.VISIBLE);
                    passwordErrorTextView.setText(getResources().getString(R.string.passwordNotExist));
                    return;
                } else if (passwordEditText.getText().toString().length() < 6) {
                    passwordErrorTextView.setVisibility(View.VISIBLE);
                    passwordErrorTextView.setText(getResources().getString(R.string.passwordLengthError));
                    return;
                }

                if (NetworkConnection.networkConnectivity(LoginActivity.this)) {

                    progressView.setVisibility(View.VISIBLE);
                    progressView.startAnimation();

                    Singleton.sAuth.signInWithEmailAndPassword(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                firebaseDataHelper.openAuthenticationListener();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (e instanceof FirebaseAuthInvalidUserException) { // email is not exist
                                usernameErrorTextView.setVisibility(View.VISIBLE);
                                usernameErrorTextView.setText(getResources().getString(R.string.usernameLoginError));
                            }

                            if (e instanceof FirebaseAuthInvalidCredentialsException) { // password is wrong
                                passwordErrorTextView.setVisibility(View.VISIBLE);
                                passwordErrorTextView.setText(getResources().getString(R.string.passwordLoginError));
                            }

                            progressView.setVisibility(View.GONE);
                            progressView.stopAnimation();
                        }
                    });

                } else {
                    Toast.makeText(LoginActivity.this, R.string.noConnection,
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    ArrayList<Category> categories = new ArrayList<>();

    private void openCategoriesSingleValueListener() {
        Singleton.sDatabase.child("category").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    categories.add(Category.parse(child));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "handleFacebookAccessToken:" + token);

        if (NetworkConnection.networkConnectivity(LoginActivity.this)) {

            progressView.setVisibility(View.VISIBLE);
            progressView.startAnimation();

            AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
            Singleton.sAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(CommonConstants.APPLICATION_LOG_TAG, "signInWithCredential:success");
                                user = Singleton.sAuth.getCurrentUser();
                                isFacebookLogin = true;
                                firebaseDataHelper.openAuthenticationListener();
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(CommonConstants.APPLICATION_LOG_TAG, "signInWithCredential:failure", task.getException());
                                Toast.makeText(LoginActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                                progressView.setVisibility(View.GONE);
                                progressView.stopAnimation();
                            }

                            // ...
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Log.e(CommonConstants.APPLICATION_LOG_TAG, e.toString());

                    if (e instanceof FirebaseAuthInvalidUserException) { // email is not exist
                        usernameErrorTextView.setVisibility(View.VISIBLE);
                        usernameErrorTextView.setText(getResources().getString(R.string.usernameLoginError));
                    }

                    if (e instanceof FirebaseAuthInvalidCredentialsException) { // password is wrong
                        passwordErrorTextView.setVisibility(View.VISIBLE);
                        passwordErrorTextView.setText(getResources().getString(R.string.passwordLoginError));
                    }

                    progressView.setVisibility(View.GONE);
                    progressView.stopAnimation();
                }
            });
        } else {
            Toast.makeText(LoginActivity.this, R.string.noConnection,
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        firebaseReceiver.unregisterAuthenticationObserver(this);
        firebaseReceiver.unregisterUserObserver(this);
    }

    @Override
    void showNoConnectionContainer() {

    }

    @Override
    void hideNoConnectionContainer() {

    }

    @Override
    public void authenticationStatusChanged(boolean isSignedIn) {
        if (isSignedIn) {
            if (isFacebookLogin) {
                Singleton.sDatabase.child("user").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        boolean isUserExist = false;
                        User selectedUser = new User();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            selectedUser = User.parse(child);
                            if (selectedUser != null && selectedUser.email != null &&
                                    selectedUser.email.equals(LoginActivity.this.user.getEmail())) {
                                isUserExist = true;
                                break;
                            }
                        }

                        if (!isUserExist) {
                            final String key = Singleton.sDatabase.child("user").push().getKey();
                            final User user = new User();
                            user.email = LoginActivity.this.user.getEmail();
                            user.password = "Qws!2$(*AA";
                            user.id = key;
                            user.nationality = "";
                            user.photoUrl = "";
                            user.gender = "Male";
                            user.isAdmin = false;
                            user.birthDate = "";
                            user.name = usernameEditText.getText().toString();

                            Map<String, Object> userValues = User.toMap(user);

                            Map<String, Object> childUpdates = new HashMap<>();
                            childUpdates.put("/user/" + key, userValues);
                            Singleton.sDatabase.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if (databaseError == null) {
                                        Toast.makeText(LoginActivity.this, R.string.registerSuccess,
                                                Toast.LENGTH_SHORT).show();

//                                        //Login here
                                        Singleton.getInstance().currentUser = user;
                                        userChanged();

                                        //create user settings here
                                        for (Category category : categories) {

                                            Settings settings = new Settings();
                                            settings.status = true;
                                            settings.categoryName = category.name;
                                            settings.categoryId = category.id;
                                            settings.soundStatus = 2;


                                            Singleton.sDatabase.child("user_settings").child(key)
                                                    .child(category.id).setValue(Settings.toMap(settings), new DatabaseReference.CompletionListener() {
                                                @Override
                                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                                }
                                            });
                                        }

                                    } else {
                                        Toast.makeText(LoginActivity.this, R.string.registerFailed,
                                                Toast.LENGTH_SHORT).show();
                                    }

                                    progressView.setVisibility(View.GONE);
                                    progressView.stopAnimation();
                                }
                            });
                        } else {
                            Singleton.getInstance().currentUser = selectedUser;
                            userChanged();
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            } else {
                firebaseDataHelper.openUserListener(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        }
    }

    @Override
    public void updateProfilePicture() {

    }

    @Override
    public void userChanged() {

        final MyFirebaseInstanceIDService idService = new MyFirebaseInstanceIDService(LoginActivity.this);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                idService.onTokenRefresh();
            }
        });

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "email: " + Singleton.getInstance().currentUser.email);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "password: " + Singleton.getInstance().currentUser.password);

        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(CommonConstants.IS_LOGGED_IN, true);
        editor.putString(CommonConstants.USER_NAME, Singleton.getInstance().currentUser.name);
        editor.putString(CommonConstants.USER_EMAIL, Singleton.getInstance().currentUser.email);
        editor.putString(CommonConstants.USER_PHOTO_URL, Singleton.getInstance().currentUser.photoUrl);
        editor.putString(CommonConstants.USER_ID, Singleton.getInstance().currentUser.id);
        editor.putBoolean(CommonConstants.USER_IS_ADMIN, Singleton.getInstance().currentUser.isAdmin);
        editor.putString(CommonConstants.USER_PASSWORD, Singleton.getInstance().currentUser.password);
        editor.putString(CommonConstants.USER_NATIONALITY, Singleton.getInstance().currentUser.nationality);
        editor.putString(CommonConstants.USER_GENDER, Singleton.getInstance().currentUser.gender);
        editor.putString(CommonConstants.USER_BIRTH_DATE, Singleton.getInstance().currentUser.birthDate);
        if (Singleton.getInstance().currentUser.fcmToken != null)
            editor.putString(CommonConstants.USER_FCM_TOKEN, Singleton.getInstance().currentUser.fcmToken);

        editor.commit();

        openSettingsSingleValueListener();
        openSeriesSingleValueListener();
        openEpisodeSingleValueListener();
    }

    @Override
    public void categoryChanged() {

    }

    void openSettingsSingleValueListener() {
        final DatabaseReference userSettingsDatabaseReference = Singleton.sDatabase.
                child("user_settings").child(Singleton.getInstance().currentUser.id);

        userSettingsDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Singleton.getInstance().settings.clear();

                for (DataSnapshot settingDataSnapshot : dataSnapshot.getChildren()) {
                    Settings settings = Settings.parse(settingDataSnapshot);
                    Singleton.getInstance().settings.add(settings);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void openSeriesSingleValueListener() {
        final DatabaseReference seriesDatabaseReference = Singleton.sDatabase.
                child("series");

        seriesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Singleton.getInstance().seriesArrayList.add(Series.parse(child));
                }

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void openEpisodeSingleValueListener() {
        Singleton.sDatabase.
                child("episode").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Singleton.getInstance().episodeArrayList.add(Episode.parse(child));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}

