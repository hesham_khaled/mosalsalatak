package com.brighteyes.mosalsalatak_aflam.views.fragments;


import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.brighteyes.mosalsalatak_aflam.views.adapters.RecentAddedVideosAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecentlyAddedFragment extends Fragment {


    @Bind(R.id.recentAddedRecyclerView)
    RecyclerView recentAddedRecyclerView;

    @Bind(R.id.loadMoreCardView)
    CardView loadMoreCardView;

    @Bind(R.id.loadMoreTextView)
    TextView loadMoreTextView;

    int page = 1;
    ArrayList<Episode> episodes = new ArrayList<>();
    private ArrayList<Episode> paginatedEpisodeArrayList = new ArrayList<>();

    Activity mActivity;

    public RecentlyAddedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recently_added, container, false);
        ButterKnife.bind(this, view);

        openEpisodesListener();

        loadMoreCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page++;

                updateView();
            }
        });

        loadMoreCardView.setVisibility(View.GONE);
        Typeface typeface = App.objectGraph.get(Typeface.class);
        loadMoreTextView.setTypeface(typeface);

        return view;
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//
//    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public void updateView() {

        paginatedEpisodeArrayList.clear();

        if (episodes.size() <= 10 * page) {
            for (int i = 0; i < episodes.size(); i++) {
                paginatedEpisodeArrayList.add(episodes.get(i));
            }
            loadMoreCardView.setVisibility(View.GONE);
        } else {
            for (int i = 0; i < 10 * page; i++) {
                paginatedEpisodeArrayList.add(episodes.get(i));
            }
            loadMoreCardView.setVisibility(View.VISIBLE);
        }

        ArrayList<Episode> temp = paginatedEpisodeArrayList;

        RecentAddedVideosAdapter addedVideosAdapter = new RecentAddedVideosAdapter(mActivity,
                temp);
        recentAddedRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recentAddedRecyclerView.setAdapter(addedVideosAdapter);
    }

    public void openEpisodesListener() {
        Singleton.sDatabase.child("episode").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                episodes.clear();

                for (DataSnapshot episodeDataSnaspshot : dataSnapshot.getChildren()) {
                    Episode episode = Episode.parse(episodeDataSnaspshot);
                    episodes.add(episode);
                }
                Collections.reverse(episodes);
                updateView();
                openEpisodesChildListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    int size = 0;

    public void openEpisodesChildListener() {
        Singleton.sDatabase.child("episode").orderByChild("creationDate").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Episode episode = Episode.parse(dataSnapshot);
                size++;

                if (episodes.size() < size) {
                    episodes.add(episode);
                    sortEpisodes();
                    Singleton.getInstance().episodeArrayList.add(Episode.parse(dataSnapshot));
                    if (mActivity != null)
                        updateView();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                ArrayList<Episode> temp = episodes;

                for (Episode episode : temp) {
                    if (episode.id.equals(dataSnapshot.getKey())) {
                        episode.name = (String) dataSnapshot.child("name").getValue();
                        episode.photoUrl = (String) dataSnapshot.child("photo_url").getValue();
                        episode.photoUrl = (String) dataSnapshot.child("video_url").getValue();
                        episode.seriesName = (String) dataSnapshot.child("seriesName").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("creationDate").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("number_of_viewes").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("number_of_favourites").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("number_of_likes").getValue();
                        episode.numberOfViews = (long) dataSnapshot.child("number_of_unlikes").getValue();

                        Singleton.getInstance().episodeArrayList.get(temp.indexOf(episode)).name = (String) dataSnapshot.child("name").getValue();
                        Singleton.getInstance().episodeArrayList.get(temp.indexOf(episode)).photoUrl = (String) dataSnapshot.child("photo_url").getValue();
                        Singleton.getInstance().episodeArrayList.get(temp.indexOf(episode)).photoUrl = (String) dataSnapshot.child("video_url").getValue();
                        Singleton.getInstance().episodeArrayList.get(temp.indexOf(episode)).seriesName = (String) dataSnapshot.child("seriesName").getValue();
                        Singleton.getInstance().episodeArrayList.get(temp.indexOf(episode)).numberOfViews = (long) dataSnapshot.child("creationDate").getValue();
                        Singleton.getInstance().episodeArrayList.get(temp.indexOf(episode)).numberOfViews = (long) dataSnapshot.child("number_of_viewes").getValue();
                        Singleton.getInstance().episodeArrayList.get(temp.indexOf(episode)).numberOfViews = (long) dataSnapshot.child("number_of_favourites").getValue();
                        Singleton.getInstance().episodeArrayList.get(temp.indexOf(episode)).numberOfViews = (long) dataSnapshot.child("number_of_likes").getValue();
                        Singleton.getInstance().episodeArrayList.get(temp.indexOf(episode)).numberOfViews = (long) dataSnapshot.child("number_of_unlikes").getValue();

                        updateView();
                        break;
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                size--;
                ArrayList<Episode> temp = episodes;

                for (int i = 0; i < temp.size(); i++) {
                    if (episodes.get(i).id.equals(dataSnapshot.getKey())) {
                        episodes.remove(i);
                        Singleton.getInstance().episodeArrayList.remove(i);
                        updateView();
                        break;
                    }
                }
//                for (int i = 0; i < episodes.size(); i++) {
//                    if (episodes.get(i).id.equals(episode.id)) {
//                        episodes.remove(i);
//                        break;
//                    }
//                }
//                if (mActivity != null)
//                    updateView();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sortEpisodes() {
        Collections.sort(episodes, new Comparator<Episode>() {
            @Override
            public int compare(Episode o1, Episode o2) {
                if (o1.creationDate < o2.creationDate) {
                    return -1;
                } else if (o1.creationDate < o2.creationDate) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
    }
}
