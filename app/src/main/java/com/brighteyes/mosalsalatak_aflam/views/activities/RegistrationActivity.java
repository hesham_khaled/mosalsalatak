package com.brighteyes.mosalsalatak_aflam.views.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Category;
import com.brighteyes.mosalsalatak_aflam.entities.Episode;
import com.brighteyes.mosalsalatak_aflam.entities.Series;
import com.brighteyes.mosalsalatak_aflam.entities.Settings;
import com.brighteyes.mosalsalatak_aflam.entities.User;
import com.brighteyes.mosalsalatak_aflam.fcm.MyFirebaseInstanceIDService;
import com.brighteyes.mosalsalatak_aflam.interfaces.AuthenticationObserverListener;
import com.brighteyes.mosalsalatak_aflam.interfaces.UserObserverListener;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.utils.FirebaseDataHelper;
import com.brighteyes.mosalsalatak_aflam.utils.NetworkConnection;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RegistrationActivity extends AbstractActivity implements AuthenticationObserverListener, UserObserverListener {

    @Bind(R.id.registerTitleTextView)
    TextView registerTitleTextView;

    @Bind(R.id.usernameTextView)
    TextView usernameTextView;

    @Bind(R.id.usernameEditText)
    EditText usernameEditText;

    @Bind(R.id.usernameErrorTextView)
    TextView usernameErrorTextView;

    @Bind(R.id.passwordTextView)
    TextView passwordTextView;

    @Bind(R.id.passwordEditText)
    EditText passwordEditText;

    @Bind(R.id.passwordErrorTextView)
    TextView passwordErrorTextView;

    @Bind(R.id.registerButton)
    Button registerButton;

    @Bind(R.id.progress_view)
    CircularProgressView progressView;

    public FirebaseDataHelper firebaseDataHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);
        ButterKnife.bind(this);

        openCategoriesSingleValueListener();

        firebaseDataHelper = new FirebaseDataHelper(this);

        Typeface typeface = App.objectGraph.get(Typeface.class);
        registerTitleTextView.setTypeface(typeface);
        usernameTextView.setTypeface(typeface);
        usernameEditText.setTypeface(typeface);
        usernameErrorTextView.setTypeface(typeface);
        passwordTextView.setTypeface(typeface);
        passwordEditText.setTypeface(typeface);
        passwordErrorTextView.setTypeface(typeface);

        registerButton.setTypeface(typeface);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernameErrorTextView.setVisibility(View.GONE);
                passwordErrorTextView.setVisibility(View.GONE);

                if (usernameEditText.getText().toString().isEmpty()) {
                    usernameErrorTextView.setVisibility(View.VISIBLE);
                    usernameErrorTextView.setText(getResources().getString(R.string.usernameNotExist));
                    return;
                } else if (!validEmail(usernameEditText.getText().toString())) {
                    usernameErrorTextView.setVisibility(View.VISIBLE);
                    usernameErrorTextView.setText(getResources().getString(R.string.emailError));
                    return;
                }

                if (passwordEditText.getText().toString().isEmpty()) {
                    passwordErrorTextView.setVisibility(View.VISIBLE);
                    passwordErrorTextView.setText(getResources().getString(R.string.passwordNotExist));
                    return;
                } else if (passwordEditText.getText().toString().length() < 6) {
                    passwordErrorTextView.setVisibility(View.VISIBLE);
                    passwordErrorTextView.setText(getResources().getString(R.string.passwordLengthError));
                    return;
                }

                if (NetworkConnection.networkConnectivity(RegistrationActivity.this)) {

                    progressView.setVisibility(View.VISIBLE);
                    progressView.startAnimation();

                    Singleton.sAuth.createUserWithEmailAndPassword(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString())
                            .addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                                    if (!task.isSuccessful()) {
                                        Toast.makeText(RegistrationActivity.this, R.string.registerFailed,
                                                Toast.LENGTH_SHORT).show();
                                        progressView.setVisibility(View.GONE);
                                        progressView.stopAnimation();
                                    } else {
                                        // add data to user
                                        final String key = Singleton.sDatabase.child("user").push().getKey();
                                        final User user = new User();
                                        user.email = usernameEditText.getText().toString();
                                        user.password = passwordEditText.getText().toString();
                                        user.id = key;
                                        user.nationality = "";
                                        user.photoUrl = "";
                                        user.gender = "Male";
                                        user.isAdmin = false;
                                        user.birthDate = "";
                                        user.name = usernameEditText.getText().toString();

                                        Map<String, Object> userValues = User.toMap(user);

                                        Map<String, Object> childUpdates = new HashMap<>();
                                        childUpdates.put("/user/" + key, userValues);
                                        Singleton.sDatabase.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
                                            @Override
                                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                                if (databaseError == null) {
                                                    Toast.makeText(RegistrationActivity.this, R.string.registerSuccess,
                                                            Toast.LENGTH_SHORT).show();

                                                    //Login here

                                                    Singleton.getInstance().currentUser = user;
                                                    userChanged();

//                                                    Singleton.sAuth.signInWithEmailAndPassword(usernameEditText.getText().toString(),
//                                                            passwordEditText.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                                                        @Override
//                                                        public void onComplete(@NonNull Task<AuthResult> task) {
//                                                            if (task.isSuccessful()) {
//                                                                firebaseDataHelper.openAuthenticationListener();
//                                                            }
//                                                        }
//                                                    }).addOnFailureListener(new OnFailureListener() {
//                                                        @Override
//                                                        public void onFailure(@NonNull Exception e) {
//                                                            if (e instanceof FirebaseAuthInvalidUserException) { // email is not exist
//                                                                usernameErrorTextView.setVisibility(View.VISIBLE);
//                                                                usernameErrorTextView.setText(getResources().getString(R.string.usernameLoginError));
//                                                            }
//
//                                                            if (e instanceof FirebaseAuthInvalidCredentialsException) { // password is wrong
//                                                                passwordErrorTextView.setVisibility(View.VISIBLE);
//                                                                passwordErrorTextView.setText(getResources().getString(R.string.passwordLoginError));
//                                                            }
//
//                                                            progressView.setVisibility(View.GONE);
//                                                            progressView.stopAnimation();
//                                                        }
//                                                    });

//                                                    Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
//                                                    startActivity(intent);
//                                                    finish();

                                                    //create user settings here
                                                    for (Category category : categories) {

                                                        Settings settings = new Settings();
                                                        settings.status = true;
                                                        settings.categoryName = category.name;
                                                        settings.categoryId = category.id;
                                                        settings.soundStatus = 2;


                                                        Singleton.sDatabase.child("user_settings").child(key)
                                                                .child(category.id).setValue(Settings.toMap(settings), new DatabaseReference.CompletionListener() {
                                                            @Override
                                                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                                            }
                                                        });
                                                    }

                                                } else {
                                                    Toast.makeText(RegistrationActivity.this, R.string.registerFailed,
                                                            Toast.LENGTH_SHORT).show();
                                                }

                                                progressView.setVisibility(View.GONE);
                                                progressView.stopAnimation();
                                            }
                                        });


                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(RegistrationActivity.this, R.string.emailError,
                                        Toast.LENGTH_SHORT).show();
                            } else if (e instanceof FirebaseAuthUserCollisionException) {
                                Toast.makeText(RegistrationActivity.this, R.string.emailUsed,
                                        Toast.LENGTH_SHORT).show();
                            }

                            progressView.setVisibility(View.GONE);
                            progressView.stopAnimation();
                        }
                    });
                } else {
                    Toast.makeText(RegistrationActivity.this, R.string.noConnection,
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    void showNoConnectionContainer() {

    }

    @Override
    void hideNoConnectionContainer() {

    }

    public boolean validEmail(String email) {
        if (email == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    ArrayList<Category> categories = new ArrayList<>();

    void openCategoriesSingleValueListener() {
        Singleton.sDatabase.child("category").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    categories.add(Category.parse(child));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void userChanged() {
        final MyFirebaseInstanceIDService idService = new MyFirebaseInstanceIDService(RegistrationActivity.this);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                idService.onTokenRefresh();
            }
        });

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "email: " + Singleton.getInstance().currentUser.email);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "password: " + Singleton.getInstance().currentUser.password);

        SharedPreferences sharedPreferences = App.objectGraph.get(SharedPreferences.class);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(CommonConstants.IS_LOGGED_IN, true);
        editor.putString(CommonConstants.USER_NAME, Singleton.getInstance().currentUser.name);
        editor.putString(CommonConstants.USER_EMAIL, Singleton.getInstance().currentUser.email);
        editor.putString(CommonConstants.USER_PHOTO_URL, Singleton.getInstance().currentUser.photoUrl);
        editor.putString(CommonConstants.USER_ID, Singleton.getInstance().currentUser.id);
        editor.putBoolean(CommonConstants.USER_IS_ADMIN, Singleton.getInstance().currentUser.isAdmin);
        editor.putString(CommonConstants.USER_PASSWORD, Singleton.getInstance().currentUser.password);
        editor.putString(CommonConstants.USER_NATIONALITY, Singleton.getInstance().currentUser.nationality);
        editor.putString(CommonConstants.USER_GENDER, Singleton.getInstance().currentUser.gender);
        editor.putString(CommonConstants.USER_BIRTH_DATE, Singleton.getInstance().currentUser.birthDate);
        if (Singleton.getInstance().currentUser.fcmToken != null)
            editor.putString(CommonConstants.USER_FCM_TOKEN, Singleton.getInstance().currentUser.fcmToken);

        editor.commit();

        openSettingsSingleValueListener();
        openSeriesSingleValueListener();
        openEpisodeSingleValueListener();
    }

    @Override
    public void authenticationStatusChanged(boolean isSignedIn) {
        if (isSignedIn) {
            firebaseDataHelper.openUserListener(usernameEditText.getText().toString(),
                    passwordEditText.getText().toString());
        }
    }

    @Override
    public void updateProfilePicture() {

    }

    void openSettingsSingleValueListener() {
        final DatabaseReference userSettingsDatabaseReference = Singleton.sDatabase.
                child("user_settings").child(Singleton.getInstance().currentUser.id);

        userSettingsDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Singleton.getInstance().settings.clear();

                for (DataSnapshot settingDataSnapshot : dataSnapshot.getChildren()) {
                    Settings settings = Settings.parse(settingDataSnapshot);
                    Singleton.getInstance().settings.add(settings);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void openSeriesSingleValueListener() {
        final DatabaseReference seriesDatabaseReference = Singleton.sDatabase.
                child("series");

        seriesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Singleton.getInstance().seriesArrayList.add(Series.parse(child));
                }

                Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void openEpisodeSingleValueListener() {
        Singleton.sDatabase.
                child("episode").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Singleton.getInstance().episodeArrayList.add(Episode.parse(child));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
