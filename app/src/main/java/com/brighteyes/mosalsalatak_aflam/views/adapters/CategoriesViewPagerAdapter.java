package com.brighteyes.mosalsalatak_aflam.views.adapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.ViewGroup;

import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.TabObject;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;
import com.brighteyes.mosalsalatak_aflam.views.fragments.RecentlyAddedFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 1/8/17.
 */

public class CategoriesViewPagerAdapter extends FragmentPagerAdapter {

    private List<TabObject> tabObjects = new ArrayList<>();
    Activity context;
    FragmentManager fragmentManager;
    int baseId = 0;

    public CategoriesViewPagerAdapter(Activity context, FragmentManager fm, List<TabObject> tabObjects) {
        super(fm);
        this.fragmentManager = fm;
        this.tabObjects = tabObjects;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return tabObjects.get(position).fragment;
    }

    @Override
    public int getCount() {
        return tabObjects.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabObjects.get(position).fragmentTitle;
    }

    public void removeTabPage(int index) {

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "index to be deleted from removeTabPage is: " + index);


        TabObject tabObject = tabObjects.remove(index);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "category name to be deleted from removeTabPage is: " + tabObject.fragmentTitle);
        notifyDataSetChanged();

//        for (TabObject tabObject : tabObjects) {
//            if (tabObject.fragmentTitle.equals(title)) {
//                tabObjects.remove(tabObject);
//                notifyDataSetChanged();
//            }
//            break;
//        }

    }

    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return FragmentPagerAdapter.POSITION_NONE;
    }

    @Override
    public long getItemId(int position) {
        return baseId + position;
    }

    public void addTabPage(TabObject tabObject1) {

        tabObjects.remove(tabObjects.size() - 1);
//        notifyDataSetChanged();

        tabObjects.add(tabObject1);
//        notifyDataSetChanged();

        RecentlyAddedFragment recentlyAddedFragment = new RecentlyAddedFragment();
        tabObjects.add(new TabObject(recentlyAddedFragment, context.getResources().getString(R.string.recentAdded)));

//        notifyPositionChanged(1);

        notifyDataSetChanged();
    }

//    public void editTab(TabObject tabObject) {
//
//        for (int i = 0; i < tabObjects.size(); i++) {
//            if (((CategoryFragment) tabObjects.get(i).fragment).categoryId.equals(tabObject.categoryId)) {
//                tabObjects.get(i).fragmentTitle = tabObject.fragmentTitle;
//
//                notifyDataSetChanged();
//                break;
//            }
//        }
//    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);

        try {
            if (position <= getCount()) {
//                FragmentManager fragmentManager = ((Fragment) object).getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.remove((Fragment) object);
                transaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void notifyPositionChanged(int n) {
        baseId = baseId + getCount() + n;
    }
}
