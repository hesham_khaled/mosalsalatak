package com.brighteyes.mosalsalatak_aflam.views.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.percent.PercentRelativeLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.R;
import com.brighteyes.mosalsalatak_aflam.entities.Settings;
import com.brighteyes.mosalsalatak_aflam.utils.Singleton;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by user on 2/13/17.
 */

public class NotificationSettingsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    LayoutInflater inflater;
    Typeface typeface;
    RecyclerView.ViewHolder viewHolder;

    private ArrayList<Settings> mSettings;

    private Activity mContext;

    public NotificationSettingsRecyclerViewAdapter(Activity context, ArrayList<Settings> settings) {
        mContext = context;
        mSettings = settings;
        inflater = LayoutInflater.from(mContext);
        typeface = App.objectGraph.get(Typeface.class);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == 0) {
            View view = inflater.inflate(R.layout.notification_settings_item, parent, false);
            viewHolder = new ViewHolder(view);
        } else {
            View view = inflater.inflate(R.layout.button_item, parent, false);
            viewHolder = new ButtonViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder.getItemViewType() == 0) {
            final ViewHolder ordinaryHolder = (ViewHolder) holder;
            ordinaryHolder.emptyViewHolder();

            ordinaryHolder.categoryNameTextView.setText(mSettings.get(position).categoryName);

            ordinaryHolder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
//                        ordinaryHolder.soundContainer.setVisibility(View.VISIBLE);
                        mSettings.get(position).status = true;
                    } else {
//                        ordinaryHolder.soundContainer.setVisibility(View.GONE);
                        mSettings.get(position).status = false;
                    }
                }
            });

            if (mSettings.get(position).status) {
                ordinaryHolder.aSwitch.setChecked(true);
//                ordinaryHolder.soundContainer.setVisibility(View.VISIBLE);

                switch ((int) mSettings.get(position).soundStatus) {
                    case 0: // vibration
                        ordinaryHolder.vibrationRadioButton.setChecked(true);
                        break;

                    case 1: //mute
                        ordinaryHolder.muteRadioButton.setChecked(true);
                        break;

                    case 2: // sound
                        ordinaryHolder.soundRadioButton.setChecked(true);
                        break;

                    default:
                        break;
                }

            } else {
                ordinaryHolder.aSwitch.setChecked(false);
//                ordinaryHolder.soundContainer.setVisibility(View.GONE);
            }

//            ordinaryHolder.vibrationRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked) {
//                        ordinaryHolder.soundRadioButton.setChecked(false);
//                        ordinaryHolder.muteRadioButton.setChecked(false);
//                        mSettings.get(position).soundStatus = 0;
//                    }
//                }
//            });
//
//            ordinaryHolder.muteRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked) {
//                        ordinaryHolder.soundRadioButton.setChecked(false);
//                        ordinaryHolder.vibrationRadioButton.setChecked(false);
//                        mSettings.get(position).soundStatus = 1;
//                    }
//                }
//            });
//
//            ordinaryHolder.soundRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked) {
//                        ordinaryHolder.muteRadioButton.setChecked(false);
//                        ordinaryHolder.vibrationRadioButton.setChecked(false);
//                        mSettings.get(position).soundStatus = 2;
//                    }
//                }
//            });
        } else if (holder.getItemViewType() == 1) {
            ButtonViewHolder ordinaryHolder = (ButtonViewHolder) holder;
            ordinaryHolder.emptyViewHolder();

            ordinaryHolder.updateButton.setTypeface(typeface);
            ordinaryHolder.updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (Settings settings : Singleton.getInstance().settings) {
                        Singleton.sDatabase.child("user_settings").child(Singleton.getInstance().currentUser.id)
                                .child(settings.categoryId).updateChildren(Settings.toMap(settings));
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mSettings.size()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return mSettings.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.switch1)
        Switch aSwitch;

        @Bind(R.id.categoryNameTextView)
        TextView categoryNameTextView;

        @Bind(R.id.soundContainer)
        PercentRelativeLayout soundContainer;

        @Bind(R.id.vibrationRadioButton)
        RadioButton vibrationRadioButton;

        @Bind(R.id.vibrationImageView)
        ImageView vibrationImageView;

        @Bind(R.id.muteRadioButton)
        RadioButton muteRadioButton;

        @Bind(R.id.muteImageView)
        ImageView muteImageView;

        @Bind(R.id.soundRadioButton)
        RadioButton soundRadioButton;

        @Bind(R.id.soundImageView)
        ImageView soundImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void emptyViewHolder() {
            aSwitch.setChecked(false);

            categoryNameTextView.setTypeface(typeface);
            categoryNameTextView.setText("");

//            soundContainer.setVisibility(View.VISIBLE);

            vibrationRadioButton.setChecked(false);
            muteRadioButton.setChecked(false);
            soundRadioButton.setChecked(false);
        }
    }

    class ButtonViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.updateButton)
        Button updateButton;

        public ButtonViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void emptyViewHolder() {
            updateButton.setOnClickListener(null);
        }
    }
}
