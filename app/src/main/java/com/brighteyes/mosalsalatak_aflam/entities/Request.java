package com.brighteyes.mosalsalatak_aflam.entities;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 1/11/17.
 */

public class Request extends Entity {

    public String body;
    public String name;
    public boolean isActive;

    public static Map<String, Object> parse(Request request) {
        HashMap<String, Object> map = new HashMap<>();

        map.put("body", request.body);
        map.put("name", request.name);
        map.put("isActive", request.isActive);

        return map;
    }

}
