package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 1/4/17.
 */

public class Settings extends Entity {

    public String categoryId;
    public long soundStatus;
    public boolean status;
    public String categoryName;

    public static Settings parse(DataSnapshot dataSnapshot) {

        Settings settings = new Settings();
        if (dataSnapshot.getValue() != null) {
            settings.categoryId = dataSnapshot.getKey();
            settings.status = (boolean) dataSnapshot.child("isOpen").getValue();
            settings.soundStatus = (long) dataSnapshot.child("sound_state").getValue();
            settings.categoryName = (String) dataSnapshot.child("category_name").getValue();
        }
        return settings;
    }

    public static Map<String, Object> toMap(Settings settings) {
        HashMap<String, Object> result = new HashMap<>();

        result.put("isOpen", settings.status);
        result.put("sound_state", settings.soundStatus);
        result.put("category_name", settings.categoryName);

        return result;
    }

}
