package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

/**
 * Created by user on 1/4/17.
 */

public class UserEpisodes extends Entity {

    public String userId;
    public ArrayList<Episode> episodes = new ArrayList<>();


    public static UserEpisodes parse(String userId, DataSnapshot dataSnapshot) {
        UserEpisodes userEpisodes = new UserEpisodes();

        userEpisodes.userId = userId;
        if (dataSnapshot != null) {
            userEpisodes.episodes.add(Episode.parse(dataSnapshot));
        }

        return userEpisodes;
    }

}
