package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 1/4/17.
 */

public class Series extends Entity {

    public String id;
    public String categoryId;
    public String name;
    public long numberOfVideos;
    public long numberOfViews;
    public String photoUrl;

    public static Series parse(DataSnapshot dataSnapshot) {

        Series series = new Series();

        if (dataSnapshot.getValue() != null) {
            series.id = dataSnapshot.getKey();
            if (dataSnapshot.child("category_id").exists())
                series.categoryId = (String) dataSnapshot.child("category_id").getValue();
            series.name = (String) dataSnapshot.child("name").getValue();
            series.photoUrl = (String) dataSnapshot.child("photo_url").getValue();
            series.numberOfVideos = (long) dataSnapshot.child("number_of_videos").getValue();
            series.numberOfViews = (long) dataSnapshot.child("number_of_viewes").getValue();
        }

        return series;

    }

    public static Map<String, Object> toMap(Series series, boolean isAddCategoryId) {

        HashMap<String, Object> map = new HashMap<>();

        if (isAddCategoryId)
            map.put("category_id", series.categoryId);
        map.put("name", series.name);
        map.put("photo_url", series.photoUrl);
        map.put("number_of_videos", series.numberOfVideos);
        map.put("number_of_viewes", series.numberOfViews);

        return map;

    }

}
