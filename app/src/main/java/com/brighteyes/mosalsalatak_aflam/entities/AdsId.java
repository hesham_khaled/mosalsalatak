package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

/**
 * Created by user on 2/22/17.
 */

public class AdsId extends Entity {

    public String id;

    public AdsId() {
    }

    public AdsId(String id) {
        this.id = id;
    }

    public static AdsId parse(DataSnapshot dataSnapshot) {

        AdsId adsId = new AdsId();

        if (dataSnapshot != null) {
            adsId.id = (String) dataSnapshot.child("ad_link").getValue();
        }

        return adsId;
    }

}
