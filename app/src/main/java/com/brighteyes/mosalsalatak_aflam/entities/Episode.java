package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 1/4/17.
 */

public class Episode extends Entity {

    public String name;
    public String id;
    public String seriesId;
    public String photoUrl;
    public String videoUrl;
    public long numberOfFavourites;
    public long numberOfLikes;
    public long numberOfUnlikes;
    public long numberOfViews;
    public long creationDate;
    public String seriesName;

    public static Episode parse(DataSnapshot dataSnapshot) {

        Episode episode = new Episode();

        if (dataSnapshot.getValue() != null) {
            episode.id = dataSnapshot.getKey();
            episode.seriesId = (String) dataSnapshot.child("series_id").getValue();
            episode.creationDate = (long) dataSnapshot.child("creationDate").getValue();
            episode.name = (String) dataSnapshot.child("name").getValue();
            episode.photoUrl = (String) dataSnapshot.child("photo_url").getValue();
            episode.videoUrl = (String) dataSnapshot.child("video_url").getValue();
            episode.seriesName = (String) dataSnapshot.child("seriesName").getValue();
            episode.numberOfFavourites = (long) dataSnapshot.child("number_of_favourites").getValue();
            episode.numberOfLikes = (long) dataSnapshot.child("number_of_likes").getValue();
            episode.numberOfUnlikes = (long) dataSnapshot.child("number_of_unlikes").getValue();
            episode.numberOfViews = (long) dataSnapshot.child("number_of_viewes").getValue();
        }

        return episode;
    }

    public static Map<String, Object> toMap(Episode episode) {
        HashMap<String, Object> map = new HashMap<>();

        map.put("creationDate", episode.creationDate);
        map.put("name", episode.name);
        map.put("photo_url", episode.photoUrl);
        map.put("video_url", episode.videoUrl);
        map.put("seriesName", episode.seriesName);
        map.put("number_of_favourites", episode.numberOfFavourites);
        map.put("number_of_likes", episode.numberOfLikes);
        map.put("number_of_unlikes", episode.numberOfUnlikes);
        map.put("number_of_viewes", episode.numberOfViews);

        return map;
    }

}
