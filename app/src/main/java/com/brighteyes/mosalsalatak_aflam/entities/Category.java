package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

/**
 * Created by user on 1/4/17.
 */

public class Category extends Entity {

    public String id;
    public String name;

    public Category(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Category() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Category parse(DataSnapshot dataSnapshot) {

        Category category = new Category();
        if (dataSnapshot.getValue() != null) {
            category.id = dataSnapshot.getKey();
            category.name = (String) dataSnapshot.child("name").getValue();
        }
        return category;

    }

}
