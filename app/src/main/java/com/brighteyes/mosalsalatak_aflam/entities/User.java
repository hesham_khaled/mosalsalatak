package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 1/4/17.
 */

public class User extends Entity {

    public String id;
    public String name;
    public String email;
    public String password;
    public String nationality;
    public String gender;
    public String birthDate;
    public String photoUrl;
    public boolean isAdmin;
    public String fcmToken;

    public static User parse(DataSnapshot dataSnapshot) {

        User user = new User();

        if (dataSnapshot.getValue() != null) {
            user.id = dataSnapshot.getKey();
            user.email = (String) dataSnapshot.child("email").getValue();
            user.isAdmin = (boolean) dataSnapshot.child("isAdmin").getValue();
            user.password = (String) dataSnapshot.child("password").getValue();
            user.photoUrl = (String) dataSnapshot.child("photo_url").getValue();
            user.nationality = (String) dataSnapshot.child("nationality").getValue();
            user.birthDate = (String) dataSnapshot.child("birth_date").getValue();
            user.gender = (String) dataSnapshot.child("gender").getValue();
            if (dataSnapshot.child("fcm_token").exists()) {
                user.fcmToken = (String) dataSnapshot.child("fcm_token").getValue();
            }
        }
        return user;
    }

    public static Map<String, Object> toMap(User user) {
        HashMap<String, Object> result = new HashMap<>();

        result.put("birth_date", user.birthDate);
        result.put("email", user.email);
        result.put("password", user.password);
        result.put("gender", user.gender);
        result.put("name", user.name);
        result.put("photo_url", user.photoUrl);
        result.put("isAdmin", user.isAdmin);
        result.put("nationality", user.nationality);
        if (user.fcmToken != null) {
            result.put("fcm_token", user.fcmToken);
        }
        return result;
    }


}
