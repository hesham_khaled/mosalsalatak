package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by user on 1/11/17.
 */

public class CommentUser extends Entity {

    public String commentId;
    public String userId;
    public String userPhotoUrl;

    public static CommentUser parse(DataSnapshot dataSnapshot) {
        CommentUser commentUser = new CommentUser();
        if (dataSnapshot != null) {
            JSONObject jsonObject = new JSONObject((HashMap) dataSnapshot.getValue());
            commentUser.commentId = dataSnapshot.getKey();
            Iterator<String> keys = jsonObject.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                commentUser.userId = key;
                try {
                    if (jsonObject.get(key) instanceof HashMap) {
                        HashMap<String, String> photo = (HashMap<String, String>) jsonObject.get(key);
                        commentUser.userPhotoUrl = photo.get("photo_url");
                    } else {
                        JSONObject photo = (JSONObject) jsonObject.get(key);
                        commentUser.userPhotoUrl = photo.getString("photo_url");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return commentUser;
    }
}
