package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 1/4/17.
 */

public class Comment extends Entity {


    public String id;
    public String body;

    public static Comment parse(DataSnapshot dataSnapshot) {

        Comment comment = new Comment();

        if (dataSnapshot.getValue() != null) {
            comment.id = dataSnapshot.getKey();
            comment.body = (String) dataSnapshot.child("comment_body").getValue();
        }

        return comment;
    }

    public static Map<String, Object> toMap(Comment comment) {
        HashMap<String, Object> map = new HashMap<>();

        map.put("comment_body", comment.body);

        return map;
    }
}
