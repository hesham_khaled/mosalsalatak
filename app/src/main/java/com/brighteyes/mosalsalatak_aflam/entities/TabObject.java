package com.brighteyes.mosalsalatak_aflam.entities;

import android.support.v4.app.Fragment;

/**
 * Created by user on 1/15/17.
 */

public class TabObject extends Entity {

    public Fragment fragment;
    public String fragmentTitle;
    public String categoryId;

    public TabObject(Fragment fragment, String fragmentTitle) {
        this.fragment = fragment;
        this.fragmentTitle = fragmentTitle;
    }

    public TabObject(String categoryId, String title) {
        this.categoryId = categoryId;
        this.fragmentTitle = title;
    }

}
