package com.brighteyes.mosalsalatak_aflam.entities;

import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by compumart on 8/15/17.
 */

public class FCMToken extends Entity {

    public String id;
    public String token;

    public static FCMToken parse(DataSnapshot dataSnapshot) {

        FCMToken token = new FCMToken();

        if (dataSnapshot.getValue() != null) {
            token.id = dataSnapshot.getKey();
            token.token = (String) dataSnapshot.child("token").getValue();
        }
        return token;
    }

    public static Map<String, Object> toMap(FCMToken fcmToken) {
        HashMap<String, Object> result = new HashMap<>();

        result.put("token", fcmToken.token);

        return result;
    }


}
