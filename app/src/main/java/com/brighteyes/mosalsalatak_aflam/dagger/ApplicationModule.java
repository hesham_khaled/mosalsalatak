package com.brighteyes.mosalsalatak_aflam.dagger;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.brighteyes.mosalsalatak_aflam.App;
import com.brighteyes.mosalsalatak_aflam.utils.CommonConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by heshamkhaled on 9/21/16.
 */


@Module(
        library = true,
        complete = false,
        injects = {App.class, SharedPreferences.class, Typeface.class}
)
public class ApplicationModule {

    private App mApp;

    public ApplicationModule(App app) {
        mApp = app;
    }

    @Provides
    @Singleton
    @ForApplication
    public Context provideApplicationContext() {
        return mApp.getApplicationContext();
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharedPreferences() {
        return mApp.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    public Typeface provideTypeFace() {
        return Typeface.createFromAsset(mApp.getAssets(), "fonts/gf-flat-regular.ttf");
    }

    @Provides
    @Singleton
    public FragmentTransaction provideFragmentManager() {
        return ((FragmentActivity) mApp.getApplicationContext()).getSupportFragmentManager().beginTransaction();
    }

    @Provides
    @Singleton
    public FragmentManager provideFragmentTransaction() {
        return ((FragmentActivity) mApp.getApplicationContext()).getSupportFragmentManager();
    }

}
