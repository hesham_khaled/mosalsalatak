package com.brighteyes.mosalsalatak_aflam.dagger;

import javax.inject.Qualifier;


@Qualifier
public @interface ForApplication {
}
