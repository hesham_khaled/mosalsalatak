package com.brighteyes.mosalsalatak_aflam.interfaces;

/**
 * Created by heshamdonia on 11/6/16.
 */

public interface SubjectListener {

    void register(ObserverListener observer);

    void unregister(ObserverListener observer);

    void notifyObservers(boolean internetConnectionExist);

    void notifyProfilePictureUpdated();

    void notifyDataChanged();

}
