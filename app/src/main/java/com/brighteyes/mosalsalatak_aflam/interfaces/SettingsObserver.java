package com.brighteyes.mosalsalatak_aflam.interfaces;

/**
 * Created by user on 1/11/17.
 */

public interface SettingsObserver {

    void settingsChanged();

}
