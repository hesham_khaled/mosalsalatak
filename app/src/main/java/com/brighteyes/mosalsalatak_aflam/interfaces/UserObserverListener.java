package com.brighteyes.mosalsalatak_aflam.interfaces;

/**
 * Created by user on 1/6/17.
 */

public interface UserObserverListener {

    void userChanged();

}
