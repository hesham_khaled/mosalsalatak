package com.brighteyes.mosalsalatak_aflam.interfaces;

import com.brighteyes.mosalsalatak_aflam.entities.CategorySeries;

/**
 * Created by user on 1/11/17.
 */

public interface CategorySeriesObserver {

    void categorySeriesChanged(CategorySeries categorySeries);

    void onError(String errorMessage);

}
