package com.brighteyes.mosalsalatak_aflam.interfaces;


import com.brighteyes.mosalsalatak_aflam.entities.Entity;
import com.brighteyes.mosalsalatak_aflam.entities.Filter;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public interface DomainListener {
    void find(String id, MainListener<Entity> listener);

    void find(Filter filters, MainListener<Entity> listener);

    void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener);

    void save(Entity entity, MainListener<Boolean> listener);
}
