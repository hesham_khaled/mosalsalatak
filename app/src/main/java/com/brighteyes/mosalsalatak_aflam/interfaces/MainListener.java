package com.brighteyes.mosalsalatak_aflam.interfaces;

/**
 * Created by hesham on 04/04/16.
 */
public interface MainListener<T> {

    void onSuccess(T result);

    void onError(Throwable error);
}
