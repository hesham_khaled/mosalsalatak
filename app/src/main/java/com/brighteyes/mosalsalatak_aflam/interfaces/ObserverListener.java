package com.brighteyes.mosalsalatak_aflam.interfaces;

/**
 * Created by heshamdonia on 11/6/16.
 */

public interface ObserverListener {

    void update(boolean internetConnectionExist);

    void updateProfilePicture();
}
