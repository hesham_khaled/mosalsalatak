package com.brighteyes.mosalsalatak_aflam.interfaces;

/**
 * Created by user on 1/5/17.
 */

public interface CategoryObserverListener {

    void categoryChanged();

}
