package com.brighteyes.mosalsalatak_aflam.interfaces;

import android.content.Intent;

import com.brighteyes.mosalsalatak_aflam.NotifyFirebaseObserverEnum;

/**
 * Created by user on 1/3/17.
 */

public interface FirebaseSubjectListener {

    void registerAuthenticationObserver(AuthenticationObserverListener observer);

    void unregisterAuthenticationObserver(AuthenticationObserverListener observer);

    void registerUserObserver(UserObserverListener observer);

    void unregisterUserObserver(UserObserverListener observer);

    void registerCategoryObserver(CategoryObserverListener observer);

    void unregisterCategoryObserver(CategoryObserverListener observer);

    void registerCategorySeriesObserver(CategorySeriesObserver observer);

    void unregisterCategorySeriesObserver(CategorySeriesObserver observer);

    void registerEpisodeCommentsObserver(EpisodeCommentsObserver observer);

    void unregisterEpisodeCommentsObserver(EpisodeCommentsObserver observer);

    void registerFavouriteObserver(FavouritesObserver observer);

    void unregisterFavouriteObserver(FavouritesObserver observer);

    void registerSeriesEpisodeObserver(SeriesEpisodesObserver observer);

    void unregisterSeriesEpisodeObserver(SeriesEpisodesObserver observer);

    void registerSeriesObserver(SeriesObserver observer);

    void unregisterSeriesObserver(SeriesObserver observer);

    void registerSettingsObserver(SettingsObserver observer);

    void unregisterSettingsObserver(SettingsObserver observer);

    void registerEpisodesObserver(EpisodesObserver observer);

    void unregisterEpisodesObserver(EpisodesObserver observer);

    void notifyObservers(NotifyFirebaseObserverEnum type, Intent intent);
}
